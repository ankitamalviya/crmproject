package com.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

@Configuration
public class DBClass {

	@Bean(name = "dataSource")

	/******** Connection for database class *********/

	public DriverManagerDataSource dataSource() {

		DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource();
		driverManagerDataSource.setDriverClassName("com.mysql.jdbc.Driver");
//		driverManagerDataSource.setUrl("jdbc:mysql://localhost:3306/CueLabDB?useUnicode=true&characterEncoding=UTF-8");
		driverManagerDataSource.setUrl("jdbc:mysql://103.118.157.109:3307/cuelabdb?useUnicode=true&characterEncoding=UTF-8");
		driverManagerDataSource.setUsername("root");
		driverManagerDataSource.setPassword("HDl@839Jd8hdjH");
//		driverManagerDataSource.setPassword("abhinav");
		System.out.println("connected...");
		return driverManagerDataSource;
	}
}	
