package com.security;

import java.net.HttpURLConnection;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;

import com.dao.FcmDao;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class SendNotification {
	
	
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	static final String DB_URL = "jdbc:mysql://103.118.157.109:3307/cuelabdb?useUnicode=true&characterEncoding=UTF-8";
	// static final String DB_URL =
	// "jdbc:mysql://localhost:3306/CueLabDB?useUnicode=true&characterEncoding=UTF-8";

	static final String USER = "root";
	static final String PASS = "HDl@839Jd8hdjH";
	// static final String PASS = "abhinav";

	FcmDao fcmDao = new FcmDao();
	ObjectMapper objectMapper = new ObjectMapper();
	String json = null;
	String json1 = "";
	List<String> fcmList = new ArrayList<String>();
	

	public String Notification(String uid, String title, String body1) throws JSONException, JsonProcessingException {
System.out.println("helooo");
		FcmDao fcmDao = new FcmDao();
		ObjectMapper objectMapper = new ObjectMapper();
		String json = null;
		String json1 = "";
		String firebaseResponse = "";
		List<String> respList = new ArrayList<String>();
		Map<String, Object> response = new HashMap<String, Object>();
		int statusCode = 0;
		Connection conn = null;
		Statement stmt = null;
		String fcmid = null;

		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			stmt = conn.createStatement();

			PreparedStatement statement = conn
					.prepareStatement("select fcmid from tbl_fcmdetails where  uid =? order by tbid desc limit 1");
			statement.setString(1, uid);
			ResultSet rs = statement.executeQuery();
			while (rs.next()) {
				fcmid = rs.getString("fcmid");
				
				System.out.println("fcmid...list..."+fcmid);
//				fcmList.add(fcmid);
			}

			stmt.close();
//			for (int i = 0; i < fcmList.size(); i++) {
				String fcm = fcmid;
				JSONObject body = new JSONObject();
				body.put("to", fcm);

				body.put("priority", "high");
				JSONObject notification = new JSONObject();
				JSONObject notification1 = new JSONObject();
				 notification.put("title", title);
				 notification.put("body", body1);
				notification1.put("notification", notification);
				body.put("data", notification1);
				HttpEntity<String> request = new HttpEntity<>(body.toString());

				CompletableFuture<String> pushNotification = fcmDao.send(request);
				CompletableFuture.allOf(pushNotification).join();
				try {
					firebaseResponse = pushNotification.get();
					respList.add(firebaseResponse);

				} catch (InterruptedException e) {

				} catch (ExecutionException e) {

				}

//			}

			if (firebaseResponse.equals("")) {
				response.put("StatusCode", HttpURLConnection.HTTP_NO_CONTENT);
				// map1.put("SendSMS", );
				response.put("message", "Not Notification");
				statusCode = HttpURLConnection.HTTP_NO_CONTENT;
			} else {
				response.put("StatusCode", HttpURLConnection.HTTP_OK);
				response.put("response", respList);
				response.put("message", "Notification Ok");
			}
			json = objectMapper.writeValueAsString(response);
			// return new ResponseEntity<>(respList, HttpStatus.OK);
			return json;

		} catch (SQLException se) {
			// Handle errors for JDBC
			se.printStackTrace();
		} catch (Exception e) {
			// Handle errors for Class.forName
			e.printStackTrace();
		} finally {
			// finally block used to close resources
			try {
				if (stmt != null)
					conn.close();
			} catch (SQLException se) {
			} // do nothing
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException se) {
				se.printStackTrace();
			} // end finally try
		} // end try
		return uid;
	}

}
