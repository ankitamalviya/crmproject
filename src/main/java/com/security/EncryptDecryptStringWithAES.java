package com.security;

import java.security.Key;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

public class EncryptDecryptStringWithAES {
	
	private static final String ALGO = "AES";
	private static final byte[] keyValue = new byte[] { 'A', 'I', 'S', 'P', 'L', 'K', 'E', 'Y', '9', '3', '2', '6', '8',
			'5', '4', '7' };

	private Key generateKey() throws Exception {
		Key key = new SecretKeySpec(keyValue, ALGO);
		return key;
	}

	@SuppressWarnings("restriction")
	public String encrypt(String Data) throws Exception {

		Key key = generateKey();
		Cipher c = Cipher.getInstance(ALGO);
		c.init(Cipher.ENCRYPT_MODE, key);
		byte[] encVal = c.doFinal(Data.getBytes());
		String encryptedValue = new BASE64Encoder().encode(encVal);
		return encryptedValue;
	}

	@SuppressWarnings("restriction")
	public String decrypt(String str) throws Exception {
		Key key = generateKey();
		Cipher c = Cipher.getInstance(ALGO);
		c.init(Cipher.DECRYPT_MODE, key);
		byte[] decordedValue = new BASE64Decoder().decodeBuffer(str);
		byte[] decValue = c.doFinal(decordedValue);
		String decryptedValue = new String(decValue);
		return decryptedValue;

	}

}
