package com.config;

import javax.servlet.Filter;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import com.config.CORSFilter;
import com.config.HelloWorldConfiguration;

public class HelloWorldInitializer extends AbstractAnnotationConfigDispatcherServletInitializer  {
	
	protected Class<?>[] getRootConfigClasses() {
        return new Class[] { HelloWorldConfiguration.class };
    }
  
    protected Class<?>[] getServletConfigClasses() {
        return null;
    }
  
    protected String[] getServletMappings() {
        return new String[] { "/" };
    }
    
    protected Filter[] getServletFilters() {
    	Filter [] singleton = { new CORSFilter()};
    	return singleton;
    }

}
