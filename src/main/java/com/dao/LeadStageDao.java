package com.dao;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import com.model.LeadStageModel;
import com.security.EncryptDecryptStringWithAES;

@Repository
public class LeadStageDao {
	private JdbcTemplate jdbcTemplate;
	private DataSource dataSource;
	private SimpleJdbcCall sp_addleadstage, sp_downloadleadstage, sp_downloadleadstagebymaxid,
			sp_downloadtaskassignedbyname;
	EncryptDecryptStringWithAES encry = new EncryptDecryptStringWithAES();

	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
		this.dataSource = dataSource;
		this.sp_addleadstage = new SimpleJdbcCall(this.dataSource).withProcedureName("sp_addleadstage");
		this.sp_downloadleadstage = new SimpleJdbcCall(this.dataSource).withProcedureName("sp_downloadleadstage");
		this.sp_downloadleadstagebymaxid = new SimpleJdbcCall(this.dataSource)
				.withProcedureName("sp_downloadleadstagebymaxid");
		this.sp_downloadtaskassignedbyname = new SimpleJdbcCall(this.dataSource)
				.withProcedureName("sp_downloadtaskassignedbyname");
	}

	/*****************************
	 * Add Lead Details Detail
	 * 
	 * @throws Exception
	 ********************************/
	public Map<String, Object> addleadstage(LeadStageModel leadStageModel) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		int maxId = 0;
		int status = 0;
		String uid = encry.decrypt(leadStageModel.getUid());
		String orguniqueid = encry.decrypt(leadStageModel.getOrguniqueid());

		try {
			SqlParameterSource parameter = new MapSqlParameterSource()
					.addValue("i_stageid", leadStageModel.getStageid())
					.addValue("i_stagename", leadStageModel.getStagename()).addValue("i_uid", uid)
					.addValue("i_orguniqueid", orguniqueid).addValue("i_status", leadStageModel.getStatus())
					.addValue("i_createdby", leadStageModel.getCreatedby())
					.addValue("i_deviceid", leadStageModel.getDeviceid())
					.addValue("i_modifiedby", leadStageModel.getModifiedby());

			map = sp_addleadstage.execute(parameter);
			System.out.println("map......" + map);
			status = (int) map.get("o_status");
			maxId = (int) map.get("o_maxid");
			System.out.println("maxId" + maxId);
			if (maxId != 0)
				map = downloadleadstagebymaxid(maxId);
		} catch (Exception e) {
			System.out.println(e);
		}
		map.put("status", status);
		return map;
	}

	public Map<String, Object> downloadleadstage(String orguniqueid) throws Exception {
		String orguniqueid1 = encry.decrypt(orguniqueid);
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			SqlParameterSource in = new MapSqlParameterSource().addValue("i_orguniqueid", orguniqueid1);
			map = sp_downloadleadstage.execute(in);
			System.out.println(map);
			System.out.println(map.get("o_status"));
		} catch (Exception e) {
			System.out.println(e);
		}

		return map;
	}

	public Map<String, Object> downloadleadstagebymaxid(int id) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			SqlParameterSource in = new MapSqlParameterSource().addValue("i_maxid", id);
			map = sp_downloadleadstagebymaxid.execute(in);
			System.out.println(map);
			System.out.println(map.get("o_status"));
		} catch (Exception e) {
			System.out.println(e);
		}

		return map;
	}

	public Map<String, Object> getUserRegisterData(String uid) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			SqlParameterSource in = new MapSqlParameterSource().addValue("i_uid", uid);
			map = sp_downloadtaskassignedbyname.execute(in);
		} catch (Exception e) {
			System.out.println(e);
		}

		return map;

	}

}
