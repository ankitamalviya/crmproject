package com.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import javax.sql.DataSource;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import com.controller.HeaderRequestInterceptor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.model.FcmModel;
import com.security.EncryptDecryptStringWithAES;

@Repository
public class FcmDao {
//	private static final String FIREBASE_SERVER_KEY = "AAAAIBavI_g:APA91bEGqOWkfUHdXBDURxVE3xgpsU1ToeEbQGRHKouC5ZojMpTZc_06qdGpSpZ1-nNyTVKeG5J2zpy45q6OEDfjaL6shjZIX1ZZqLwATpcbJ8wcjKKRFa7GjTAkp6r_eP_fOEShU7sk";
//	private static final String FIREBASE_SENDER_ID = "137819530232";
	
	private static final String FIREBASE_SERVER_KEY = "AAAANU200CE:APA91bFlGrO11rj_F5f69lfqKyJfntvGxS5g0LfDsi2P-4rXw8qnBgRmsGvWcj2HDCMyvfnfoKCKs9DaxBLzrDHvphuoFXddsEJW4KNtFRRXdJ5PLYzaFM1HazzVAZK3HC7gd4917Gq4";
	private static final String FIREBASE_SENDER_ID = "228936962081";
	private static final String FIREBASE_API_URL = "https://fcm.googleapis.com/fcm/send";
	String json = "";
	ObjectMapper objectMapper = new ObjectMapper();
	private JdbcTemplate jdbcTemplate;
	private DataSource dataSource;
	private SimpleJdbcCall sp_addfmcdata, sp_downloadfmcdatabymaxid, sp_downloadfmcdata,
			sp_downloadFireBaseMessagesByMobNonew;
	EncryptDecryptStringWithAES encry = new EncryptDecryptStringWithAES();

	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
		this.dataSource = dataSource;
		this.sp_addfmcdata = new SimpleJdbcCall(this.dataSource).withProcedureName("sp_addfmcdata");
		this.sp_downloadfmcdatabymaxid = new SimpleJdbcCall(this.dataSource)
				.withProcedureName("sp_downloadfmcdatabymaxid");
		this.sp_downloadfmcdata = new SimpleJdbcCall(this.dataSource).withProcedureName("sp_downloadfmcdata");
		this.sp_downloadFireBaseMessagesByMobNonew = new SimpleJdbcCall(this.dataSource)
				.withProcedureName("sp_downloadFireBaseMessagesByMobNonew");

	}

	/*****************************
	 * Add addfmc Details Detail
	 * 
	 * @throws Exception
	 ********************************/
	public Map<String, Object> addfmcdata(FcmModel fcmModel) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		int maxId = 0;
		int status = 0;
		String uid = encry.decrypt(fcmModel.getUid());
		String token = encry.decrypt(fcmModel.getToken());

		try {
			SqlParameterSource parameter = new MapSqlParameterSource().addValue("i_tbid", fcmModel.getTbid())
					.addValue("i_fcmid", fcmModel.getFcmid()).addValue("i_appkeyword", fcmModel.getAppkeyword())
					.addValue("i_localtb_id", fcmModel.getLocaltb_id()).addValue("i_mobileno", fcmModel.getMobileno())
					.addValue("i_uid", uid).addValue("i_deviceid", fcmModel.getDeviceid()).addValue("i_token", token)
					.addValue("i_createdby", fcmModel.getCreatedby())
					.addValue("i_modifiedby", fcmModel.getModifiedby());

			map = sp_addfmcdata.execute(parameter);
			status = (int) map.get("o_status");
			maxId = (int) map.get("o_maxid");
			System.out.println("maxId" + maxId);
			if (maxId != 0)
				map = downloadfmcdatabymaxid(maxId);
		} catch (Exception e) {
			System.out.println(e);
		}
		map.put("status", status);
		System.out.println("map............" + map);
		return map;
	}

	/*****************************
	 * downloadfmcdatabymaxid
	 * 
	 * @throws Exception
	 ********************************/
	public Map<String, Object> downloadfmcdatabymaxid(int id) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			SqlParameterSource in = new MapSqlParameterSource().addValue("i_maxid", id);
			map = sp_downloadfmcdatabymaxid.execute(in);
			System.out.println(map);
			System.out.println(map.get("o_status"));
		} catch (Exception e) {
			System.out.println(e);
		}

		return map;
	}

	/*****************************
	 * downloadfmcdata
	 * 
	 * @throws Exception
	 ********************************/
	public Map<String, Object> downloadfmcdata(String uid) throws Exception {
		String orguniqueid1 = encry.decrypt(uid);
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			SqlParameterSource in = new MapSqlParameterSource().addValue("i_orguniqueid", orguniqueid1);
			map = sp_downloadfmcdata.execute(in);
			System.out.println(map);
			System.out.println(map.get("o_status"));
		} catch (Exception e) {
			System.out.println(e);
		}

		return map;
	}

	public List<String> downloadFireBaseMessagesByMobNo(FcmModel fireBaseDataModel) {
		Map<String, Object> map = new HashMap<String, Object>();
		List<String> fcmList = new ArrayList<String>();
		int cnt = 0;
		String o_fcmId = null;
		try {
			System.out.println("fireBaseDataModel.getUserMobNoArr()....." + fireBaseDataModel.getUserMobNoArr());
			ArrayList<String> list = fireBaseDataModel.getUserMobNoArr();
			for (int i = 0; i < list.size(); i++) {
				SqlParameterSource in = new MapSqlParameterSource().addValue("i_uid", list.get(i));
				map = sp_downloadFireBaseMessagesByMobNonew.execute(in);
				json = objectMapper.writeValueAsString(map);
				JSONObject obj = new JSONObject(json);
				JSONArray data = obj.getJSONArray("#result-set-1");
				JSONObject jObj = data.getJSONObject(0);
				fcmList.add(jObj.getString("fcmid"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return fcmList;
	}

	public String downloadFireBaseMessages(String uid) {
		System.out.println("hello............" + uid);
		Map<String, Object> map = new HashMap<String, Object>();
		String fcmList = null;
		int cnt = 0;
		String o_fcmId = null;
		try {
			System.out.println("try..................");

			String sql = "select fcmid from tbl_fcmdetails where mobileno = ?";
			fcmList = jdbcTemplate.queryForObject(sql, String.class, uid);
			
			
//			SqlParameterSource in = new MapSqlParameterSource().addValue("i_uid", uid);
//			map = sp_downloadFireBaseMessagesByMobNonew.execute(in);
//			json = objectMapper.writeValueAsString(map);
//			JSONObject obj = new JSONObject(json);
//			JSONArray data = obj.getJSONArray("#result-set-1");
//			JSONObject jObj = data.getJSONObject(0);
//			fcmList = jObj.getString("fcmid");
			System.out.println("fcmList..............." + fcmList);

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e);
		}
		return fcmList;
	}

	// **Download FireBase Response Data
	public Map<String, Object> downloadFireBaseMessagesByMobNo(String toMobileNo) {

		SqlParameterSource in = new MapSqlParameterSource().addValue("i_usermobno", toMobileNo);
		Map<String, Object> simpleJdbcCallResult = sp_downloadFireBaseMessagesByMobNonew.execute(in);
		System.out.println("simpleJdbcCallResult....." + simpleJdbcCallResult);
		return simpleJdbcCallResult;
	}

	public CompletableFuture<String> send(HttpEntity<String> entity) {

		RestTemplate restTemplate = new RestTemplate();

		/**
		 * https://fcm.googleapis.com/fcm/send Content-Type:application/json
		 * Authorization:key=FIREBASE_SERVER_KEY
		 */

		ArrayList<ClientHttpRequestInterceptor> interceptors = new ArrayList<>();
		interceptors.add(new HeaderRequestInterceptor("Authorization", "key=" + FIREBASE_SERVER_KEY));
		// interceptors.add(new HeaderRequestInterceptor("Authorization", "id="
		// + FIREBASE_SENDER_ID));
		interceptors.add(new HeaderRequestInterceptor("Content-Type", "application/json"));
		restTemplate.setInterceptors(interceptors);
		String firebaseResponse = restTemplate.postForObject(FIREBASE_API_URL, entity, String.class);
		System.out.println("after................" + CompletableFuture.completedFuture(firebaseResponse).toString());
		return CompletableFuture.completedFuture(firebaseResponse);
	}
	// **
}
