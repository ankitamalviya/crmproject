package com.dao;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;
import com.model.LeadSourceModel;
import com.security.EncryptDecryptStringWithAES;

@Repository
public class LeadSourceDao {
	private JdbcTemplate jdbcTemplate;
	private DataSource dataSource;
	private SimpleJdbcCall sp_addleadsource, sp_downloadleadsource, sp_downloadleadsourcebymaxid;

	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
		this.dataSource = dataSource;
		this.sp_addleadsource = new SimpleJdbcCall(this.dataSource).withProcedureName("sp_addleadsource");
		this.sp_downloadleadsource = new SimpleJdbcCall(this.dataSource).withProcedureName("sp_downloadleadsource");
		this.sp_downloadleadsourcebymaxid = new SimpleJdbcCall(this.dataSource)
				.withProcedureName("sp_downloadleadsourcebymaxid");
	}

	/*****************************
	 * Add Lead Details Detail
	 ********************************/
	EncryptDecryptStringWithAES encry = new EncryptDecryptStringWithAES();

	public Map<String, Object> addleadsource(LeadSourceModel leadSourceModel) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		int maxId = 0;
		int status = 0;
		String uid = encry.decrypt(leadSourceModel.getUid());
		String orguniqueid = encry.decrypt(leadSourceModel.getOrguniqueid());

		try {
			SqlParameterSource parameter = new MapSqlParameterSource()
					.addValue("i_sourceid", leadSourceModel.getSourceid())
					.addValue("i_sourcename", leadSourceModel.getSourcename()).addValue("i_uid", uid)
					.addValue("i_orguniqueid", orguniqueid).addValue("i_status", leadSourceModel.getStatus())
					.addValue("i_createdby", leadSourceModel.getCreatedby())
					.addValue("i_deviceid", leadSourceModel.getDeviceid())
					.addValue("i_modifiedby", leadSourceModel.getModifiedby());

			map = sp_addleadsource.execute(parameter);
			status = (int) map.get("o_status");
			maxId = (int) map.get("o_maxid");
			if (maxId != 0)
				map = downloadleadsourcebymaxid(maxId);
		} catch (Exception e) {
			System.out.println(e);
		}
		map.put("status", status);
		return map;
	}

	public Map<String, Object> downloadleadsource(String orguniqueid) throws Exception {
		System.out.println("hello......................");
		String orguniqueid1 = encry.decrypt(orguniqueid);
		System.out.println("orguniqueid1" + orguniqueid1);
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			SqlParameterSource in = new MapSqlParameterSource().addValue("i_orguniqueid", orguniqueid1);
			map = sp_downloadleadsource.execute(in);

		} catch (Exception e) {
			System.out.println(e);
		}
		System.out.println("downloadleadsource....." + map);
		return map;
	}

	public Map<String, Object> downloadleadsourcebymaxid(int id) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			SqlParameterSource in = new MapSqlParameterSource().addValue("i_maxid", id);
			map = sp_downloadleadsourcebymaxid.execute(in);
			System.out.println(map);
			System.out.println(map.get("o_status"));
		} catch (Exception e) {
			System.out.println(e);
		}

		return map;
	}

}
