package com.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import com.model.LeadStageModel;
import com.model.LeadTagsModel;

@Repository
public class LeadTagsDao {

	private JdbcTemplate jdbcTemplate;
	private DataSource dataSource;
	private SimpleJdbcCall sp_addleadtags, sp_downloadleadtags,sp_downloadleadtagsswithmaxid;

	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
		this.dataSource = dataSource;
		this.sp_addleadtags = new SimpleJdbcCall(this.dataSource).withProcedureName("sp_addleadtags");
		this.sp_downloadleadtags = new SimpleJdbcCall(this.dataSource).withProcedureName("sp_downloadleadtags");
		this.sp_downloadleadtagsswithmaxid = new SimpleJdbcCall(this.dataSource).withProcedureName("sp_downloadleadtagsswithmaxid");
	}

	/*****************************
	 * Add Lead Details Detail
	 * @param leadid 
	 ********************************/

	public Map<String, Object> addleadtags(LeadTagsModel leadTagsModel, int leadid) {
//		LeadTagsModel leadTagsModel = list.get(0);
		Map<String, Object> map = new HashMap<String, Object>();
		int maxId = 0;
		int status = 0;

		try {
			SqlParameterSource parameter = new MapSqlParameterSource()
					.addValue("i_leadtagid", leadTagsModel.getLeadtagid())
					.addValue("i_leadid", leadid).addValue("i_tagid", leadTagsModel.getTagid())
//					.addValue("i_leadtagname", leadTagsModel.getLeadtagname())
//					.addValue("i_colour", leadTagsModel.getColor())
					.addValue("i_deviceid", leadTagsModel.getDeviceid())
					.addValue("i_createdby", leadTagsModel.getCreatedby())
					.addValue("i_modifiedby", leadTagsModel.getModifiedby())
					.addValue("i_status", leadTagsModel.getStatus());

			map = sp_addleadtags.execute(parameter);
			status = (int) map.get("o_status");
			
		} catch (Exception e) {
			System.out.println(e);
		}
		map.put("status", status);
		return map;
	}

	public Map<String, Object> downloadleadtagsswithmaxid(int id) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			SqlParameterSource in = new MapSqlParameterSource().addValue("i_maxid", id);
			map = sp_downloadleadtagsswithmaxid.execute(in);
			System.out.println(map);
			System.out.println(map.get("o_status"));
		} catch (Exception e) {
			System.out.println(e);
		}

		return map;
	}

//	public Map<String, Object> addleadtags(LeadTagsModel leadTagsModel) {
//		Map<String, Object> map = new HashMap<String, Object>();
//		int maxId = 0;
//		int status = 0;
//
//		try {
//			SqlParameterSource parameter = new MapSqlParameterSource()
//					.addValue("i_leadtagid", leadTagsModel.getLeadtagid())
//					.addValue("i_leadid", leadTagsModel.getLeadid()).addValue("i_tagid", leadTagsModel.getTagid())
//					.addValue("i_colour", leadTagsModel.getColor()).addValue("i_deviceid", leadTagsModel.getDeviceid())
//					.addValue("i_createdby", leadTagsModel.getCreatedby())
//					.addValue("i_modifiedby", leadTagsModel.getModifiedby())
//					.addValue("i_status", leadTagsModel.getStatus());
//
//			map = sp_addleadtags.execute(parameter);
//			System.out.println(map.get("o_status"));
//		} catch (Exception e) {
//			System.out.println(e);
//		}
//
//		return map;
//	}

	public Map<String, Object> downloadleadstage(int leadid) {
		System.out.println("leadid"+ leadid);
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			SqlParameterSource in = new MapSqlParameterSource().addValue("i_leadid", leadid);
			map = sp_downloadleadtags.execute(in);
			
		} catch (Exception e) {
			System.out.println(e);
		}

		return map;
	}

}
