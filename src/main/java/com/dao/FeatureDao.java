package com.dao;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import com.model.FeatureModel;

@Repository
public class FeatureDao {
	private JdbcTemplate jdbcTemplate;
	private DataSource dataSource;
	private SimpleJdbcCall sp_savefeaturedetails, sp_downloadfeaturedetailBymaxid;

	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
		this.dataSource = dataSource;
		this.sp_savefeaturedetails = new SimpleJdbcCall(this.dataSource).withProcedureName("sp_savefeaturedetails");
		this.sp_downloadfeaturedetailBymaxid = new SimpleJdbcCall(this.dataSource)
				.withProcedureName("sp_downloadfeaturedetailBymaxid");

	}

	public Map<String, Object> savefeaturedetails(FeatureModel featureModel) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		int maxId = 0;
		int status = 0;

		try {
			SqlParameterSource parameter = new MapSqlParameterSource()
					.addValue("i_fid", featureModel.getFid())
					.addValue("i_amount", featureModel.getAmount())
					.addValue("i_duration", featureModel.getDuration())
					.addValue("i_discount", featureModel.getDiscount())
					.addValue("i_initialamount", featureModel.getInitialamount())
					.addValue("i_finalamount", featureModel.getFinalamount())
					.addValue("i_createdby", featureModel.getCreatedby())
					.addValue("i_modifiedby", featureModel.getModifiedby());
				
			map = sp_savefeaturedetails.execute(parameter);
			status = (int) map.get("o_status");
			System.out.println("status" + status);
			maxId = (int) map.get("o_maxid");
			System.out.println(maxId);
			if (maxId != 0)
				map = downloadfeaturedetailBymaxid(maxId);
		} catch (Exception e) {
			System.out.println(e);
		}
		map.put("status", status);
		return map;
	}

	private Map<String, Object> downloadfeaturedetailBymaxid(int id) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			SqlParameterSource in = new MapSqlParameterSource().addValue("i_maxid", id);
			map = sp_downloadfeaturedetailBymaxid.execute(in);
			System.out.println(map);
			System.out.println(map.get("o_status"));
		} catch (Exception e) {
			System.out.println(e);
		}

		return map;
	}

}
