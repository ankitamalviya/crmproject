package com.dao;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.Map.Entry;
import java.util.Random;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import com.controller.OtpMessageTest;
import com.model.AppVersionDetails;
import com.model.CRMmodel;
import com.model.CreateOrganization;
import com.model.LastSeenModel;
import com.model.MemberDetailsOfSameOrg;
import com.model.OrganizationMemberModel;
import com.security.EncryptDecryptStringWithAES;

@Repository
public class BusinessDAO {

	/****** Static variable declaration ********/
	public String otp = "";
	public String space = "";
	private JdbcTemplate jdbcTemplate;
	private DataSource dataSource;
	private SimpleJdbcCall sp_downloadRegisterUserData, getOtpPasswordnew, sp_createOrganizationData, sp_downloadToken,
			forgetPassword, OTPVerificationNew, userRegistration, downloadCreatedOrganizationData,
			sp_downloadempstrength, sp_addmemberdata, sp_downloadleadorganizationdetails, sp_resendotp,
			sp_downloadleadorganizationmemberlist, sp_downloadorganiztionwithorgcode, sp_chenckorgcodeavailability,
			sp_addorganizationwithoutname, sp_downloadmemberdetailswithmaxid, sp_downloadTokenNew, sp_joinmember,
			sp_downloadorganiztiondetailswithuid, sp_downloadorganizationbymaxid, sp_checkdailylogin,
			sp_deletejoinmember, sp_savelastseendetails, sp_checkappversiondetails, sp_generaterandomno,
			OTPVerificationFireBase, sp_downloadleadinfowithleadmobileno;
	private String userMobile = "";
	private String refrenceMobile = "";
	private String password = "";
	public String token = "";
	public String UID = "";
	public String orguniqueid = "";
	boolean passwordExist = false;
	boolean otpExist = false;
	private static final DateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssS");
	OtpMessageTest otpMessageTest = new OtpMessageTest();

	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
		this.dataSource = dataSource;
		this.getOtpPasswordnew = new SimpleJdbcCall(this.dataSource).withProcedureName("getOtpPasswordnew");
		this.OTPVerificationNew = new SimpleJdbcCall(this.dataSource).withProcedureName("OTPVerificationNew");
		this.userRegistration = new SimpleJdbcCall(this.dataSource).withProcedureName("userRegistration");
		this.sp_downloadRegisterUserData = new SimpleJdbcCall(this.dataSource)
				.withProcedureName("sp_downloadRegisterUserData");
		this.forgetPassword = new SimpleJdbcCall(this.dataSource).withProcedureName("forgetPassword");
		this.sp_downloadToken = new SimpleJdbcCall(this.dataSource).withProcedureName("sp_downloadToken");
		this.sp_createOrganizationData = new SimpleJdbcCall(this.dataSource)
				.withProcedureName("sp_createOrganizationData");
		this.downloadCreatedOrganizationData = new SimpleJdbcCall(this.dataSource)
				.withProcedureName("downloadCreatedOrganizationData");
		this.sp_downloadempstrength = new SimpleJdbcCall(this.dataSource).withProcedureName("sp_downloadempstrength");
		this.sp_addmemberdata = new SimpleJdbcCall(this.dataSource).withProcedureName("sp_addmemberdata");
		this.sp_resendotp = new SimpleJdbcCall(this.dataSource).withProcedureName("sp_resendotp");
		this.sp_createOrganizationData = new SimpleJdbcCall(this.dataSource)
				.withProcedureName("sp_createOrganizationData");
		this.downloadCreatedOrganizationData = new SimpleJdbcCall(this.dataSource)
				.withProcedureName("downloadCreatedOrganizationData");
		this.sp_addmemberdata = new SimpleJdbcCall(this.dataSource).withProcedureName("sp_addmemberdata");
		this.sp_downloadleadorganizationdetails = new SimpleJdbcCall(this.dataSource)
				.withProcedureName("sp_downloadleadorganizationdetails");
		this.sp_downloadleadorganizationmemberlist = new SimpleJdbcCall(this.dataSource)
				.withProcedureName("sp_downloadleadorganizationmemberlist");
		this.sp_downloadorganiztionwithorgcode = new SimpleJdbcCall(this.dataSource)
				.withProcedureName("sp_downloadorganiztionwithorgcode");
		this.sp_chenckorgcodeavailability = new SimpleJdbcCall(this.dataSource)
				.withProcedureName("sp_chenckorgcodeavailability");
		this.sp_addorganizationwithoutname = new SimpleJdbcCall(this.dataSource)
				.withProcedureName("sp_addorganizationwithoutname");
		this.sp_downloadmemberdetailswithmaxid = new SimpleJdbcCall(this.dataSource)
				.withProcedureName("sp_downloadmemberdetailswithmaxid");
		this.sp_downloadTokenNew = new SimpleJdbcCall(this.dataSource).withProcedureName("sp_downloadTokenNew");
		this.sp_joinmember = new SimpleJdbcCall(this.dataSource).withProcedureName("sp_joinmember");
		this.sp_downloadorganiztiondetailswithuid = new SimpleJdbcCall(this.dataSource)
				.withProcedureName("sp_downloadorganiztiondetailswithuid");
		this.sp_downloadorganizationbymaxid = new SimpleJdbcCall(this.dataSource)
				.withProcedureName("sp_downloadorganizationbymaxid");
		this.sp_checkdailylogin = new SimpleJdbcCall(this.dataSource).withProcedureName("sp_checkdailylogin");
		this.sp_deletejoinmember = new SimpleJdbcCall(this.dataSource).withProcedureName("sp_deletejoinmember");
		this.sp_savelastseendetails = new SimpleJdbcCall(this.dataSource).withProcedureName("sp_savelastseendetails");
		this.sp_checkappversiondetails = new SimpleJdbcCall(this.dataSource)
				.withProcedureName("sp_checkappversiondetails");
		this.sp_generaterandomno = new SimpleJdbcCall(this.dataSource).withProcedureName("sp_generaterandomno");
		this.OTPVerificationFireBase = new SimpleJdbcCall(this.dataSource).withProcedureName("OTPVerificationFireBase");
		this.sp_downloadleadinfowithleadmobileno = new SimpleJdbcCall(this.dataSource).withProcedureName("sp_downloadleadinfowithleadmobileno");
	}

	public String returnOtp() {
		Integer value = (int) (Math.random() * 9999) + 1001;
		otp = value.toString();
		return otp;
	}

	public String password() {
		Integer value1 = (int) (Math.random() * 9999) + 1001;
		password = value1.toString();
		return password;
	}

	public String oauthToken() {
		token = UUID.randomUUID().toString();
		return token;
	}

	public String orgUniqueCode() {
		orguniqueid = UUID.randomUUID().toString();
		return orguniqueid;
	}

	/********** REGISTER TO MYSOCIETY APP ************/

	/******* NEW REGISTRATION *********/

	// @SuppressWarnings({ "unchecked", "rawtypes", "unused" })
	// public Map<String, Object> register(CRMmodel model) throws Exception {
	//
	// /****** FOR GENERATING OTP *****/
	//
	// Integer value = (int) ((Math.random() * 9999) + 1001);
	// otp = value.toString();
	//
	// /********* FOR GENERATING UID *********/
	//
	// UID = UUID.randomUUID().toString();
	//
	// /******* FOR GENERATING PASSWORD *******/
	//
	// Integer value1 = (int) (Math.random() * 9999) + 1001;
	// password = value1.toString();
	//
	// /********* FOR GENERATING TOKEN *********/
	//
	// token = UUID.randomUUID().toString();
	//
	// Map<String, Object> map = new HashMap<String, Object>();
	// int maxid = 0;
	// int status = 0;
	// String dbdeviceid = "";
	// String dbOTP = "";
	// String dbStatus = "";
	// String spMsg = "";
	//
	// try {
	// SqlParameterSource parameter = new
	// MapSqlParameterSource().addValue("i_uid", UID)
	// .addValue("i_countrycode", model.getCountrycode())
	// .addValue("i_usermobileno",
	// model.getUserMobileNo()).addValue("i_emailid", model.getEmailid())
	// .addValue("i_appkeyword", model.getAppkeyword()).addValue("i_firstname",
	// model.getFirstName())
	// .addValue("i_lastname", model.getLastName()).addValue("i_address",
	// model.getAddress())
	// .addValue("i_country", model.getCountry()).addValue("i_zipcode",
	// model.getZipcode())
	// .addValue("i_lat", model.getLat()).addValue("i_longi", model.getLongi())
	// // .addValue("i_otpstatus", model.getOtpstatus())
	// .addValue("i_state", model.getState())
	// // .addValue("i_companyname", model.getCompanyname())
	// .addValue("i_deviceid", model.getDeviceid())
	// // .addValue("i_usersimserialno",
	// // model.getUserSimSerialNo())
	// // .addValue("i_district", model.getDistrict())
	// .addValue("i_otp", otp).addValue("i_password",
	// password).addValue("i_token", token);
	//
	// Map out = userRegistration.execute(parameter);
	// System.out.println(out);
	// maxid = (Integer) out.get("maxid");
	// status = (Integer) out.get("status");
	// dbdeviceid = (String) out.get("dbdeviceid");
	// dbOTP = (String) out.get("dbotp");
	// dbStatus = (String) out.get("dbstatus");
	// spMsg = (String) out.get("msg");
	//
	// } catch (NullPointerException e) {
	// }
	//
	// if (status == 106) {
	// map.put("maxid", maxid);
	// map.put("status", status);
	// String msg = " Your OTP for mobile no " + model.getUserMobileNo() + " is
	// " + otp + " ";
	// otpMessageTest.sendSms(model.getUserMobileNo(), msg);
	// } else if (status == 105) {
	// map.put("maxid", maxid);
	// map.put("status", status);
	// String msg = "Your OTP for mobile no " + model.getUserMobileNo() + " is "
	// + dbOTP + " ";
	// otpMessageTest.sendSms(model.getUserMobileNo(), msg);
	// } else if (status == 104) {
	// map.put("maxid", maxid);
	// map.put("status", status);
	// } else if (status == 103) {
	// map.put("maxid", maxid);
	// map.put("status", status);
	// String msg = "Your OTP for mobile no " + model.getUserMobileNo() + " is "
	// + otp + " ";
	// otpMessageTest.sendSms(model.getUserMobileNo(), msg);
	// }
	// return map;
	// }
	public Map<String, Object> register(CRMmodel model) throws Exception {

		/****** FOR GENERATING OTP *****/

		Integer value = (int) ((Math.random() * 8998) + 1001);
		otp = value.toString();

		/********* FOR GENERATING UID *********/

		UID = UUID.randomUUID().toString();

		/******* FOR GENERATING PASSWORD *******/

		Integer value1 = (int) (Math.random() * 9999) + 1001;
		password = value1.toString();

		/********* FOR GENERATING TOKEN *********/

		token = UUID.randomUUID().toString();

		Map<String, Object> map = new HashMap<String, Object>();
		int maxid = 0;
		int status = 0;
		String dbdeviceid = "";
		String dbOTP = "";
		String dbStatus = "";
		String spMsg = "";

		try {
			SqlParameterSource parameter = new MapSqlParameterSource().addValue("i_uid", UID)
					.addValue("i_countrycode", model.getCountrycode())
					.addValue("i_usermobileno", model.getUserMobileNo()).addValue("i_emailid", model.getEmailid())
					.addValue("i_appkeyword", model.getAppkeyword()).addValue("i_firstname", model.getFirstName())
					.addValue("i_lastname", model.getLastName()).addValue("i_address", model.getAddress())
					.addValue("i_country", model.getCountry()).addValue("i_zipcode", model.getZipcode())
					.addValue("i_lat", model.getLat()).addValue("i_longi", model.getLongi())
					// .addValue("i_otpstatus", model.getOtpstatus())
					.addValue("i_state", model.getState())
					// .addValue("i_companyname", model.getCompanyname())
					.addValue("i_deviceid", model.getDeviceid())

					.addValue("i_firebaseuuid", model.getFirebaseuuid())
					.addValue("i_firebasecreateddate", model.getFirebasecreateddate())
					.addValue("i_signupdate", model.getSignupdate())
					.addValue("i_verificationid", model.getVerificationid())

					// .addValue("i_usersimserialno",
					// model.getUserSimSerialNo())
					// .addValue("i_district", model.getDistrict())
					.addValue("i_otp", otp).addValue("i_password", password).addValue("i_token", token);

			Map out = userRegistration.execute(parameter);
			System.out.println(out);
			maxid = (Integer) out.get("maxid");
			status = (Integer) out.get("status");
			dbdeviceid = (String) out.get("dbdeviceid");
			dbOTP = (String) out.get("dbotp");
			dbStatus = (String) out.get("dbstatus");
			spMsg = (String) out.get("msg");

		} catch (NullPointerException e) {
		}

		if (status == 106) {
			map.put("maxid", maxid);
			map.put("status", status);
			String msg = " Your OTP for mobile no " + model.getUserMobileNo() + " is " + otp + " ";
			otpMessageTest.sendSms(model.getUserMobileNo(), msg);
		} else if (status == 108) {
			map.put("maxid", maxid);
			map.put("status", status);
			/*
			 * String msg = "Your OTP for mobile no " + model.getUserMobileNo()
			 * + " is " + dbOTP + " ";
			 * otpMessageTest.sendSms(model.getUserMobileNo(), msg);
			 */
		} else if (status == 105) {
			map.put("maxid", maxid);
			map.put("status", status);
			String msg = "Your OTP for mobile no " + model.getUserMobileNo() + " is " + dbOTP + " ";
			otpMessageTest.sendSms(model.getUserMobileNo(), msg);
		} else if (status == 104) {
			map.put("maxid", maxid);
			map.put("status", status);
		} else if (status == 103) {
			map.put("maxid", maxid);
			map.put("status", status);
			String msg = "Your OTP for mobile no " + model.getUserMobileNo() + " is " + otp + " ";
			otpMessageTest.sendSms(model.getUserMobileNo(), msg);
		}
		return map;
	}

	public Map<String, Object> getUserRegisterData(String userMobileNo) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("i_usermobileno", userMobileNo);

		SqlParameterSource in = new MapSqlParameterSource(map);

		Map<String, Object> simpleJdbcCallResult = sp_downloadRegisterUserData.execute(in);

		Iterator<Entry<String, Object>> it = simpleJdbcCallResult.entrySet().iterator();

		while (it.hasNext()) {
			Map.Entry<String, Object> entry = (Map.Entry<String, Object>) it.next();
			String key = (String) entry.getKey();
			Object value = (Object) entry.getValue();
			System.out.println("Key: " + key);
			System.out.println("Value: " + value);
		}
		return simpleJdbcCallResult;
	}

	/************ OTP VERIFICATION ***************/
	EncryptDecryptStringWithAES encry = new EncryptDecryptStringWithAES();

	public int OTPVerification(List<CRMmodel> data) throws Exception {
		CRMmodel model = data.get(0);
		String uid = encry.decrypt(model.getUid());
		System.out.println(uid);
		int status = 0;
		int status1 = 0;
		String DbPassword = "";
		try {
			SqlParameterSource parameter = new MapSqlParameterSource().addValue("i_uid", uid)
					.addValue("i_deviceid", model.getDeviceid()).addValue("i_otp", model.getOtp());
			Map out = getOtpPasswordnew.execute(parameter);
			System.out.println(out);
			// DbPassword = (String) out.get("i_userpassword");
			status1 = (Integer) out.get("status");
			System.out.println("status1" + status1);
		} catch (EmptyResultDataAccessException e) {
		}

		try {

			SqlParameterSource parameter = new MapSqlParameterSource().addValue("i_uid", uid)
					.addValue("i_deviceid", model.getDeviceid()).addValue("i_otp", model.getOtp());
			Map out = OTPVerificationNew.execute(parameter);
			status = (Integer) out.get("status");
			System.out.println("out stuts" + status);
			if (status == 106) {

				System.out.println("ststus" + status);
				String msg = " Your Password for mobile no " + model.getUserMobileNo() + " is " + DbPassword + " ";
				otpMessageTest.sendSms(model.getUserMobileNo(), msg);
			}
		} catch (NullPointerException e) {
		}
		return status;
	}

	public List<CRMmodel> getData(List<CRMmodel> data) {
		CRMmodel model = data.get(0);
		List<CRMmodel> societies = null;

		try {
			societies = jdbcTemplate.query(
					"SELECT tbl_registration.uid,"
							+ "tbl_registration.userMobileNo ,tbl_registration.emailid,tbl_registration.deviceid, "
							+ " tbl_registration.rId from tbl_registration "
							+ "where tbl_registration.uid=? &&  tbl_registration.deviceid=? ",
					new Object[] { model.getUserMobileNo(), model.getDeviceid() },
					new BeanPropertyRowMapper<CRMmodel>(CRMmodel.class));
		} catch (EmptyResultDataAccessException e) {

		}
		return societies;
	}

	/************ get token for authentication *************/

	public String getTokenForAuthentication(String userMobileNo) {

		String DBToken = "";

		try {

			String sql = "Select token from tbl_login where userMobileNo=?";
			DBToken = jdbcTemplate.queryForObject(sql, String.class, userMobileNo);

		} catch (EmptyResultDataAccessException e) {
			// TODO: handle exception
		}

		return DBToken;
	}

	/******* resend otp ********/

	public Map<String, Object> resendOTP(String uid, String deviceid) {
		Map<String, Object> map = new HashMap<String, Object>();

		try {
			SqlParameterSource in = new MapSqlParameterSource().addValue("i_uid", uid).addValue("i_deviceid", deviceid);

			Map out = sp_resendotp.execute(in);

			if ((out.get("o_dbotp")).equals("") || out.get("o_dbotp") == null) {
				map.put("status", 400);
			} else {
				String msg = "";
				String userMobileNo1 = "";
				String otp = (String) out.get("o_dbotp");
				userMobileNo1 = (String) out.get("o_mobileno");
				map.put("otp", out.get("o_dbotp"));
				map.put("regid", out.get("o_maxid"));
				map.put("userMobileNo", out.get("o_mobileno"));
				map.put("status", 200);

				msg = otp + " OTP for verification of CueLab installation.";

				System.out.println(msg);
				System.out.println(userMobileNo1);
				otpMessageTest.sendSms(userMobileNo1, msg);
				// sender.sendSMS(userMobileNo, msg);
			}

		} catch (Exception e) {
			System.out.println(e);
		}

		return map;
	}

	public String forgetPassword(String userMobileNo) {

		String msg = "Your-Password-is:";
		String password = "";

		SqlParameterSource in = new MapSqlParameterSource().addValue("i_userMobileNo", userMobileNo);

		Map out = forgetPassword.execute(in);

		int status = (int) out.get("status");

		if (status == 106) {
			password = (String) out.get("i_password");
			passwordExist = true;
			String passwdMsg = "Your password for mobile no " + userMobileNo + " is " + password + " ";
			otpMessageTest.sendSms(userMobileNo, passwdMsg);
		} else if (status == 107) {
			passwordExist = false;
		}

		return password;

	}

	/*
	 * public Map<String, Object> downloadTokenId(String uId) {
	 * 
	 * Map<String, Object> result = new HashMap<String, Object>();
	 * 
	 * SqlParameterSource in = new MapSqlParameterSource().addValue("i_uId",
	 * uId); // .addValue("i_imeiNo", imeiNo); Map out =
	 * sp_downloadToken.execute(in);
	 * 
	 * int status = (int) out.get("o_status"); System.out.println((String)
	 * out.get("o_token"));
	 * 
	 * if (status == 106) { String token = (String) out.get("o_token");
	 * result.put("Status", status); result.put("Token", token); } else if
	 * (status == 107) { result.put("Status", status); }
	 * 
	 * return result; }
	 */

	public Map<String, Object> downloadCreateorganizationData(Integer maxId, String userMobileNo) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("i_maxId", maxId);
		map.put("i_userMobileNo", userMobileNo);

		SqlParameterSource in = new MapSqlParameterSource(map);

		Map<String, Object> simpleJdbcCallResult = downloadCreatedOrganizationData.execute(in);

		Iterator<Entry<String, Object>> it = simpleJdbcCallResult.entrySet().iterator();

		while (it.hasNext()) {
			Map.Entry<String, Object> entry = (Map.Entry<String, Object>) it.next();
			String key = (String) entry.getKey();
			Object value = (Object) entry.getValue();
			System.out.println("Key: " + key);
			System.out.println("Value: " + value);
		}
		return simpleJdbcCallResult;
	}

	public Map<String, Object> downloadempStrength() {
		Map<String, Object> map = new HashMap<String, Object>();
		SqlParameterSource in = new MapSqlParameterSource();
		map = sp_downloadempstrength.execute(in);
		return map;
	}

	public Map<String, Object> createorganization(CreateOrganization data) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		/******* FOR OrganizationUnique Code *******/
		orguniqueid = UUID.randomUUID().toString();
		String uid = encry.decrypt(data.getUid());
		String createdby = encry.decrypt(data.getCreatedBy());
		int status = 0;
		int maxId = 0;

		try {
			SqlParameterSource in = new MapSqlParameterSource().addValue("i_orgid", data.getOrgid())
					.addValue("i_orgname", data.getOrgname()).addValue("i_orgcode", data.getOrgcode())
					.addValue("i_orguniqueid", orguniqueid).addValue("i_uid", uid).addValue("i_city", data.getCity())
					.addValue("i_zipcode", data.getZipcode()).addValue("i_udf1", data.getUdf1())
					.addValue("i_state", data.getState()).addValue("i_country", data.getCountry())
					.addValue("i_userMobileNo", data.getUserMobileNo()).addValue("i_logo", data.getLogo())
					.addValue("i_domain", data.getDomain()).addValue("i_empstrength", data.getEmpstrength())
					.addValue("i_longitude", data.getLongitude()).addValue("i_latitude", data.getLatitude())
					.addValue("i_deviceid", data.getDeviceid()).addValue("i_createdBy", createdby)
					.addValue("i_modifiedby", data.getModifiedBy());

			map = sp_createOrganizationData.execute(in);
			status = (int) map.get("o_status");
			maxId = (int) map.get("i_maxid");
			if (maxId != 0)
				map = downloadorganizationbymaxid(maxId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		map.put("status", status);
		return map;
	}

	private Map<String, Object> downloadorganizationbymaxid(int maxId) {
		Map<String, Object> map = null;
		SqlParameterSource in = null;
		try {
			in = new MapSqlParameterSource().addValue("i_maxid", maxId);
			map = sp_downloadorganizationbymaxid.execute(in);
			System.out.println(map);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;

	}

	public Map<String, Object> addorganizationwithoutname(CreateOrganization data) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		/******* FOR OrganizationUnique Code *******/
		int status = 0;
		int maxId = 0;
		String orguniqueid = UUID.randomUUID().toString();
		String uid = encry.decrypt(data.getUid());
		String createdby = encry.decrypt(data.getCreatedBy());

		String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		StringBuilder orgcode1 = new StringBuilder();
		Random rnd = new Random();
		while (orgcode1.length() < 10) { // length of the random string.
			int index = (int) (rnd.nextFloat() * SALTCHARS.length());
			orgcode1.append(SALTCHARS.charAt(index));
		}
		String orgcodeStr = orgcode1.toString();

		StringBuilder orgname1 = new StringBuilder();
		while (orgname1.length() < 8) { // length of the random string.
			int index = (int) (rnd.nextFloat() * SALTCHARS.length());
			orgname1.append(SALTCHARS.charAt(index));
		}
		String orgnameStr = orgname1.toString();

		try {
			SqlParameterSource in = new MapSqlParameterSource().addValue("i_orgid", data.getOrgid())
					.addValue("i_orgname", orgnameStr).addValue("i_orgcode", orgcodeStr)
					.addValue("i_orguniqueid", orguniqueid).addValue("i_uid", uid)
					.addValue("i_longitude", data.getLongitude()).addValue("i_latitude", data.getLatitude())
					.addValue("i_deviceid", data.getDeviceid()).addValue("i_createdBy", createdby)
					.addValue("i_modifiedby", data.getModifiedBy());

			map = sp_addorganizationwithoutname.execute(in);
			status = (int) map.get("o_status");
			maxId = (int) map.get("i_maxid");
			if (maxId != 0)
				map = downloadorganizationbymaxid(maxId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		map.put("status", status);
		return map;
	}

	// public Map<String, Object> addorganizationmembers(OrganizationMemberModel
	// data) {
	// Map<String, Object> map = new HashMap<String, Object>();
	//
	// try {
	// SqlParameterSource in = new
	// MapSqlParameterSource().addValue("i_orgmemberid", data.getOrgmemberid())
	// .addValue("i_orgcode", data.getOrgcode()).addValue("i_orguniqueid",
	// data.getOrguniqueid())
	// .addValue("i_roleId", data.getRoleId()).addValue("i_address",
	// data.getAddress())
	// .addValue("i_usermobileno", data.getUsermobileno()).addValue("i_uid",
	// data.getUid())
	// .addValue("i_emailid", data.getEmailid()).addValue("i_status",
	// data.getStatus())
	// .addValue("i_membername", data.getMembername()).addValue("i_deviceid",
	// data.getDeviceid())
	// .addValue("i_createdBy", data.getCreatedBy()).addValue("i_modifiedby",
	// data.getModifiedby());
	//
	// map = sp_addmemberdata.execute(in);
	// System.out.println(map);
	// } catch (Exception e) {
	//
	// }
	//
	// return map;
	// }

	// public Map<String, Object> addorganizationmembers(OrganizationMemberModel
	// data) throws Exception {
	// Map<String, Object> map = new HashMap<String, Object>();
	// int status = 0;
	// int maxId = 0;
	// String orguniqueid = encry.decrypt(data.getOrguniqueid());
	// String uid = encry.decrypt(data.getUid());
	//
	// try {
	// SqlParameterSource in = new
	// MapSqlParameterSource().addValue("i_orgmemberid", data.getOrgmemberid())
	// .addValue("i_orgcode", data.getOrgcode()).addValue("i_orguniqueid",
	// orguniqueid)
	// .addValue("i_roleId", data.getRoleId()).addValue("i_address",
	// data.getAddress())
	// .addValue("i_usermobileno", data.getUsermobileno()).addValue("i_uid",
	// uid)
	// .addValue("i_emailid", data.getEmailid()).addValue("i_status",
	// data.getStatus())
	// .addValue("i_membername", data.getMembername()).addValue("i_deviceid",
	// data.getDeviceid())
	// .addValue("i_createdBy", data.getCreatedBy()).addValue("i_modifiedby",
	// data.getModifiedby());
	//
	// map = sp_addmemberdata.execute(in);
	// System.out.println("map............................"+map);
	// status = (int) map.get("o_status");
	// maxId = (int) map.get("i_maxid");
	// System.out.println("maxId........."+maxId);
	// if (maxId != 0)
	// map = downloadMemberDetailsById(maxId);
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// map.put("status", status);
	// System.out.println("map......+"+map);
	// return map;
	// }

	public Map<String, Object> addorganizationmembers(OrganizationMemberModel data) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		int status = 0;
		int maxId = 0;
		String orguniqueid = encry.decrypt(data.getOrguniqueid());
		String uid = encry.decrypt(data.getUid());

		try {
			SqlParameterSource in = new MapSqlParameterSource().addValue("i_orgmemberid", data.getOrgmemberid())
					.addValue("i_orgcode", data.getOrgcode()).addValue("i_orguniqueid", orguniqueid)
					.addValue("i_roleId", data.getRoleId()).addValue("i_address", data.getAddress())
					.addValue("i_invitationto", data.getInvitationto()).addValue("i_invitedfrom", data.getInvitedfrom())
					.addValue("i_uid", uid).addValue("i_emailid", data.getEmailid())
					.addValue("i_status", data.getStatus()).addValue("i_membername", data.getMembername())
					.addValue("i_deviceid", data.getDeviceid()).addValue("i_createdBy", data.getCreatedBy())
					.addValue("i_modifiedby", data.getModifiedby());

			map = sp_addmemberdata.execute(in);
			status = (int) map.get("o_status");
			maxId = (int) map.get("i_maxid");
			if (maxId != 0)
				map = downloadMemberDetailsById(maxId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		map.put("status", status);
		return map;
	}

	private Map<String, Object> downloadMemberDetailsById(int maxId) {
		Map<String, Object> map = null;
		SqlParameterSource in = null;
		try {
			in = new MapSqlParameterSource().addValue("i_maxid", maxId);
			map = sp_downloadmemberdetailswithmaxid.execute(in);
			System.out.println(map);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;

	}

	public Map<String, Object> downloadOrganizationDetails(CreateOrganization data) throws Exception {
		String uid1 = encry.decrypt(data.getUid());
		String orguniqueid1 = encry.decrypt(data.getOrguniqueid());
		Map<String, Object> result = new HashMap<String, Object>();

		SqlParameterSource in = new MapSqlParameterSource().addValue("i_uid", uid1).addValue("i_orguniqueid",
				orguniqueid1);
		// .addValue("i_imeiNo", imeiNo);
		result = sp_downloadleadorganizationdetails.execute(in);

		return result;
	}

	public Map<String, Object> downloadOrganizationMemberList(OrganizationMemberModel data) throws Exception {
		String uid = encry.decrypt(data.getUid());
		String orguniqueid = encry.decrypt(data.getOrguniqueid());

		System.out.println("uid....." + uid);
		System.out.println("token....." + orguniqueid);
		Map<String, Object> result = new HashMap<String, Object>();

		SqlParameterSource in = new MapSqlParameterSource().addValue("i_uid", uid).addValue("i_orguniqueid",
				orguniqueid);
		// .addValue("i_imeiNo", imeiNo);
		result = sp_downloadleadorganizationmemberlist.execute(in);

		return result;
	}

	public Map<String, Object> downloadorganiztionwithorgcode(CreateOrganization data) {
		Map<String, Object> result = new HashMap<String, Object>();

		SqlParameterSource in = new MapSqlParameterSource().addValue("i_orgcode", data.getOrgcode());
		// .addValue("i_imeiNo", imeiNo);
		result = sp_downloadorganiztionwithorgcode.execute(in);

		return result;
	}

	public Map<String, Object> checkOrgCodeAvailability(String orgcode) {
		Map<String, Object> map = new HashMap<String, Object>();

		try {

			SqlParameterSource in = new MapSqlParameterSource().addValue("i_orgcode", orgcode);

			map = sp_chenckorgcodeavailability.execute(in);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e);
		}
		System.out.println(map);
		return map;
	}

	public Map<String, Object> checkdailylogin(String uid, String orguniqueid2, String deviceid) {
		Map<String, Object> map = new HashMap<String, Object>();

		try {

			SqlParameterSource in = new MapSqlParameterSource().addValue("i_uid", uid).addValue("i_deviceid", deviceid)
					.addValue("i_orguniqueid", orguniqueid2);

			map = sp_checkdailylogin.execute(in);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e);
		}
		System.out.println(map);
		return map;
	}

	public Map<String, Object> downloadToken(String uid, String deviceid) throws Exception {
		String uid1 = encry.decrypt(uid);
		Map<String, Object> map = new HashMap<String, Object>();

		try {
			SqlParameterSource in = new MapSqlParameterSource().addValue("i_uid", uid1).addValue("i_deviceid",
					deviceid);

			Map out = sp_downloadTokenNew.execute(in);
			int status = (int) out.get("o_status");
			String token = (String) out.get("o_token");
			if (status == 106) {
				map.put("status", status);
				map.put("token", token);
			} else if (status == 107) {
				map.put("status", status);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		System.out.println("download token map....." + map);
		return map;
	}

	public Map<String, Object> joinMember(OrganizationMemberModel data) {
		Map<String, Object> result = new HashMap<String, Object>();

		SqlParameterSource in = new MapSqlParameterSource().addValue("i_orgcode", data.getOrgcode())
				.addValue("i_invitationto", data.getInvitationto());
		// .addValue("i_imeiNo", imeiNo);
		result = sp_joinmember.execute(in);
		return result;

	}

	public Map<String, Object> downloadorganiztionDetailwithUid(CreateOrganization data) throws Exception {
		String uid = encry.decrypt(data.getUid());
		// String orguniqueid = encry.decrypt(data.getOrguniqueid());
		Map<String, Object> result = new HashMap<String, Object>();

		SqlParameterSource in = new MapSqlParameterSource().addValue("i_uid", uid);
		// .addValue("i_imeiNo", imeiNo);
		result = sp_downloadorganiztiondetailswithuid.execute(in);

		return result;

	}

	public Map<String, Object> deleteMemberJoin(OrganizationMemberModel data) throws Exception {
		String uid = encry.decrypt(data.getUid());
		String orguniid = encry.decrypt(data.getOrguniqueid());
		Map<String, Object> map = new HashMap<String, Object>();
		SqlParameterSource in = new MapSqlParameterSource().addValue("i_orgmemberid", data.getOrgmemberid())
				.addValue("i_orguniqueid", orguniid).addValue("i_uid", uid);
		map = sp_deletejoinmember.execute(in);
		return map;
	}

	public Map<String, Object> SaveLastSeenDetails(LastSeenModel data) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		int maxId = 0;
		int status = 0;
		String uid = encry.decrypt(data.getUid());
		// String orguniquieid = encry.decrypt(data.getOrguniqueid());

		try {
			SqlParameterSource parameter = new MapSqlParameterSource().addValue("i_app_keyword", data.getApp_keyword())
					.addValue("i_app_version_name", data.getApp_version_name())
					.addValue("i_app_version_code", data.getApp_version_code())
					.addValue("i_registered_mobile", data.getRegistered_mobile())
					.addValue("i_last_used_date", data.getLast_used_date()).addValue("i_imei", data.getImei())
					.addValue("i_uid", uid);
			map = sp_savelastseendetails.execute(parameter);
			status = (int) map.get("o_status");
		} catch (Exception e) {
			System.out.println(e);
		}
		map.put("status", status);
		return map;
	}

	public Map<String, Object> checkVersion(AppVersionDetails data) {
		Map<String, Object> map = new HashMap<String, Object>();

		try {
			SqlParameterSource in = new MapSqlParameterSource().addValue("i_app_version", data.getAppVersion())
					.addValue("i_appkeyword", data.getAppkeyword());

			map = sp_checkappversiondetails.execute(in);
			int status = (int) map.get("o_status");
		} catch (Exception e) {
			// TODO: handle exception
		}

		return map;
	}

	public Map<String, Object> randomNoGenaeration() {
		Map<String, Object> map = new HashMap<String, Object>();
		SqlParameterSource in = new MapSqlParameterSource();
		map = sp_generaterandomno.execute(in);
		return map;
	}

	public int otpverifyfirebase(List<CRMmodel> data1) throws Exception {
		CRMmodel model = data1.get(0);
		String uid = encry.decrypt(model.getUid());
		System.out.println(uid);
		int status = 0;
		int status1 = 0;
		String DbPassword = "";
		try {

			SqlParameterSource parameter = new MapSqlParameterSource().addValue("i_uid", uid)
					.addValue("i_deviceid", model.getDeviceid()).addValue("i_fbstatus", model.getFbstatus());
			Map out = OTPVerificationFireBase.execute(parameter);
			status = (Integer) out.get("status");
			System.out.println("out stuts" + status);
			if (status == 106) {

				System.out.println("ststus" + status);
				/*
				 * String msg = " Your Password for mobile no " +
				 * model.getUserMobileNo() + " is " + DbPassword + " ";
				 * otpMessageTest.sendSms(model.getUserMobileNo(), msg);
				 */
			}
		} catch (NullPointerException e) {
		}
		return status;
	}

	public Map<String, Object> downloadDetailsofSameOrganizationMemberLeads(MemberDetailsOfSameOrg data)
			throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		String orgid = encry.decrypt(data.getOrguniqueid());
		String uid = encry.decrypt(data.getUid());
		SqlParameterSource in = new MapSqlParameterSource().addValue("i_leadphoneno", data.getLeadphoneno())
				.addValue("i_orguniqueid", orgid).addValue("i_uid", uid);
		// .addValue("i_imeiNo", imeiNo);
		result = sp_downloadleadinfowithleadmobileno.execute(in);

		return result;
	}
}
