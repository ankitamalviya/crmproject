package com.dao;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import com.model.LeadSourceModel;
import com.model.TaskassignmentModel;
import com.security.EncryptDecryptStringWithAES;

@Repository
public class TaskassignmentDao {
	private JdbcTemplate jdbcTemplate;
	private DataSource dataSource;
	private SimpleJdbcCall sp_addtaskassignment, sp_downloadtaskbymaxid, sp_downloadtaskassignment,
			sp_downloadtaskassignedbyname, sp_deletetaskassignment;
	EncryptDecryptStringWithAES encry = new EncryptDecryptStringWithAES();

	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
		this.dataSource = dataSource;
		this.sp_addtaskassignment = new SimpleJdbcCall(this.dataSource).withProcedureName("sp_addtaskassignment");
		this.sp_downloadtaskbymaxid = new SimpleJdbcCall(this.dataSource).withProcedureName("sp_downloadtaskbymaxid");
		this.sp_downloadtaskassignment = new SimpleJdbcCall(this.dataSource)
				.withProcedureName("sp_downloadtaskassignment");
		this.sp_downloadtaskassignedbyname = new SimpleJdbcCall(this.dataSource)
				.withProcedureName("sp_downloadtaskassignedbyname");
		this.sp_deletetaskassignment = new SimpleJdbcCall(this.dataSource).withProcedureName("sp_deletetaskassignment");
	}

	/*****************************
	 * Add Lead Details Detail
	 * 
	 * @throws Exception
	 ********************************/
	public Map<String, Object> addtaskassignment(TaskassignmentModel taskassignment) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		int maxId = 0;
		int status = 0;

		String uid = encry.decrypt(taskassignment.getUid());

		String orguniqueid = encry.decrypt(taskassignment.getOrguniqueid());

		String assignto = encry.decrypt(taskassignment.getAssigendto());

		try {
			SqlParameterSource parameter = new MapSqlParameterSource().addValue("i_taskid", taskassignment.getTaskid())
					.addValue("i_type", taskassignment.getType()).addValue("i_leadid", taskassignment.getLeadid())
					.addValue("i_leadorgname", taskassignment.getLeadorgname())
					.addValue("i_taskname", taskassignment.getTaskname())
					.addValue("i_taskdescription", taskassignment.getTaskdescription())
					.addValue("i_assigendby", taskassignment.getAssigendby()).addValue("i_assigendto", assignto)
					.addValue("i_reminder_date", taskassignment.getReminderdate())
					.addValue("i_reminder_time", taskassignment.getRemindertime())
					.addValue("i_duedate", taskassignment.getDuedate())
					.addValue("i_duetime", taskassignment.getDuetime()).addValue("i_orguniqueid", orguniqueid)
					.addValue("i_deviceid", taskassignment.getDeviceid()).addValue("i_uid", uid)
					.addValue("i_short_description", taskassignment.getShort_description())
					.addValue("i_long_description", taskassignment.getLong_description())
					.addValue("i_createdby", taskassignment.getCreatedby())
					.addValue("i_modifiedby", taskassignment.getModifiedby());

			map = sp_addtaskassignment.execute(parameter);
			status = (int) map.get("o_status");
			maxId = (int) map.get("o_maxid");
			System.out.println("map..." + map);
			if (maxId != 0)
				map = downloadtaskbymaxid(maxId);
		} catch (Exception e) {
			System.out.println(e);
		}
		map.put("status", status);
		return map;
	}

	public Map<String, Object> downloadtaskbymaxid(int id) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			SqlParameterSource in = new MapSqlParameterSource().addValue("i_maxid", id);
			map = sp_downloadtaskbymaxid.execute(in);
		} catch (Exception e) {
			System.out.println(e);
		}

		return map;
	}

	// public Map<String, Object> downloadtaskassignment() {
	//
	// Map<String, Object> map = new HashMap<String, Object>();
	// try {
	// SqlParameterSource in = new MapSqlParameterSource();
	// map = sp_downloadtaskassignment.execute(in);
	// System.out.println("map......." + map);
	//
	// } catch (Exception e) {
	// System.out.println(e);
	// }
	//
	// return map;
	// }

	public Map<String, Object> downloadtaskassignment(String orguniqueid, String uid) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String orguniqueid1 = encry.decrypt(orguniqueid);
		String uid1 = encry.decrypt(uid);
		System.out.println("orguniqueid1.."+orguniqueid1);
		System.out.println("uid1..."+uid1);
		try {
			SqlParameterSource in = new MapSqlParameterSource().addValue("i_uid", uid1).addValue("i_orguniqueid",
					orguniqueid1);
			map = sp_downloadtaskassignment.execute(in);
			System.out.println("map......." + map);

		} catch (Exception e) {
			System.out.println(e);
		}

		return map;
	}

	public Map<String, Object> getUserRegisterData(String uid) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			SqlParameterSource in = new MapSqlParameterSource().addValue("i_uid", uid);
			map = sp_downloadtaskassignedbyname.execute(in);
		} catch (Exception e) {
			System.out.println(e);
		}

		return map;

	}

	public Map<String, Object> deleteTaskInfo(TaskassignmentModel data) throws Exception {
		String uid = encry.decrypt(data.getUid());
		String orguniid = encry.decrypt(data.getOrguniqueid());
		Map<String, Object> map = new HashMap<String, Object>();
		SqlParameterSource in = new MapSqlParameterSource().addValue("i_taskid", data.getTaskid())
				.addValue("i_leadid", data.getLeadid()).addValue("i_orguniqueid", orguniid).addValue("i_uid", uid);
		map = sp_deletetaskassignment.execute(in);
		return map;
	}

}
