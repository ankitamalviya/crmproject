package com.dao;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import com.model.OrganizationTagModel;
import com.security.EncryptDecryptStringWithAES;

@Repository

public class OrganizationTagDao {

	private JdbcTemplate jdbcTemplate;
	private DataSource dataSource;
	private SimpleJdbcCall sp_saveorganizationtag, sp_downloadorgtagswithmaxid, sp_downloadogranizationtags;
	EncryptDecryptStringWithAES encry = new EncryptDecryptStringWithAES();

	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
		this.dataSource = dataSource;
		this.sp_saveorganizationtag = new SimpleJdbcCall(this.dataSource).withProcedureName("sp_saveorganizationtag");
		this.sp_downloadorgtagswithmaxid = new SimpleJdbcCall(this.dataSource)
				.withProcedureName("sp_downloadorgtagswithmaxid");
		this.sp_downloadogranizationtags = new SimpleJdbcCall(this.dataSource)
				.withProcedureName("sp_downloadogranizationtags");
	}

	public Map<String, Object> addOrgTags(OrganizationTagModel orgtagmodel) throws Exception {
		System.out.println("hello on.....orgdao");
		Map<String, Object> map = new HashMap<String, Object>();
		int maxId = 0;
		int status = 0;
		String uid = encry.decrypt(orgtagmodel.getUid());
		String orguniqueid = encry.decrypt(orgtagmodel.getOrguniqueid());

		try {
			SqlParameterSource parameter = new MapSqlParameterSource().addValue("i_tagid", orgtagmodel.getTagid())
					.addValue("i_tagname", orgtagmodel.getTagname())
					.addValue("i_tagdescription", orgtagmodel.getTagdescription())
					.addValue("i_tagcolor", orgtagmodel.getTagcolor()).addValue("i_uid", uid)
					.addValue("i_orguniqueid", orguniqueid).addValue("i_status", orgtagmodel.getStatus())
					.addValue("i_deviceid", orgtagmodel.getDeviceid())
					.addValue("i_createdby", orgtagmodel.getCreatedby())
					.addValue("i_modifiedby", orgtagmodel.getModifiedby());

			map = sp_saveorganizationtag.execute(parameter);
			status = (int) map.get("o_status");
			maxId = (int) map.get("i_maxid");
			System.out.println("maxId is................." + maxId);
			if (maxId != 0)
				map = downloadleadsourcebymaxid(maxId);
		} catch (Exception e) {
			System.out.println(e);
		}
		map.put("status", status);
		System.out.println("map......................." + maxId);
		return map;
	}

	private Map<String, Object> downloadleadsourcebymaxid(int id) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			SqlParameterSource in = new MapSqlParameterSource().addValue("i_maxid", id);
			map = sp_downloadorgtagswithmaxid.execute(in);
			System.out.println(map);
			System.out.println(map.get("o_status"));
		} catch (Exception e) {
			System.out.println(e);
		}

		return map;

	}

	public Map<String, Object> downloadOrganizationTags(OrganizationTagModel data) throws Exception {
		String uid = encry.decrypt(data.getUid());
		String orguniqueid = encry.decrypt(data.getOrguniqueid());
						Map<String, Object> result = new HashMap<String, Object>();

		SqlParameterSource in = new MapSqlParameterSource()
				.addValue("i_uid", uid).addValue("i_orguniqueid",orguniqueid);
		// .addValue("i_imeiNo", imeiNo);
		result = sp_downloadogranizationtags.execute(in);

		return result;
	}

}
