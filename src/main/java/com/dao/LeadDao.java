package com.dao;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;
import com.model.LeadModel;
import com.security.EncryptDecryptStringWithAES;

@Repository
public class LeadDao {
	private JdbcTemplate jdbcTemplate;
	private DataSource dataSource;
	private SimpleJdbcCall sp_addleaddetails, sp_downloadleaddetails, sp_downloadleaddetailswithmaxid,
			sp_downloadtaskassignedbyname;
	EncryptDecryptStringWithAES encry = new EncryptDecryptStringWithAES();

	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
		this.dataSource = dataSource;
		this.sp_addleaddetails = new SimpleJdbcCall(this.dataSource).withProcedureName("sp_addleaddetails");
		this.sp_downloadleaddetails = new SimpleJdbcCall(this.dataSource).withProcedureName("sp_downloadleaddetails");
		this.sp_downloadleaddetailswithmaxid = new SimpleJdbcCall(this.dataSource)
				.withProcedureName("sp_downloadleaddetailswithmaxid");
		this.sp_downloadtaskassignedbyname = new SimpleJdbcCall(this.dataSource)
				.withProcedureName("sp_downloadtaskassignedbyname");
	}

	/*****************************
	 * Add Lead Details Detail
	 * 
	 * @throws Exception
	 ********************************/
	public Map<String, Object> addleaddetails(LeadModel data) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		int maxId = 0;
		int status = 0;
		String uid = encry.decrypt(data.getUid());
		String orguniquieid = encry.decrypt(data.getOrguniqueid());
		String firstname = encry.decrypt(data.getFirstname());
		String lastname = encry.decrypt(data.getLastname());
		String emailid = encry.decrypt(data.getEmailid());
		String phonenocountrycode = encry.decrypt(data.getPhonenocountrycode());
		String phoneno = encry.decrypt(data.getPhoneno());
		String whatsupcountrycode = encry.decrypt(data.getWhatsupcountrycode());
		String whatappno = encry.decrypt(data.getWhatappno());
		String assignto = data.getAssignedto();

		if (!assignto.equals("")) {
			assignto = encry.decrypt(data.getAssignedto());
		}
		if (assignto.equals("")) {
			assignto = uid;

		}

		try {
			SqlParameterSource parameter = new MapSqlParameterSource().addValue("i_leadid", data.getLeadid())
					.addValue("i_firstname", firstname).addValue("i_lastname", lastname).addValue("i_emailid", emailid)
					.addValue("i_phonenocountrycode", phonenocountrycode).addValue("i_phoneno", phoneno)
					.addValue("i_whatsupcountrycode", whatsupcountrycode).addValue("i_whatappno", whatappno)
					.addValue("i_leadsourceid", data.getLeadsourceid()).addValue("i_designation", data.getDesignation())
					.addValue("i_leadorgname", data.getLeadorgname()).addValue("i_domain", data.getDomain())
					.addValue("i_leadstageid", data.getLeadstageid()).addValue("i_priority", data.getPriority())
					.addValue("i_tags", data.getTags()).addValue("i_assignedto", assignto)
					.addValue("i_orguniqueid", orguniquieid).addValue("i_deviceid", data.getDeviceid())
					.addValue("i_country", data.getCountry()).addValue("i_state", data.getState())
					.addValue("i_city", data.getCity()).addValue("i_uid", uid)
					.addValue("i_createdby", data.getCreatedby()).addValue("i_createddate", data.getCreatedby())
					.addValue("i_modifiedby", data.getModifiedby()).addValue("i_modifieddate", data.getModifieddate());

			map = sp_addleaddetails.execute(parameter);
			System.out.println("map....." + map);
			status = (int) map.get("o_status");
			maxId = (int) map.get("o_maxid");
			if (maxId != 0)
				map = downloadleaddetailswithmaxid(maxId);

		} catch (Exception e) {
			System.out.println(e);
		}
		map.put("status", status);
		return map;
	}

	/*****************************
	 * Download Lead Detail
	 * 
	 * @param orguniqueid
	 * @throws Exception
	 ********************************/
	public Map<String, Object> downloadleaddetails(String orguniqueid, String uid) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String orguniqueid1 = encry.decrypt(orguniqueid);
		String uid1 = encry.decrypt(uid);
		try {
			SqlParameterSource in = new MapSqlParameterSource().addValue("i_orguniqueid", orguniqueid1)
					.addValue("i_uid", uid1);
			map = sp_downloadleaddetails.execute(in);
			System.out.println(map);
			System.out.println(map.get("o_status"));
		} catch (Exception e) {
			System.out.println(e);
		}

		return map;
	}

	public Map<String, Object> downloadleaddetailswithmaxid(int id) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			SqlParameterSource in = new MapSqlParameterSource().addValue("i_maxid", id);
			map = sp_downloadleaddetailswithmaxid.execute(in);
			System.out.println(map);
			System.out.println(map.get("o_status"));
		} catch (Exception e) {
			System.out.println(e);
		}

		return map;
	}

	public Map<String, Object> getUserRegisterData(String uid) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			SqlParameterSource in = new MapSqlParameterSource().addValue("i_uid", uid);
			map = sp_downloadtaskassignedbyname.execute(in);
		} catch (Exception e) {
			System.out.println(e);
		}

		return map;
	}
}
