package com.dao;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import com.model.FacebookpagesModel;
import com.model.FacebookuserModel;
import com.model.LeadSourceModel;
import com.security.EncryptDecryptStringWithAES;

@Repository
public class FacebookDao {
	private JdbcTemplate jdbcTemplate;
	private DataSource dataSource;
	private SimpleJdbcCall sp_savefacebookuser,sp_savefacebookpages, sp_downloadfacebookuserbymaxid,
	sp_downloadfacebookpagesbymaxid, sp_downloadfacebookpages;
	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
		this.dataSource = dataSource;
		this.sp_savefacebookuser = new SimpleJdbcCall(this.dataSource).withProcedureName("sp_savefacebookuser");
		this.sp_savefacebookpages = new SimpleJdbcCall(this.dataSource).withProcedureName("sp_savefacebookpages");
		this.sp_downloadfacebookuserbymaxid = new SimpleJdbcCall(this.dataSource).withProcedureName("sp_downloadfacebookuserbymaxid");
		this.sp_downloadfacebookpagesbymaxid = new SimpleJdbcCall(this.dataSource).withProcedureName("sp_downloadfacebookpagesbymaxid");
		this.sp_downloadfacebookpages = new SimpleJdbcCall(this.dataSource).withProcedureName("sp_downloadfacebookpages");
	}
	EncryptDecryptStringWithAES encry = new EncryptDecryptStringWithAES();
	
	/*****************************
	 * Add facebook user
	 ********************************/
	public Map<String, Object> addfacebookuser(FacebookuserModel facebookuserModel) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		int maxId = 0;
		int status = 0;
		String uid = encry.decrypt(facebookuserModel.getUid());
		String orguniqueid = encry.decrypt(facebookuserModel.getOrguniqueid());
		
		try {
			SqlParameterSource parameter = new MapSqlParameterSource()
					.addValue("i_id", facebookuserModel.getId())
					.addValue("i_firstname", facebookuserModel.getFirstname())
					.addValue("i_lastname", facebookuserModel.getLastname())
					.addValue("i_email", facebookuserModel.getEmail())
					.addValue("i_fbuserid", facebookuserModel.getFbuserid())
					.addValue("i_status",facebookuserModel.getStatus())
					.addValue("i_address", facebookuserModel.getAddress())
					.addValue("i_gender", facebookuserModel.getGender())
					.addValue("i_timelinelink", facebookuserModel.getTimelinelink())
					.addValue("i_userbirthday", facebookuserModel.getUserbirthday())
					.addValue("i_uid",uid)
					.addValue("i_orguniqueid", orguniqueid)
					.addValue("i_deviceid", facebookuserModel.getDeviceid())
					.addValue("i_createdby", facebookuserModel.getCreatedby())
					.addValue("i_modifiedby", facebookuserModel.getModifiedby());

			map = sp_savefacebookuser.execute(parameter);
			status = (int) map.get("o_status");
			maxId = (int) map.get("o_maxid");
			if (maxId != 0)
				map = downloadfacebookuserbymaxid(maxId);
		} catch (Exception e) {
			System.out.println(e);
		}
		map.put("status", status);
		return map;
	}
	
	/*****************************
	 * Download facebook user by maxid
	 ********************************/
	public Map<String, Object> downloadfacebookuserbymaxid(int id) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			SqlParameterSource in = new MapSqlParameterSource().addValue("i_maxid", id);
			map = sp_downloadfacebookuserbymaxid.execute(in);
			System.out.println(map);
			System.out.println(map.get("o_status"));
		} catch (Exception e) {
			System.out.println(e);
		}

		return map;
	}
	
	/*****************************
	 * Add facebook user
	 ********************************/
	public Map<String, Object> addfacebookpage(FacebookpagesModel facebookpagesModel) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		int maxId = 0;
		int status = 0;
		
		try {
			SqlParameterSource parameter = new MapSqlParameterSource()
					.addValue("i_pid", facebookpagesModel.getPid())
					.addValue("i_fbuserid", facebookpagesModel.getFbuserid())
					.addValue("i_pageid", facebookpagesModel.getPageid())
					.addValue("i_pagename", facebookpagesModel.getPagename())
					.addValue("i_pagecategory", facebookpagesModel.getPagecategory())
					.addValue("i_accesstoken", facebookpagesModel.getAccesstoken())
					.addValue("i_deviceid", facebookpagesModel.getDeviceid())
					.addValue("i_createdby", facebookpagesModel.getCreatedby())
					.addValue("i_modifiedby", facebookpagesModel.getModifiedby());					
			map = sp_savefacebookpages.execute(parameter);
			status = (int) map.get("o_status");
			maxId = (int) map.get("o_maxid");
			if (maxId != 0)
				map = downloadfacebookpagesbymaxid(maxId);
		} catch (Exception e) {
			System.out.println(e);
		}
		map.put("status", status);
		return map;
	}
	
	/*****************************
	 * Download facebook pages by maxid
	 ********************************/
	public Map<String, Object> downloadfacebookpagesbymaxid(int id) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			SqlParameterSource in = new MapSqlParameterSource().addValue("i_maxid", id);
			map = sp_downloadfacebookpagesbymaxid.execute(in);
			System.out.println(map);
			System.out.println(map.get("o_status"));
		} catch (Exception e) {
			System.out.println(e);
		}

		return map;
	}
	
	public Map<String, Object> downloadfacebookpages(String fbuserid) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			SqlParameterSource in = new MapSqlParameterSource().addValue("i_fbuserid", fbuserid);
			map = sp_downloadfacebookpages.execute(in);
			
		} catch (Exception e) {
			System.out.println(e);
		}

		return map;
	}
}
