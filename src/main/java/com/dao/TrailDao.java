package com.dao;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import com.model.TrailModel;
import com.security.EncryptDecryptStringWithAES;

@Repository

public class TrailDao {
	private JdbcTemplate jdbcTemplate;
	private DataSource dataSource;
	private SimpleJdbcCall sp_savetrailinfo, sp_downloadtrailinfowithmaxid, sp_downloadtrailinfo,
			sp_downloadtaskassignedbyname, sp_deletetrailinfo;

	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
		this.dataSource = dataSource;
		this.sp_savetrailinfo = new SimpleJdbcCall(this.dataSource).withProcedureName("sp_savetrailinfo");
		this.sp_downloadtrailinfowithmaxid = new SimpleJdbcCall(this.dataSource)
				.withProcedureName("sp_downloadtrailinfowithmaxid");
		this.sp_downloadtrailinfo = new SimpleJdbcCall(this.dataSource).withProcedureName("sp_downloadtrailinfo");
		this.sp_downloadtaskassignedbyname = new SimpleJdbcCall(this.dataSource)
				.withProcedureName("sp_downloadtaskassignedbyname");
		this.sp_deletetrailinfo = new SimpleJdbcCall(this.dataSource).withProcedureName("sp_deletetrailinfo");
	}

	EncryptDecryptStringWithAES encry = new EncryptDecryptStringWithAES();

	public Map<String, Object> addTrailInfo(TrailModel trailmodel) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		int maxId = 0;
		int status = 0;
		String uid = encry.decrypt(trailmodel.getUid());
		String orguniqueid = encry.decrypt(trailmodel.getOrguniqueid());
		String associatedwith = encry.decrypt(trailmodel.getAssociatedwith());
		

		try {
			SqlParameterSource parameter = new MapSqlParameterSource().addValue("i_trailid", trailmodel.getTrailid())
					.addValue("i_leadid", trailmodel.getLeadid()).addValue("i_tagid", trailmodel.getTagid())
					.addValue("i_orguniqueid", orguniqueid).addValue("i_uid", uid)
					.addValue("i_activitytype", trailmodel.getActivitytype())
					.addValue("i_callnotes", trailmodel.getCallnotes()).addValue("i_status", trailmodel.getStatus())
					.addValue("i_activitydate", trailmodel.getActivitydate())
					.addValue("i_activitytime", trailmodel.getActivitytime())
					.addValue("i_associatedwith", associatedwith).addValue("i_calloutcome", trailmodel.getCalloutcome())
					.addValue("i_deviceid", trailmodel.getDeviceid()).addValue("i_associatedby", trailmodel.getAssociatedby())
					.addValue("i_createdby", trailmodel.getCreatedby())
					.addValue("i_modifiedby", trailmodel.getModifiedby());

			map = sp_savetrailinfo.execute(parameter);
			status = (int) map.get("o_status");
			System.out.println("status" + status);
			maxId = (int) map.get("o_maxid");
			System.out.println(maxId);
			if (maxId != 0)
				map = downloadTrailInfoBymaxid(maxId);
		} catch (Exception e) {
			System.out.println(e);
		}
		map.put("status", status);
		return map;
	}

	private Map<String, Object> downloadTrailInfoBymaxid(int id) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			SqlParameterSource in = new MapSqlParameterSource().addValue("i_maxid", id);
			map = sp_downloadtrailinfowithmaxid.execute(in);
			System.out.println(map);
			System.out.println(map.get("o_status"));
		} catch (Exception e) {
			System.out.println(e);
		}

		return map;
	}

	public Map<String, Object> downloadTrailInfo(TrailModel data) throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		String orguniqueid = encry.decrypt(data.getOrguniqueid());

		SqlParameterSource in = new MapSqlParameterSource().addValue("i_leadid", data.getLeadid())
				.addValue("i_orguniqueid", orguniqueid);
		// .addValue("i_imeiNo", imeiNo);
		result = sp_downloadtrailinfo.execute(in);

		return result;
	}

	public Map<String, Object> getUserRegisterData(String uid) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			SqlParameterSource in = new MapSqlParameterSource().addValue("i_uid", uid);
			map = sp_downloadtaskassignedbyname.execute(in);
		} catch (Exception e) {
			System.out.println(e);
		}

		return map;

	}

	public Map<String, Object> deleteTrailInfo(TrailModel data) throws Exception {
		String uid = encry.decrypt(data.getUid());
		String orguniid = encry.decrypt(data.getOrguniqueid());
		Map<String, Object> map = new HashMap<String, Object>();
		SqlParameterSource in = new MapSqlParameterSource().addValue("i_trailid", data.getTrailid())
				.addValue("i_leadid", data.getLeadid()).addValue("i_orguniqueid", orguniid).addValue("i_uid", uid);
		map = sp_deletetrailinfo.execute(in);
		return map;
	}

}
