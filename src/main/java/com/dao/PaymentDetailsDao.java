package com.dao;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import com.model.PaymentDetailsModel;
import com.security.EncryptDecryptStringWithAES;

@Repository
public class PaymentDetailsDao {
	private JdbcTemplate jdbcTemplate;
	private DataSource dataSource;
	private SimpleJdbcCall sp_savepaymentdetails, sp_downloadPaymentdetailBymaxid;
	EncryptDecryptStringWithAES encry = new EncryptDecryptStringWithAES();
	
	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
		this.dataSource = dataSource;
		this.sp_savepaymentdetails = new SimpleJdbcCall(this.dataSource).withProcedureName("sp_savepaymentdetails");
		this.sp_downloadPaymentdetailBymaxid = new SimpleJdbcCall(this.dataSource).withProcedureName("sp_downloadPaymentdetailBymaxid");
		
	}
	
	public Map<String, Object> savepaymentdetails(PaymentDetailsModel paymentDetailsModel) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		int maxId = 0;
		int status = 0;
		String uid = encry.decrypt(paymentDetailsModel.getUid());
		

		try {
			SqlParameterSource parameter = new MapSqlParameterSource()
					.addValue("i_paymentid", paymentDetailsModel.getPaymentid())
					.addValue("i_appid", paymentDetailsModel.getAppid())
					.addValue("i_securitykey", paymentDetailsModel.getSecuritykey())
					.addValue("i_orderid", paymentDetailsModel.getOrderid())
					.addValue("i_orderamount", paymentDetailsModel.getOrderamount())
					.addValue("i_ordercountry", paymentDetailsModel.getOrdercurrency())
					.addValue("i_order_notes", paymentDetailsModel.getOrder_notes())
					.addValue("i_customer_name", paymentDetailsModel.getCustomer_name())
					.addValue("i_customer_phone", paymentDetailsModel.getCustomer_phone())
					.addValue("i_customer_email", paymentDetailsModel.getCustomer_email())
					.addValue("i_seller_phone", paymentDetailsModel.getSeller_phone())
					.addValue("i_return_url", paymentDetailsModel.getReturn_url())
					.addValue("i_notify_url", paymentDetailsModel.getNotify_url())
					.addValue("i_payment_mode", paymentDetailsModel.getPayment_mode())
					.addValue("i_partner_code", paymentDetailsModel.getPartner_code())
					.addValue("i_admintransectionid", paymentDetailsModel.getAdmintransectionid())
					.addValue("i_transaction_id", paymentDetailsModel.getTransaction_id())
					
					
					.addValue("i_uid", uid)
					.addValue("i_discount", paymentDetailsModel.getDiscount())
					.addValue("i_dateofpayment", paymentDetailsModel.getDateofpayment())
					.addValue("i_timeofpayment", paymentDetailsModel.getTimeofpayment())
					.addValue("i_deviceid", paymentDetailsModel.getDeviceid())
					.addValue("i_appversion", paymentDetailsModel.getAppversion())
					.addValue("i_token", paymentDetailsModel.getToken())
					.addValue("i_txStatus", paymentDetailsModel.getTxStatus())
					.addValue("i_txMsg", paymentDetailsModel.getTxMsg())
					.addValue("i_txTime", paymentDetailsModel.getTxTime())
					.addValue("i_type", paymentDetailsModel.getType())
					.addValue("i_signature", paymentDetailsModel.getSignature())
					
					

					
					.addValue("i_createdby", paymentDetailsModel.getCreatedby())
					.addValue("i_modifiedby", paymentDetailsModel.getModifiedby());		
			map = sp_savepaymentdetails.execute(parameter);
			status = (int) map.get("o_status");
			System.out.println("status" + status);
			maxId = (int) map.get("o_maxid");
			System.out.println(maxId);
			if (maxId != 0)
				map = downloadPaymentdetailBymaxid(maxId);
		} catch (Exception e) {
			System.out.println(e);
		}
		map.put("status", status);
		return map;
	}
	
	private Map<String, Object> downloadPaymentdetailBymaxid(int id) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			SqlParameterSource in = new MapSqlParameterSource().addValue("i_maxid", id);
			map = sp_downloadPaymentdetailBymaxid.execute(in);
			System.out.println(map);
			System.out.println(map.get("o_status"));
		} catch (Exception e) {
			System.out.println(e);
		}

		return map;
	}

}
