package com.model;

public class StaticData {
	/********** NEW REGISTRATION STATUS CODE ***************/

	public static final Integer NEW_REGISTRATION = 200;
	public static final Integer REGISTRATION_SAME_OTP_NOT_VERIFIED = 102;
	public static final Integer REGISTRATION_SAME_OTP_VERIFIED = 103;
	public static final Integer REGISTER_FROM_NEW_DEVICE = 400;
	public static final Integer NEW_REGISTRATIONFIREBASE = 108;

	/*********** NEW REGISTRATION RESPONSE MESSAGE **********/

	public static final String REGISTRATION_SUCCESS = "Registration Successful";
	public static final String ALREADY_REGISTERED_VERIFY_OTP = "Already Registered,Verify OTP!";
	public static final String ALREADY_REGISTERED_OTP_VERIFIED = "Already Registered";
	public static final String DEVICE_CHANGED = "Device Changed,Please verify OTP";

	/************* OTP VERIFICATION STATUS CODE *************/

	public static final Integer OTP_VERIFIED = 200;
	public static final Integer OTP_NOT_VERIFIED = 400;

	/******** OTP VERIFICATION RESPONSE MESSAGE *********/

	public static final String OTP_SUCCESS = "OTP Verification Successful";
	public static final String OTP_FAILED = "OTP Verification Failed";

	/********** LOGIN VALIDATION STATUS CODE **********/

	public static final Integer LOGIN_SUCCESS = 200;
	public static final Integer LOGIN_FAILED = 400;
	public static final Integer LOGIN_FROM_DIFFERENT_DEVICE = 300;

	/********** LOGIN VALIDATION RESPONSE MESSAGE ***********/

	public static final String LOGIN_SUCCESSFUL = "Login Successful";
	public static final String LOGIN_FAILED_MSG = "Login Failed";
	public static final String LOGIN_FROM_DIFFERENT_DEVICE_MSG = "Login From Different Device,please Verify OTP";

	/************* CREATE SOCIETY STATUS CODE **************/

	public static final Integer ORGANIZATION_CREATED = 200;
	public static final Integer ORGANIZATION_CREATION_FAILED = 400;

	/********** CREATE SOCIETY RESPONSE MESSAGE *************/

	public static final String ORGANIZATION_CREATION_SUCCESS = "success";
	public static final String ORGANIZATION_CREATION_FAILED_MSG = "Data Exist";

	/********* FORGET PASSWORD RESPONSE CODE **************/

	public static final Integer FORGET_PASSWORD_SENT = 200;
	public static final Integer FORGET_PASSWORD_FAILED = 400;

	/********* FORGET PASSWORD RESPONSE MESSAGE **********/

	public static final String FORGET_PASSWORD_MSG = "success";
	public static final String FORGET_PASSWORD_FAILED_MSG = "failed";

	/********** SEARCH SOCIETY RESPONSE CODE ***********/

	public static final Integer SOCIETY_FOUND = 200;
	public static final Integer SOCIETY_NOT_FOUND = 400;

	/********* SEARCH SOCIETY RESPONSE MESSAGE ***********/

	public static final String SOCIETY_FOUND_MSG = "success";
	public static final String SOCIETY_NOT_FOUND_MSG = "failed";

	/******** COMMITTEE MEMBER && SOCIETY MEMBER RESPONSE CODE ***********/

	public static final Integer MEMBER_FOUND = 200;
	public static final Integer MEMBER_NOT_FOUND = 400;

	/******** COMMITTEE MEMBER && SOCIETY MEMBER RESPONSE MESSAGE ***********/

	public static final String MEMBER_FOUND_MSG = "member found";
	public static final String MEMBER_NOT_FOUND_MSG = "member not found";

	/************
	 * COMMON DATA FOUND && NO DATA FOUND && ERROR RESPONSE CODE
	 *************/

	public static final Integer DATA_FOUND = 200;
	public static final Integer NO_DATA_FOUND = 400;
	public static final Integer ERROR = 404;

	/************
	 * COMMON DATA FOUND && NO DATA FOUND && ERROR RESPONSE MESSAGE
	 *************/

	public static final String DATA_FOUND_MSG = "success";
	public static final String NO_DATA_FOUND_MSG = "no data found";
	public static final String ERROR_MSG = "error";

}
