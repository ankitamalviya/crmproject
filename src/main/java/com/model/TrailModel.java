package com.model;

public class TrailModel {

	private int trailid;
	private int leadid;
	private int tagid;
	private String orguniqueid;
	private String uid;
	private int activitytype;
	private String callnotes;
	private String status;
	private String activitydate;
	private String activitytime;
	private String associatedwith;
	private String calloutcome;
	private String deviceid;
	private String associatedby;
	private String createdby;
	private String modifiedby;
	private String token;
	private String local_id;
	private String firstname;
	private String lastname;

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getLocal_id() {
		return local_id;
	}

	public void setLocal_id(String local_id) {
		this.local_id = local_id;
	}

	public int getActivitytype() {
		return activitytype;
	}

	public void setActivitytype(int activitytype) {
		this.activitytype = activitytype;
	}

	public String getCallnotes() {
		return callnotes;
	}

	public void setCallnotes(String callnotes) {
		this.callnotes = callnotes;
	}

	public String getActivitydate() {
		return activitydate;
	}

	public void setActivitydate(String activitydate) {
		this.activitydate = activitydate;
	}

	public String getActivitytime() {
		return activitytime;
	}

	public void setActivitytime(String activitytime) {
		this.activitytime = activitytime;
	}

	public String getAssociatedwith() {
		return associatedwith;
	}

	public void setAssociatedwith(String associatedwith) {
		this.associatedwith = associatedwith;
	}

	public String getCalloutcome() {
		return calloutcome;
	}

	public void setCalloutcome(String calloutcome) {
		this.calloutcome = calloutcome;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public int getTrailid() {
		return trailid;
	}

	public void setTrailid(int trailid) {
		this.trailid = trailid;
	}

	public int getLeadid() {
		return leadid;
	}

	public void setLeadid(int leadid) {
		this.leadid = leadid;
	}

	public int getTagid() {
		return tagid;
	}

	public void setTagid(int tagid) {
		this.tagid = tagid;
	}

	public String getOrguniqueid() {
		return orguniqueid;
	}

	public void setOrguniqueid(String orguniqueid) {
		this.orguniqueid = orguniqueid;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDeviceid() {
		return deviceid;
	}

	public void setDeviceid(String deviceid) {
		this.deviceid = deviceid;
	}

	public String getCreatedby() {
		return createdby;
	}

	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}

	public String getModifiedby() {
		return modifiedby;
	}

	public void setModifiedby(String modifiedby) {
		this.modifiedby = modifiedby;
	}

	public String getAssociatedby() {
		return associatedby;
	}

	public void setAssociatedby(String associatedby) {
		this.associatedby = associatedby;
	}
	

}
