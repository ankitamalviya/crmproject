package com.model;

public class FeatureModel {
	private int fid;
	private String amount;
	private String duration;
	private String discount;
	private String initialamount;
	private String finalamount;
	private int local_id;
	private String createdby;
	private String createddate;
	private String modifiedby;
	private String modifieddate;

	public int getFid() {
		return fid;
	}

	public void setFid(int fid) {
		this.fid = fid;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getDiscount() {
		return discount;
	}

	public void setDiscount(String discount) {
		this.discount = discount;
	}

	public String getInitialamount() {
		return initialamount;
	}

	public void setInitialamount(String initialamount) {
		this.initialamount = initialamount;
	}

	public String getFinalamount() {
		return finalamount;
	}

	public void setFinalamount(String finalamount) {
		this.finalamount = finalamount;
	}

	public int getLocal_id() {
		return local_id;
	}

	public void setLocal_id(int local_id) {
		this.local_id = local_id;
	}

	public String getCreatedby() {
		return createdby;
	}

	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}

	public String getCreateddate() {
		return createddate;
	}

	public void setCreateddate(String createddate) {
		this.createddate = createddate;
	}

	public String getModifiedby() {
		return modifiedby;
	}

	public void setModifiedby(String modifiedby) {
		this.modifiedby = modifiedby;
	}

	public String getModifieddate() {
		return modifieddate;
	}

	public void setModifieddate(String modifieddate) {
		this.modifieddate = modifieddate;
	}

}
