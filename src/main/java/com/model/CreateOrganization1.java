package com.model;

public class CreateOrganization1 {

	private String orgcode1;
	private String orguniid1;
	private String uid1;
	private String city1;
	private String orgid1;
	private String orgname1;
	private String zipcode1;
	
	private String state1;
	private String country1;
	private String userMobileNo1;
	private String logo1;
	private String domain1;
	private String empstrength1;
	private String latitude1;
	private String longitude1;
	private String deviceid1;
	private String createdby1;
	public String getOrgcode1() {
		return orgcode1;
	}
	public void setOrgcode1(String orgcode1) {
		this.orgcode1 = orgcode1;
	}
	public String getOrguniid1() {
		return orguniid1;
	}
	public void setOrguniid1(String orguniid1) {
		this.orguniid1 = orguniid1;
	}
	public String getUid1() {
		return uid1;
	}
	public void setUid1(String uid1) {
		this.uid1 = uid1;
	}
	public String getCity1() {
		return city1;
	}
	public void setCity1(String city1) {
		this.city1 = city1;
	}
	public String getOrgid1() {
		return orgid1;
	}
	public void setOrgid1(String orgid1) {
		this.orgid1 = orgid1;
	}
	public String getOrgname1() {
		return orgname1;
	}
	public void setOrgname1(String orgname1) {
		this.orgname1 = orgname1;
	}
	public String getZipcode1() {
		return zipcode1;
	}
	public void setZipcode1(String zipcode1) {
		this.zipcode1 = zipcode1;
	}
	public String getState1() {
		return state1;
	}
	public void setState1(String state1) {
		this.state1 = state1;
	}
	public String getCountry1() {
		return country1;
	}
	public void setCountry1(String country1) {
		this.country1 = country1;
	}
	public String getUserMobileNo1() {
		return userMobileNo1;
	}
	public void setUserMobileNo1(String userMobileNo1) {
		this.userMobileNo1 = userMobileNo1;
	}
	public String getLogo1() {
		return logo1;
	}
	public void setLogo1(String logo1) {
		this.logo1 = logo1;
	}
	public String getDomain1() {
		return domain1;
	}
	public void setDomain1(String domain1) {
		this.domain1 = domain1;
	}
	public String getEmpstrength1() {
		return empstrength1;
	}
	public void setEmpstrength1(String empstrength1) {
		this.empstrength1 = empstrength1;
	}
	public String getLatitude1() {
		return latitude1;
	}
	public void setLatitude1(String latitude1) {
		this.latitude1 = latitude1;
	}
	public String getLongitude1() {
		return longitude1;
	}
	public void setLongitude1(String longitude1) {
		this.longitude1 = longitude1;
	}
	public String getDeviceid1() {
		return deviceid1;
	}
	public void setDeviceid1(String deviceid1) {
		this.deviceid1 = deviceid1;
	}
	public String getCreatedby1() {
		return createdby1;
	}
	public void setCreatedby1(String createdby1) {
		this.createdby1 = createdby1;
	}
	
	
}
