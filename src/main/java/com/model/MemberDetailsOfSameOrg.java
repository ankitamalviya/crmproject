package com.model;

public class MemberDetailsOfSameOrg {
	private int leadid;
	private String leadphoneno;
	private String orguniqueid;
	private String uid;
	private String token;
	private String deviceid;

	private String firstname;
	private String lastname;
	private String emailid;
	private String phonenocountrycode;
	private String phoneno;

	private String whatsupcountrycode;
	private String whatappno;
	private String leadsourceid;
	private String designation;
	private String leadorgname;
	private String domain;
	private String leadstageid;
	private String priority;
	private String tags;
	private String assignedto;
	private String country;
	private String state;
	private String city;

	public int getLeadid() {
		return leadid;
	}

	public void setLeadid(int leadid) {
		this.leadid = leadid;
	}

	public String getLeadphoneno() {
		return leadphoneno;
	}

	public void setLeadphoneno(String leadphoneno) {
		this.leadphoneno = leadphoneno;
	}

	public String getOrguniqueid() {
		return orguniqueid;
	}

	public void setOrguniqueid(String orguniqueid) {
		this.orguniqueid = orguniqueid;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getDeviceid() {
		return deviceid;
	}

	public void setDeviceid(String deviceid) {
		this.deviceid = deviceid;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getEmailid() {
		return emailid;
	}

	public void setEmailid(String emailid) {
		this.emailid = emailid;
	}

	public String getPhonenocountrycode() {
		return phonenocountrycode;
	}

	public void setPhonenocountrycode(String phonenocountrycode) {
		this.phonenocountrycode = phonenocountrycode;
	}

	public String getPhoneno() {
		return phoneno;
	}

	public void setPhoneno(String phoneno) {
		this.phoneno = phoneno;
	}

	public String getWhatsupcountrycode() {
		return whatsupcountrycode;
	}

	public void setWhatsupcountrycode(String whatsupcountrycode) {
		this.whatsupcountrycode = whatsupcountrycode;
	}

	public String getWhatappno() {
		return whatappno;
	}

	public void setWhatappno(String whatappno) {
		this.whatappno = whatappno;
	}

	public String getLeadsourceid() {
		return leadsourceid;
	}

	public void setLeadsourceid(String leadsourceid) {
		this.leadsourceid = leadsourceid;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getLeadorgname() {
		return leadorgname;
	}

	public void setLeadorgname(String leadorgname) {
		this.leadorgname = leadorgname;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getLeadstageid() {
		return leadstageid;
	}

	public void setLeadstageid(String leadstageid) {
		this.leadstageid = leadstageid;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public String getAssignedto() {
		return assignedto;
	}

	public void setAssignedto(String assignedto) {
		this.assignedto = assignedto;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

}
