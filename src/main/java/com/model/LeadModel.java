package com.model;

import java.util.List;

public class LeadModel {
	private int leadid;
	private String firstname;
	private String lastname;
	private String emailid;
	private String phonenocountrycode;
	private String phoneno;
	private String whatsupcountrycode;
	private String whatappno;
	private int leadsourceid;
	private String designation;
	private String leadorgname;
	private String domain;
	private int leadstageid;
	private int priority;
	private int tags;
	private String assignedto;
	private String orguniqueid;
	private String createddate;
	private String createdby;
	private String deviceid;
	private String modifiedby;
	private String modifieddate;
	private String country;
	private String state;
	private String city;
	private String uid;
	private List<LeadTagsModel> leadtags;
	private int local_id;
	private String token;

	public String getPhonenocountrycode() {
		return phonenocountrycode;
	}

	public void setPhonenocountrycode(String phonenocountrycode) {
		this.phonenocountrycode = phonenocountrycode;
	}

	public String getWhatsupcountrycode() {
		return whatsupcountrycode;
	}

	public void setWhatsupcountrycode(String whatsupcountrycode) {
		this.whatsupcountrycode = whatsupcountrycode;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public int getLocal_id() {
		return local_id;
	}

	public void setLocal_id(int local_id) {
		this.local_id = local_id;
	}

	public List<LeadTagsModel> getLeadtags() {
		return leadtags;
	}

	public void setLeadtags(List<LeadTagsModel> leadtags) {
		this.leadtags = leadtags;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public int getLeadid() {
		return leadid;
	}

	public void setLeadid(int leadid) {
		this.leadid = leadid;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getEmailid() {
		return emailid;
	}

	public void setEmailid(String emailid) {
		this.emailid = emailid;
	}

	public String getPhoneno() {
		return phoneno;
	}

	public void setPhoneno(String phoneno) {
		this.phoneno = phoneno;
	}

	public String getWhatappno() {
		return whatappno;
	}

	public void setWhatappno(String whatappno) {
		this.whatappno = whatappno;
	}

	public int getLeadsourceid() {
		return leadsourceid;
	}

	public void setLeadsourceid(int leadsourceid) {
		this.leadsourceid = leadsourceid;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getLeadorgname() {
		return leadorgname;
	}

	public void setLeadorgname(String leadorgname) {
		this.leadorgname = leadorgname;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public int getLeadstageid() {
		return leadstageid;
	}

	public void setLeadstageid(int leadstageid) {
		this.leadstageid = leadstageid;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public int getTags() {
		return tags;
	}

	public void setTags(int tags) {
		this.tags = tags;
	}

	public String getAssignedto() {
		return assignedto;
	}

	public void setAssignedto(String assignedto) {
		this.assignedto = assignedto;
	}

	public String getOrguniqueid() {
		return orguniqueid;
	}

	public void setOrguniqueid(String orguniqueid) {
		this.orguniqueid = orguniqueid;
	}

	public String getCreateddate() {
		return createddate;
	}

	public void setCreateddate(String createddate) {
		this.createddate = createddate;
	}

	public String getCreatedby() {
		return createdby;
	}

	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}

	public String getDeviceid() {
		return deviceid;
	}

	public void setDeviceid(String deviceid) {
		this.deviceid = deviceid;
	}

	public String getModifiedby() {
		return modifiedby;
	}

	public void setModifiedby(String modifiedby) {
		this.modifiedby = modifiedby;
	}

	public String getModifieddate() {
		return modifieddate;
	}

	public void setModifieddate(String modifieddate) {
		this.modifieddate = modifieddate;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@Override
	public String toString() {
		return "LeadModel [leadid=" + leadid + ", firstname=" + firstname + ", lastname=" + lastname + ", emailid="
				+ emailid + ", phonenocountrycode=" + phonenocountrycode + ", phoneno=" + phoneno
				+ ", whatsupcountrycode=" + whatsupcountrycode + ", whatappno=" + whatappno + ", leadsourceid="
				+ leadsourceid + ", designation=" + designation + ", leadorgname=" + leadorgname + ", domain=" + domain
				+ ", leadstageid=" + leadstageid + ", priority=" + priority + ", tags=" + tags + ", assignedto="
				+ assignedto + ", orguniqueid=" + orguniqueid + ", createddate=" + createddate + ", createdby="
				+ createdby + ", deviceid=" + deviceid + ", modifiedby=" + modifiedby + ", modifieddate=" + modifieddate
				+ ", country=" + country + ", state=" + state + ", city=" + city + ", uid=" + uid + ", leadtags="
				+ leadtags + ", local_id=" + local_id + ", token=" + token + "]";
	}
	

}
