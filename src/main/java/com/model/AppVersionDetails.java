package com.model;

public class AppVersionDetails {
	private int vId;
	private double appVersion;
	private int priority;

	private String uid;
	private String token;
	private String deviceid;
	private String appkeyword;

	public int getvId() {
		return vId;
	}

	public void setvId(int vId) {
		this.vId = vId;
	}

	public double getAppVersion() {
		return appVersion;
	}

	public void setAppVersion(double appVersion) {
		this.appVersion = appVersion;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getDeviceid() {
		return deviceid;
	}

	public void setDeviceid(String deviceid) {
		this.deviceid = deviceid;
	}

	public String getAppkeyword() {
		return appkeyword;
	}

	public void setAppkeyword(String appkeyword) {
		this.appkeyword = appkeyword;
	}

}
