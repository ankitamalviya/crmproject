package com.model;

public class TaskassignmentModel {
	private int taskid;
	private int type;
	private int leadid;
	private String leadorgname;
	private String taskname;
	private String taskdescription;
	private String assigendby;
	private String assigendto;
	private String reminderdate;
	private String remindertime;
	private String duedate;
	private String duetime;
	private String orguniqueid;
	private String deviceid;
	private String uid;
	private String createdby;
	private String modifiedby;
	private String modifeddate;
	private String local_id;
	private String firstname;
	private String lastname;

	private String token;
	
	private String short_description;
	private String long_description;

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getDeviceid() {
		return deviceid;
	}

	public void setDeviceid(String deviceid) {
		this.deviceid = deviceid;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getLocal_id() {
		return local_id;
	}

	public void setLocal_id(String local_id) {
		this.local_id = local_id;
	}

	public int getTaskid() {
		return taskid;
	}

	public void setTaskid(int taskid) {
		this.taskid = taskid;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getLeadid() {
		return leadid;
	}

	public void setLeadid(int leadid) {
		this.leadid = leadid;
	}

	public String getLeadorgname() {
		return leadorgname;
	}

	public void setLeadorgname(String leadorgname) {
		this.leadorgname = leadorgname;
	}

	public String getTaskname() {
		return taskname;
	}

	public void setTaskname(String taskname) {
		this.taskname = taskname;
	}

	public String getTaskdescription() {
		return taskdescription;
	}

	public void setTaskdescription(String taskdescription) {
		this.taskdescription = taskdescription;
	}

	public String getAssigendby() {
		return assigendby;
	}

	public void setAssigendby(String assigendby) {
		this.assigendby = assigendby;
	}

	public String getReminderdate() {
		return reminderdate;
	}

	public void setReminderdate(String reminderdate) {
		this.reminderdate = reminderdate;
	}

	public String getRemindertime() {
		return remindertime;
	}

	public void setRemindertime(String remindertime) {
		this.remindertime = remindertime;
	}

	public String getDuedate() {
		return duedate;
	}

	public void setDuedate(String duedate) {
		this.duedate = duedate;
	}

	public String getDuetime() {
		return duetime;
	}

	public void setDuetime(String duetime) {
		this.duetime = duetime;
	}

	public String getOrguniqueid() {
		return orguniqueid;
	}

	public void setOrguniqueid(String orguniqueid) {
		this.orguniqueid = orguniqueid;
	}

	public String getAssigendto() {
		return assigendto;
	}

	public void setAssigendto(String assigendto) {
		this.assigendto = assigendto;
	}

	public String getCreatedby() {
		return createdby;
	}

	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}

	public String getModifiedby() {
		return modifiedby;
	}

	public void setModifiedby(String modifiedby) {
		this.modifiedby = modifiedby;
	}

	public String getModifeddate() {
		return modifeddate;
	}

	public void setModifeddate(String modifeddate) {
		this.modifeddate = modifeddate;
	}

	public String getShort_description() {
		return short_description;
	}

	public void setShort_description(String short_description) {
		this.short_description = short_description;
	}

	public String getLong_description() {
		return long_description;
	}

	public void setLong_description(String long_description) {
		this.long_description = long_description;
	}

	
}
