package com.model;

public class OrganizationMemberModel {
	private int orgmemberid;
	private String orgcode;
	private String orguniqueid;
	private String roleId;
	private String address;
	private String usermobileno;
	private String uid;
	private String emailid;
	private String status;
	private String membername;
	private String deviceid;
	private String createdBy;
	private String modifiedby;
	private String local_id;
	private String token;
	private String invitationto;
	private String invitedfrom;
	
	

	public String getInvitationto() {
		return invitationto;
	}

	public void setInvitationto(String invitationto) {
		this.invitationto = invitationto;
	}

	public String getInvitedfrom() {
		return invitedfrom;
	}

	public void setInvitedfrom(String invitedfrom) {
		this.invitedfrom = invitedfrom;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getLocal_id() {
		return local_id;
	}

	public void setLocal_id(String local_id) {
		this.local_id = local_id;
	}

	public int getOrgmemberid() {
		return orgmemberid;
	}

	public void setOrgmemberid(int orgmemberid) {
		this.orgmemberid = orgmemberid;
	}

	public String getOrgcode() {
		return orgcode;
	}

	public void setOrgcode(String orgcode) {
		this.orgcode = orgcode;
	}

	public String getOrguniqueid() {
		return orguniqueid;
	}

	public void setOrguniqueid(String orguniqueid) {
		this.orguniqueid = orguniqueid;
	}

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getUsermobileno() {
		return usermobileno;
	}

	public void setUsermobileno(String usermobileno) {
		this.usermobileno = usermobileno;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getEmailid() {
		return emailid;
	}

	public void setEmailid(String emailid) {
		this.emailid = emailid;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMembername() {
		return membername;
	}

	public void setMembername(String membername) {
		this.membername = membername;
	}

	public String getDeviceid() {
		return deviceid;
	}

	public void setDeviceid(String deviceid) {
		this.deviceid = deviceid;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getModifiedby() {
		return modifiedby;
	}

	public void setModifiedby(String modifiedby) {
		this.modifiedby = modifiedby;
	}

}
