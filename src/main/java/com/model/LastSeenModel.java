package com.model;

public class LastSeenModel {
	private int id;
	private String app_keyword;
	private String app_version_name;
	private String app_version_code;
	private String registered_mobile;
	private String last_used_date;
	private String imei;
	private String uid;
	private String token;
	private String deviceid;
	private int local_id;


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getApp_keyword() {
		return app_keyword;
	}

	public void setApp_keyword(String app_keyword) {
		this.app_keyword = app_keyword;
	}

	public String getApp_version_name() {
		return app_version_name;
	}

	public void setApp_version_name(String app_version_name) {
		this.app_version_name = app_version_name;
	}

	public String getApp_version_code() {
		return app_version_code;
	}

	public void setApp_version_code(String app_version_code) {
		this.app_version_code = app_version_code;
	}

	public String getRegistered_mobile() {
		return registered_mobile;
	}

	public void setRegistered_mobile(String registered_mobile) {
		this.registered_mobile = registered_mobile;
	}

	public String getLast_used_date() {
		return last_used_date;
	}

	public void setLast_used_date(String last_used_date) {
		this.last_used_date = last_used_date;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getDeviceid() {
		return deviceid;
	}

	public void setDeviceid(String deviceid) {
		this.deviceid = deviceid;
	}

	public int getLocal_id() {
		return local_id;
	}

	public void setLocal_id(int local_id) {
		this.local_id = local_id;
	}

}
