package com.model;

public class CRMmodel {

	private static final long serialVersionUID = 1L;

	private Integer rId;
	private String uid;
	private String countrycode;
	private String userMobileNo;
	private String emailid;
	private String firstName;
	private String lastName;
	private String appkeyword;
	private String deviceid;
	// private String userSimSerialNo;
	private String address;
	private String state;
	private String district;
	private String country;
	private String zipcode;
	private String otp;
	// private String userPassword;
	private String token;
	private String status;
	private String lat;
	private String longi;
	private String otpstatus;
	private String orguniqueid;

	private String firebaseuuid;
	private String firebasecreateddate;
	private String signupdate;
	private String verificationid;
	private int fbstatus;

	public String getOrguniqueid() {
		return orguniqueid;
	}

	public void setOrguniqueid(String orguniqueid) {
		this.orguniqueid = orguniqueid;
	}

	public Integer getrId() {
		return rId;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getEmailid() {
		return emailid;
	}

	public void setEmailid(String emailid) {
		this.emailid = emailid;
	}

	public String getAppkeyword() {
		return appkeyword;
	}

	public void setAppkeyword(String appkeyword) {
		this.appkeyword = appkeyword;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getLat() {
		return lat;
	}

	public void setLat(String lat) {
		this.lat = lat;
	}

	public String getLongi() {
		return longi;
	}

	public void setLongi(String longi) {
		this.longi = longi;
	}

	public String getOtpstatus() {
		return otpstatus;
	}

	public void setOtpstatus(String otpstatus) {
		this.otpstatus = otpstatus;
	}

	public void setrId(Integer rId) {
		this.rId = rId;
	}

	public String getCountrycode() {
		return countrycode;
	}

	public void setCountrycode(String countrycode) {
		this.countrycode = countrycode;
	}

	public String getUserMobileNo() {
		return userMobileNo;
	}

	public void setUserMobileNo(String userMobileNo) {
		this.userMobileNo = userMobileNo;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getDeviceid() {
		return deviceid;
	}

	public void setDeviceid(String deviceid) {
		this.deviceid = deviceid;
	}

	// public String getUserSimSerialNo() {
	// return userSimSerialNo;
	// }
	//
	// public void setUserSimSerialNo(String userSimSerialNo) {
	// this.userSimSerialNo = userSimSerialNo;
	// }

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	// public String getUserPassword() {
	// return userPassword;
	// }
	//
	// public void setUserPassword(String userPassword) {
	// this.userPassword = userPassword;
	// }

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getFirebaseuuid() {
		return firebaseuuid;
	}

	public void setFirebaseuuid(String firebaseuuid) {
		this.firebaseuuid = firebaseuuid;
	}

	public String getFirebasecreateddate() {
		return firebasecreateddate;
	}

	public void setFirebasecreateddate(String firebasecreateddate) {
		this.firebasecreateddate = firebasecreateddate;
	}

	public String getSignupdate() {
		return signupdate;
	}

	public void setSignupdate(String signupdate) {
		this.signupdate = signupdate;
	}

	public String getVerificationid() {
		return verificationid;
	}

	public void setVerificationid(String verificationid) {
		this.verificationid = verificationid;
	}

	public int getFbstatus() {
		return fbstatus;
	}

	public void setFbstatus(int fbstatus) {
		this.fbstatus = fbstatus;
	}
	
}
