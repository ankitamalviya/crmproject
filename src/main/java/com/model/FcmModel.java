package com.model;

import java.util.ArrayList;

public class FcmModel {
	private int tbid;
	private String fcmid;
	private String appkeyword;
	private String localtb_id;
	private String mobileno;
	private String uid;
	private String deviceid;
	private String token;
	private String createdby;
	private String modifiedby;
	private String title;
	private String body;
	private ArrayList<String> userMobNoArr;
	private int local_id;

	public int getLocal_id() {
		return local_id;
	}

	public void setLocal_id(int local_id) {
		this.local_id = local_id;
	}

	public ArrayList<String> getUserMobNoArr() {
		return userMobNoArr;
	}

	public int getTbid() {
		return tbid;
	}

	public void setTbid(int tbid) {
		this.tbid = tbid;
	}

	public void setUserMobNoArr(ArrayList<String> userMobNoArr) {
		this.userMobNoArr = userMobNoArr;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getFcmid() {
		return fcmid;
	}

	public void setFcmid(String fcmid) {
		this.fcmid = fcmid;
	}

	public String getAppkeyword() {
		return appkeyword;
	}

	public void setAppkeyword(String appkeyword) {
		this.appkeyword = appkeyword;
	}

	public String getLocaltb_id() {
		return localtb_id;
	}

	public void setLocaltb_id(String localtb_id) {
		this.localtb_id = localtb_id;
	}

	public String getMobileno() {
		return mobileno;
	}

	public void setMobileno(String mobileno) {
		this.mobileno = mobileno;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getDeviceid() {
		return deviceid;
	}

	public void setDeviceid(String deviceid) {
		this.deviceid = deviceid;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getCreatedby() {
		return createdby;
	}

	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}

	public String getModifiedby() {
		return modifiedby;
	}

	public void setModifiedby(String modifiedby) {
		this.modifiedby = modifiedby;
	}

}
