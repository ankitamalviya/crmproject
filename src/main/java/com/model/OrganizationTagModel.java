package com.model;

public class OrganizationTagModel {
	private int tagid;
	private String tagname;
	private String tagdescription;
	private String tagcolor;
	private String uid;
	private String orguniqueid;
	private String status;
	private String deviceid;
	private String createdby;
	private String modifiedby;
	private String local_id;
	private String token;
	
	
	
	
	
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getLocal_id() {
		return local_id;
	}
	public void setLocal_id(String local_id) {
		this.local_id = local_id;
	}
	public int getTagid() {
		return tagid;
	}
	public void setTagid(int tagid) {
		this.tagid = tagid;
	}
	public String getTagname() {
		return tagname;
	}
	public void setTagname(String tagname) {
		this.tagname = tagname;
	}
	public String getTagdescription() {
		return tagdescription;
	}
	public void setTagdescription(String tagdescription) {
		this.tagdescription = tagdescription;
	}
	public String getTagcolor() {
		return tagcolor;
	}
	public void setTagcolor(String tagcolor) {
		this.tagcolor = tagcolor;
	}
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public String getOrguniqueid() {
		return orguniqueid;
	}
	public void setOrguniqueid(String orguniqueid) {
		this.orguniqueid = orguniqueid;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDeviceid() {
		return deviceid;
	}
	public void setDeviceid(String deviceid) {
		this.deviceid = deviceid;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public String getModifiedby() {
		return modifiedby;
	}
	public void setModifiedby(String modifiedby) {
		this.modifiedby = modifiedby;
	}
	
	
	

}
