package com.model;

public class LeadTagsModel {
	private int leadtagid;
	private int leadid;
	private int tagid;
	private String deviceid;
//	private String color;
//	private String leadtagname;
	private String createdby;
	private String createddate;
	private String modifiedby;
	private String modifeddate;
	private String status;
	
	private int localtag_id;
	private String uid;
	private String token;
	private String tagcolor;
	private String tagname;
	
	

	public String getTagcolor() {
		return tagcolor;
	}

	public void setTagcolor(String tagcolor) {
		this.tagcolor = tagcolor;
	}

	public String getTagname() {
		return tagname;
	}

	public void setTagname(String tagname) {
		this.tagname = tagname;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

//	public String getLeadtagname() {
//		return leadtagname;
//	}
//
//	public void setLeadtagname(String leadtagname) {
//		this.leadtagname = leadtagname;
//	}

	public int getLocaltag_id() {
		return localtag_id;
	}

	public void setLocaltag_id(int localtag_id) {
		this.localtag_id = localtag_id;
	}

	public String getDeviceid() {
		return deviceid;
	}

	public void setDeviceid(String deviceid) {
		this.deviceid = deviceid;
	}

//	public String getColor() {
//		return color;
//	}
//
//	public void setColor(String color) {
//		this.color = color;
//	}

	public int getLeadtagid() {
		return leadtagid;
	}

	public void setLeadtagid(int leadtagid) {
		this.leadtagid = leadtagid;
	}

	public int getLeadid() {
		return leadid;
	}

	public void setLeadid(int leadid) {
		this.leadid = leadid;
	}

	public int getTagid() {
		return tagid;
	}

	public void setTagid(int tagid) {
		this.tagid = tagid;
	}

	public String getCreatedby() {
		return createdby;
	}

	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}

	public String getCreateddate() {
		return createddate;
	}

	public void setCreateddate(String createddate) {
		this.createddate = createddate;
	}

	public String getModifiedby() {
		return modifiedby;
	}

	public void setModifiedby(String modifiedby) {
		this.modifiedby = modifiedby;
	}

	public String getModifeddate() {
		return modifeddate;
	}

	public void setModifeddate(String modifeddate) {
		this.modifeddate = modifeddate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
