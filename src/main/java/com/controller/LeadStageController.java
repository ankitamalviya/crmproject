package com.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dao.BusinessDAO;
import com.dao.LeadStageDao;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.model.LeadSourceModel;
import com.model.LeadStageModel;
import com.security.EncryptDecryptStringWithAES;
import com.security.SendNotification;

@RestController
public class LeadStageController {
	// StaticData staticData = new StaticData(); // calling static class
	@Autowired
	LeadStageDao leadStageDao;
	@Autowired
	BusinessDAO businessDAO;
	ObjectMapper objectMapper = new ObjectMapper();
	String json = "";
	String json1 = "";
	EncryptDecryptStringWithAES encry = new EncryptDecryptStringWithAES();

	/**********************************
	 * Add and Update Lead Source
	 * 
	 * @throws Exception
	 ********************************/
	@RequestMapping(value = "/addleadstage", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public String addleadstage(@RequestBody List<LeadStageModel> leadStageModel) throws Exception {

		Map<String, Object> response = new HashMap<String, Object>();
		Map<String, Object> result = new HashMap<String, Object>();
		List<LeadStageModel> list = new ArrayList<LeadStageModel>();
		int status = 0;

		SendNotification send = new SendNotification();
		String sendnotification = null;

		Map<String, Object> verification = businessDAO.downloadToken(leadStageModel.get(0).getUid(),
				leadStageModel.get(0).getDeviceid());
		int stat = (int) verification.get("status");
		System.out.println(stat);
		if (stat == 106) {
			String token = (String) verification.get("token");
			String detoken = encry.decrypt(leadStageModel.get(0).getToken());

			if ((detoken).equals(token) && token != null && !token.equals("")) {

				JSONObject jsonObject = null, jObj = null;
				JSONArray jArr = null;
				LeadStageModel model = null;
				try {
					for (int j = 0; j < leadStageModel.size(); j++) {
						LeadStageModel data = leadStageModel.get(j);
						result = leadStageDao.addleadstage(data);
						status = (int) result.get("status");

						if (status == 106) {
							// converting map object into JSONArray Object
							json = objectMapper.writeValueAsString(result);
							jsonObject = new JSONObject(json);
							jArr = jsonObject.getJSONArray("#result-set-1");
							if (jArr.length() > 0) {
								for (int i = 0; i < jArr.length(); i++) {
									JSONObject MainObj = jArr.getJSONObject(i);
									model = new LeadStageModel();
									model.setStageid(MainObj.getInt("stageid"));
									model.setStagename(MainObj.getString("stagename"));
									String uid = encry.encrypt(MainObj.getString("uid"));
									model.setUid(uid);
									String orguniqueid = encry.encrypt(MainObj.getString("orguniqueid"));
									model.setOrguniqueid(orguniqueid);
									model.setStatus(MainObj.getInt("status"));
									model.setCreateddate(MainObj.getString("createddate"));
									model.setCreatedby(MainObj.getString("createdby"));
									model.setDeviceid(MainObj.getString("deviceid"));
									model.setModifiedby(MainObj.getString("modifiedby"));
									model.setModifieddate(MainObj.getString("modifieddate"));
									model.setLocal_id(leadStageModel.get(j).getLocal_id());

								}
								list.add(model);
							}

						}
					}

					System.out.println("status in controller" + status);
					if (status == 106) {
						System.out.println("status 106");
						response.put("StatusCode", 200);
						response.put("response", "Data Added");
						response.put("response", list);
					} else if (status == 107) {

						String uid = encry.decrypt(leadStageModel.get(0).getUid());
						System.out.println(uid);
						String body = "leadStage assinged to you";
					
						System.out.println("Notification status 106");
						sendnotification = send.Notification(uid, "Stage Assinged", body);
					

						response.put("StatusCode", 204);
						response.put("response", "Data Updated !!!");
						response.put("response", list);
						response.put("Notification Status", sendnotification);

					} else if (status == 404) {
						response.put("StatusCode", 404);
						response.put("response", "Server Error!!!");
					}
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
		} else if (stat == 107) {
			response.put("StatusCode", 405);
			response.put("Message", "UnAuthorized user!!!");
		}

		json = objectMapper.writeValueAsString(response);
		return json;
	}

	/**********************************
	 * Download Lead details
	 * 
	 * @throws JsonProcessingException
	 ********************************/
	@RequestMapping(value = "/downloadleadstage", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public String downloadleadstage(@RequestBody List<LeadStageModel> leadStageModel) throws Exception {
		System.out.println(encry.encrypt(leadStageModel.get(0).getToken()));

		Map<String, Object> response = new HashMap<String, Object>();
		Map<String, Object> result = new HashMap<String, Object>();
		List<LeadStageModel> list = new ArrayList<LeadStageModel>();

		Map<String, Object> verification = businessDAO.downloadToken(leadStageModel.get(0).getUid(),
				leadStageModel.get(0).getDeviceid());
		int stat = (int) verification.get("status");

		if (stat == 106) {
			String token = (String) verification.get("token");
			String detoken = encry.decrypt(leadStageModel.get(0).getToken());

			if ((detoken).equals(token) && token != null && !token.equals("")) {
				System.out.println("inside token if.........");
				result = leadStageDao.downloadleadstage(leadStageModel.get(0).getOrguniqueid());
				json = objectMapper.writeValueAsString(result);
				JSONObject jsonObj = new JSONObject(json);
				if (jsonObj.has("#result-set-1")) {
					JSONArray jsonArray = jsonObj.getJSONArray("#result-set-1");
					if (jsonArray.length() > 0) {
						for (int i = 0; i < jsonArray.length(); i++) {
							JSONObject MainObj = jsonArray.getJSONObject(i);
							LeadStageModel model = new LeadStageModel();
							model.setStageid(MainObj.getInt("stageid"));
							model.setStagename(MainObj.getString("stagename"));
							String uid = encry.encrypt(MainObj.getString("uid"));
							model.setUid(uid);
							String orguniqueid = encry.encrypt(MainObj.getString("orguniqueid"));
							model.setOrguniqueid(orguniqueid);
							model.setStatus(MainObj.getInt("status"));
							model.setCreateddate(MainObj.getString("createddate"));
							model.setCreatedby(MainObj.getString("createdby"));
							model.setDeviceid(MainObj.getString("deviceid"));
							model.setModifiedby(MainObj.getString("modifiedby"));
							model.setModifieddate(MainObj.getString("modifieddate"));
							list.add(model);
						}

						response.put("response", list);
						response.put("StatusCode", 200);
						response.put("Message", "Download Completed");
					} else {
						response.put("StatusCode", 204);
						response.put("response", list);
						response.put("Message", "No data found!!!");
					}
				} else {
					response.put("StatusCode", 204);
					response.put("Message", "No data found!!!");
				}
			}
		} else if (stat == 107) {
			response.put("StatusCode", 405);
			response.put("Message", "UnAuthorized user!!!");
		}
		json = objectMapper.writeValueAsString(response);
		return json;
	}

}
