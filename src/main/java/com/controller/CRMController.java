package com.controller;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.ws.rs.QueryParam;

import org.hibernate.engine.transaction.jta.platform.internal.SynchronizationRegistryBasedSynchronizationStrategy;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dao.BusinessDAO;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.model.AppVersionDetails;
import com.model.CRMmodel;
import com.model.CreateOrganization;
import com.model.CreateOrganization1;
import com.model.LastSeenModel;
import com.model.LeadModel;
import com.model.LeadSourceModel;
import com.model.LeadTagsModel;
import com.model.MemberDetailsOfSameOrg;
import com.model.OrganizationMemberModel;
import com.model.PaymentTokenModel;
import com.model.StaticData;
import com.security.EncryptDecryptStringWithAES;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

@RestController
public class CRMController {

	// For maintaining the log files
	// public static final Logger logger =
	// Logger.getLogger(HomeController.class);

	StaticData staticData = new StaticData(); // calling static class

	// Injecting DAO class
	@Autowired
	BusinessDAO businessDAO;
	final DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	ObjectMapper mapper = new ObjectMapper();
	String json = "";
	String json1 = "";
	EncryptDecryptStringWithAES encry = new EncryptDecryptStringWithAES();

	/******** USER REGISTRATION *******/

	// @RequestMapping(value = "/register", method = RequestMethod.POST,
	// consumes = "application/json", produces = "application/json")
	// public String registerToMySociety(@RequestBody List<CRMmodel> data1)
	// throws Exception {
	// // calling register method from dao class
	// System.out.println(data1.get(0).getUserMobileNo());
	// int maxId = 0;
	// int status = 0;
	// CRMmodel model = data1.get(0);
	// String userMobileNo = model.getUserMobileNo();
	// Map<String, Object> map = new HashMap<String, Object>();
	// Map<String, Object> map1 = new HashMap<String, Object>();
	// List<CRMmodel> list = new ArrayList<CRMmodel>();
	// map = businessDAO.register(model);
	// maxId = (int) map.get("maxid");
	// status = (int) map.get("status");
	// String datain, enc, encm, encmob;
	//
	// if (status == 106 || status == 105 || status == 104 || status == 103) {
	// map = businessDAO.getUserRegisterData(userMobileNo);
	// json = mapper.writeValueAsString(map);
	// JSONObject object = new JSONObject(json);
	// JSONArray data = object.getJSONArray("#result-set-1");
	// if (data.length() > 0) {
	//
	// for (int i = 0; i < data.length(); i++) {
	// CRMmodel crm = new CRMmodel();
	// JSONObject jsonObject = data.getJSONObject(i);
	// enc = (jsonObject.getString("uid"));
	// datain = encry.encrypt(enc);
	// System.out.println(datain);
	// crm.setrId(jsonObject.getInt("rId"));
	// crm.setUid(datain);
	// encm = (jsonObject.getString("userMobileNo"));
	// encmob = encry.encrypt(encm);
	// crm.setUserMobileNo(encmob);
	// // crm.setUserMobileNo(jsonObject.getString("userMobileNo"));
	// crm.setEmailid(jsonObject.getString("emailid"));
	// crm.setFirstName(jsonObject.getString("firstName"));
	// crm.setLastName(jsonObject.getString("lastName"));
	// // crm.setCompanyname(jsonObject.getString("companyname"));
	// crm.setAppkeyword(jsonObject.getString("appkeyword"));
	// crm.setState(jsonObject.getString("state"));
	// crm.setDeviceid(jsonObject.getString("deviceid"));
	// // crm.setDistrict(jsonObject.getString("district"));
	// crm.setCountry(jsonObject.getString("country"));
	// crm.setZipcode(jsonObject.getString("zipcode"));
	// crm.setLat(jsonObject.getString("lat"));
	// crm.setLongi(jsonObject.getString("longi"));
	// // crm.setOtpstatus(jsonObject.getString("otpstatus"));
	// crm.setOtp(jsonObject.getString("otp"));
	// String token = encry.encrypt(jsonObject.getString("authtoken"));
	// crm.setToken(token);
	// list.add(i, crm);
	// }
	// }
	// }
	//
	// try {
	// if (status == 106) {
	// map1.put("StatusCode", StaticData.NEW_REGISTRATION);
	// map1.put("response", list);
	// map1.put("message", StaticData.REGISTRATION_SUCCESS);
	// } else if (status == 105) {
	// map1.put("StatusCode", StaticData.REGISTRATION_SAME_OTP_NOT_VERIFIED);
	// map1.put("response", list);
	// map1.put("message", StaticData.ALREADY_REGISTERED_VERIFY_OTP);
	// } else if (status == 104) {
	// map1.put("StatusCode", StaticData.REGISTRATION_SAME_OTP_VERIFIED);
	// map1.put("response", list);
	// map1.put("message", StaticData.ALREADY_REGISTERED_OTP_VERIFIED);
	// }
	//
	// else if (status == 103) {
	// map1.put("StatusCode", StaticData.REGISTER_FROM_NEW_DEVICE);
	// map1.put("response", list);
	// map1.put("message", StaticData.DEVICE_CHANGED);
	// } else {
	// map1.put("StatusCode", 404);
	// // map.put("response", data);
	// map1.put("message", "error");
	// }
	// } catch (NullPointerException e) {
	// System.out.println(e);
	// }
	// json1 = mapper.writeValueAsString(map1);
	// return json1;
	// }

	@RequestMapping(value = "/register", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public String registerToMySociety(@RequestBody List<CRMmodel> data1) throws Exception {
		// calling register method from dao class
		System.out.println(data1.get(0).getUserMobileNo());
		int maxId = 0;
		int status = 0;
		CRMmodel model = data1.get(0);
		String userMobileNo = model.getUserMobileNo();
		Map<String, Object> map = new HashMap<String, Object>();
		Map<String, Object> map1 = new HashMap<String, Object>();
		List<CRMmodel> list = new ArrayList<CRMmodel>();
		map = businessDAO.register(model);
		maxId = (int) map.get("maxid");
		status = (int) map.get("status");
		String datain, enc, encm, encmob;

		if (status == 106 || status == 105 || status == 104 || status == 103 || status == 108) {
			map = businessDAO.getUserRegisterData(userMobileNo);
			json = mapper.writeValueAsString(map);
			JSONObject object = new JSONObject(json);
			JSONArray data = object.getJSONArray("#result-set-1");
			if (data.length() > 0) {

				for (int i = 0; i < data.length(); i++) {
					CRMmodel crm = new CRMmodel();
					JSONObject jsonObject = data.getJSONObject(i);
					enc = (jsonObject.getString("uid"));
					datain = encry.encrypt(enc);
					System.out.println(datain);
					crm.setrId(jsonObject.getInt("rId"));
					crm.setUid(datain);
					encm = (jsonObject.getString("userMobileNo"));
					encmob = encry.encrypt(encm);
					crm.setUserMobileNo(encmob);
					// crm.setUserMobileNo(jsonObject.getString("userMobileNo"));
					crm.setEmailid(jsonObject.getString("emailid"));
					crm.setFirstName(jsonObject.getString("firstName"));
					crm.setLastName(jsonObject.getString("lastName"));
					// crm.setCompanyname(jsonObject.getString("companyname"));
					crm.setAppkeyword(jsonObject.getString("appkeyword"));
					crm.setState(jsonObject.getString("state"));
					crm.setDeviceid(jsonObject.getString("deviceid"));

					crm.setFirebaseuuid(jsonObject.getString("firebaseuuid"));
					crm.setFirebasecreateddate(jsonObject.getString("firebasecreateddate"));
					crm.setSignupdate(jsonObject.getString("signupdate"));
					crm.setVerificationid(jsonObject.getString("verificationid"));

					// crm.setDistrict(jsonObject.getString("district"));
					crm.setCountry(jsonObject.getString("country"));
					crm.setZipcode(jsonObject.getString("zipcode"));
					crm.setLat(jsonObject.getString("lat"));
					crm.setLongi(jsonObject.getString("longi"));
					// crm.setOtpstatus(jsonObject.getString("otpstatus"));
					crm.setOtp(jsonObject.getString("otp"));
					String token = encry.encrypt(jsonObject.getString("authtoken"));
					crm.setToken(token);
					list.add(i, crm);
				}
			}
		}

		try {
			if (status == 106) {
				map1.put("StatusCode", StaticData.NEW_REGISTRATION);
				map1.put("response", list);
				map1.put("message", StaticData.REGISTRATION_SUCCESS);
			} else if (status == 108) {
				map1.put("StatusCode", StaticData.NEW_REGISTRATIONFIREBASE);
				map1.put("response", list);
				map1.put("message", StaticData.REGISTRATION_SUCCESS);
			} else if (status == 105) {
				map1.put("StatusCode", StaticData.REGISTRATION_SAME_OTP_NOT_VERIFIED);
				map1.put("response", list);
				map1.put("message", StaticData.ALREADY_REGISTERED_VERIFY_OTP);
			} else if (status == 104) {
				map1.put("StatusCode", StaticData.REGISTRATION_SAME_OTP_VERIFIED);
				map1.put("response", list);
				map1.put("message", StaticData.ALREADY_REGISTERED_OTP_VERIFIED);
			}

			else if (status == 103) {
				map1.put("StatusCode", StaticData.REGISTER_FROM_NEW_DEVICE);
				map1.put("response", list);
				map1.put("message", StaticData.DEVICE_CHANGED);
			} else {
				map1.put("StatusCode", 404);
				// map.put("response", data);
				map1.put("message", "error");
			}
		} catch (NullPointerException e) {
			System.out.println(e);
		}
		json1 = mapper.writeValueAsString(map1);
		return json1;
	}

	/**********
	 * OTP VERIFICATION
	 * 
	 * @throws Exception
	 ************/
	@RequestMapping(value = "/otpverify", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public String OTPVerify(@RequestBody List<CRMmodel> data1) throws Exception {

		int res = businessDAO.OTPVerification(data1);
		System.out.println(res);
		List<CRMmodel> data = businessDAO.getData(data1);

		Map<String, Object> map = new HashMap<String, Object>();

		if (res == 106) {
			map.put("StatusCode", StaticData.OTP_VERIFIED);
			map.put("response", data);
			map.put("message", "OTP Verification Success");
		} else if (res == 103) {
			map.put("StatusCode", StaticData.OTP_NOT_VERIFIED);
			map.put("message", "OTP Verification Failed");
		} else {
			map.put("StatusCode", HttpURLConnection.HTTP_INTERNAL_ERROR);
			map.put("message", "error");
		}

		json = mapper.writeValueAsString(map);
		return json;
	}

	@RequestMapping(value = "/societyapp/forgetpassword", method = RequestMethod.GET, produces = "application/json")
	public String forgetPassword(@QueryParam("userMobileNo") String userMobileNo) throws JsonProcessingException {

		Map<Object, Object> map = new HashMap<Object, Object>();

		// String DBToken =
		// businessDAO.getTokenForAuthentication(loginMobileNo);

		// if (DBToken.equals(tokenId)) {

		String obj = businessDAO.forgetPassword(userMobileNo);

		if (obj.length() > 0) {
			map.put("StatusCode", StaticData.FORGET_PASSWORD_SENT);
			map.put("response", obj);
			map.put("message", StaticData.FORGET_PASSWORD_MSG);
		} else {
			map.put("StatusCode", StaticData.FORGET_PASSWORD_FAILED);
			map.put("message", StaticData.FORGET_PASSWORD_FAILED_MSG);
		}

		// } else {
		// map.put("StatusCode", 405);
		// map.put("message", "Unauthorised Access");
		// }

		json = mapper.writeValueAsString(map);
		return json;
	}

	/************************************
	 * Get Token
	 * 
	 * @throws Exception
	 ************************************/
	/*
	 * @RequestMapping(value = "/getToken", method = RequestMethod.GET, produces
	 * = "application/json") public String downloadToken(@QueryParam("uId")
	 * String uId) throws JsonProcessingException { // , @QueryParam("imeiNo")
	 * String imeiNo Map<String, Object> result =
	 * businessDAO.downloadTokenId(uId); // , imeiNo Map<Object, Object> map =
	 * new HashMap<Object, Object>();
	 * 
	 * int status = (int) result.get("Status");
	 * 
	 * if (status == 106) { String token = (String) result.get("Token");
	 * System.out.println(token); map.put("StatusCode", 200); map.put("Token",
	 * token); map.put("message", "success"); } else if (status == 107) {
	 * map.put("StatusCode", 404); map.put("message", "no token found"); }
	 * 
	 * json = mapper.writeValueAsString(map); return json; }
	 */

	@RequestMapping(value = "/downloadempStrength", method = RequestMethod.GET, produces = "application/json")
	public String downloadempStrength() throws Exception {
		Map<String, Object> response = new HashMap<String, Object>();
		Map<String, Object> result = new HashMap<String, Object>();
		List<CreateOrganization> list = new ArrayList<CreateOrganization>();
		System.out.println(encry.encrypt("19560fa4-737e-4cd2-9780-704d039f0891"));
		result = businessDAO.downloadempStrength();
		json = mapper.writeValueAsString(result);
		JSONObject jsonObj = new JSONObject(json);
		if (jsonObj.has("#result-set-1")) {
			JSONArray jsonArray = jsonObj.getJSONArray("#result-set-1");
			if (jsonArray.length() > 0) {
				for (int i = 0; i < jsonArray.length(); i++) {
					JSONObject MainObj = jsonArray.getJSONObject(i);
					CreateOrganization org = new CreateOrganization();
					org.setEmpstrength(MainObj.getString("empstrength"));
					list.add(org);
				}

				response.put("response", list);
				response.put("StatusCode", 200);
				response.put("Message", "Download Completed");
			} else {
				response.put("StatusCode", 204);
				response.put("Message", "No data found!!!");
			}
		} else {
			response.put("StatusCode", 204);
			response.put("Message", "No data found!!!");
		}
		json = mapper.writeValueAsString(response);
		return json;
	}

	/********
	 * resend otp
	 * 
	 * @throws Exception
	 *******/

	@RequestMapping(value = "/resendotp", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public String resendotp(@RequestBody List<CRMmodel> data1)
			throws JSONException, IOException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException,
			IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException, Exception {

		System.out.println(encry.decrypt(data1.get(0).getUid()));
		String uid1 = data1.get(0).getUid();

		String deviceid = data1.get(0).getDeviceid();
		String uid = encry.decrypt(uid1);
		System.out.println(uid);

		Map<String, Object> result = new HashMap<String, Object>();
		Map<String, Object> response = new HashMap<String, Object>();

		try {

			result = businessDAO.resendOTP(uid, deviceid);
			int status = (int) result.get("status");
			if (status == 200) {
				CRMmodel data = new CRMmodel();
				response.put("StatusCode", 200);
				response.put("response", data);
				response.put("Message", "OTP sent to registered mobile no.!!!");
			} else if (status == 400) {
				response.put("StatusCode", 400);
				response.put("Message", "OTP sending failed!!");
			}

		} catch (Exception e) {
			// TODO: handle exception
			response.put("StatusCode", 500);
			response.put("Message", "Exception Occured!!");
		}

		json = mapper.writeValueAsString(response);
		return json;
	}

	/*****************************************
	 * Create Organization
	 * 
	 * @throws Exception
	 ***************************************/
	@RequestMapping(value = "/createorganization", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public String createorganization(@RequestBody List<CreateOrganization> createorg) throws Exception {

		Map<String, Object> result = new HashMap<String, Object>();
		Map<String, Object> response = new HashMap<String, Object>();
		CreateOrganization data = createorg.get(0);
		JSONObject jsonObject = null, jObj = null;
		JSONArray jArr = null;
		CreateOrganization model = null;
		List<CreateOrganization> list = new ArrayList<CreateOrganization>();
		int status = 0;
		String enc, datain;
		Map<String, Object> verification = businessDAO.downloadToken(data.getUid(), data.getDeviceid());
		int stat = (int) verification.get("status");
		System.out.println(stat);
		if (stat == 106) {
			String token = (String) verification.get("token");
			String detoken = encry.decrypt(data.getToken());

			if ((detoken).equals(token) && token != null && !token.equals("")) {
				try {

					result = businessDAO.createorganization(data);

					status = (int) result.get("o_status");
					System.out.println(status);

					if (status == 106) {

						// converting map object into JSONArray Object
						json = mapper.writeValueAsString(result);
						jsonObject = new JSONObject(json);
						jArr = jsonObject.getJSONArray("#result-set-1");
						if (jArr.length() > 0) {
							for (int i = 0; i < jArr.length(); i++) {
								model = new CreateOrganization();
								jObj = jArr.getJSONObject(i);
								String orguniqueid = encry.encrypt(jObj.getString("orguniqueid"));
								model.setOrguniqueid(orguniqueid);
								String createdby = encry.encrypt(jObj.getString("createdby"));
								model.setCreatedBy(createdby);
								model.setOrgcode(jObj.getString("orgcode"));
								model.setOrgid(jObj.getInt("orgid"));
								model.setOrgname(jObj.getString("orgname"));
								model.setLocal_id(data.getLocal_id());

							}
							list.add(model);
						}
						response.put("StatusCode", 200);
						response.put("response", "Data Added");
						response.put("response", list);

					}

					else if (status == 107) {

						response.put("StatusCode", 204);
						response.put("response", list);
						response.put("Message", "Data updated!!!");
					}

					else if (status == 404) {
						response.put("StatusCode", 404);
						response.put("Message", "Something went wrong!!!");
					}

				} catch (Exception e) {
					System.out.println(e);

				}
			}
		} else if (stat == 107) {
			response.put("StatusCode", 405);
			response.put("Message", "UnAuthorized user!!!");
		}

		json = mapper.writeValueAsString(response);
		return json;
	}

	// @RequestMapping(value = "/addorganizationmembers", method =
	// RequestMethod.POST, consumes = "application/json", produces =
	// "application/json")
	// public String addorganizationmembers(@RequestBody
	// List<OrganizationMemberModel> orgmember)
	// throws Exception {
	//
	// Map<String, Object> response = new HashMap<String, Object>();
	// Map<String, Object> result = new HashMap<String, Object>();
	// List<OrganizationMemberModel> list = new
	// ArrayList<OrganizationMemberModel>();
	// OrganizationMemberModel orgData = null;
	// JSONObject jsonObject = null, jObj = null;
	// JSONArray jArr = null;
	// int status = 0;
	// Map<String, Object> verification =
	// businessDAO.downloadToken(orgmember.get(0).getUid(),
	// orgmember.get(0).getDeviceid());
	// int stat = (int) verification.get("status");
	// System.out.println(stat);
	// if (stat == 106) {
	// String token = (String) verification.get("token");
	// String detoken = encry.decrypt(orgmember.get(0).getToken());
	//
	// if ((detoken).equals(token) && token != null && !token.equals("")) {
	//
	// try {
	// for (int j = 0; j < orgmember.size(); j++) {
	// OrganizationMemberModel data = orgmember.get(0);
	// result = businessDAO.addorganizationmembers(data);
	//
	// status = (int) result.get("o_status");
	// System.out.println(status);
	// if (status == 106) {
	// json = mapper.writeValueAsString(result);
	// jsonObject = new JSONObject(json);
	// jArr = jsonObject.getJSONArray("#result-set-1");
	// if (jArr.length() > 0) {
	// for (int i = 0; i < jArr.length(); i++) {
	// orgData = new OrganizationMemberModel();
	// jObj = jArr.getJSONObject(i);
	// orgData.setOrgmemberid(jObj.getInt("orgmemberid"));
	// String orguniqueid = encry.encrypt(jObj.getString("orguniqueid"));
	// orgData.setOrguniqueid(orguniqueid);
	// orgData.setOrgcode(jObj.getString("orgcode"));
	// orgData.setCreatedBy(jObj.getString("createdby"));
	// orgData.setLocal_id(orgmember.get(0).getLocal_id());
	// }
	// list.add(orgData);
	// }
	// }
	// }
	// System.out.println("list...." + list.toString());
	// if (status == 106) {
	//
	// response.put("StatusCode", 200);
	// response.put("response", list);
	// response.put("Message", "Data saved!!!");
	// }
	//
	// else if (status == 107) {
	// response.put("StatusCode", 204);
	// response.put("response", list);
	// response.put("Message", "Data updated!!!");
	// }
	//
	// else if (status == 404) {
	// response.put("StatusCode", 404);
	// response.put("Message", "Something went wrong!!!");
	// }
	//
	// } catch (Exception e) {
	// // TODO: handle exception
	// }
	// }
	// }else if (stat == 107) {
	// response.put("StatusCode", 405);
	// response.put("Message", "UnAuthorized user!!!");
	// }
	// json = mapper.writeValueAsString(response);
	// return json;
	// }

	@RequestMapping(value = "/addorganizationmembers", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public String addorganizationmembers(@RequestBody List<OrganizationMemberModel> orgmember) throws Exception {

		Map<String, Object> response = new HashMap<String, Object>();
		Map<String, Object> result = new HashMap<String, Object>();
		List<OrganizationMemberModel> list = new ArrayList<OrganizationMemberModel>();
		OrganizationMemberModel orgData = null;
		JSONObject jsonObject = null, jObj = null;
		JSONArray jArr = null;
		int status = 0;
		Map<String, Object> verification = businessDAO.downloadToken(orgmember.get(0).getUid(),
				orgmember.get(0).getDeviceid());
		int stat = (int) verification.get("status");
		System.out.println(stat);
		if (stat == 106) {
			String token = (String) verification.get("token");
			String detoken = encry.decrypt(orgmember.get(0).getToken());

			if ((detoken).equals(token) && token != null && !token.equals("")) {

				try {
					for (int j = 0; j < orgmember.size(); j++) {
						OrganizationMemberModel data = orgmember.get(0);
						result = businessDAO.addorganizationmembers(data);

						status = (int) result.get("o_status");
						System.out.println(status);
						if (status == 106) {
							json = mapper.writeValueAsString(result);
							jsonObject = new JSONObject(json);
							jArr = jsonObject.getJSONArray("#result-set-1");
							if (jArr.length() > 0) {
								for (int i = 0; i < jArr.length(); i++) {
									orgData = new OrganizationMemberModel();
									jObj = jArr.getJSONObject(i);
									orgData.setOrgmemberid(jObj.getInt("orgmemberid"));
									String orguniqueid = encry.encrypt(jObj.getString("orguniqueid"));
									orgData.setOrguniqueid(orguniqueid);
									orgData.setOrgcode(jObj.getString("orgcode"));
									orgData.setCreatedBy(jObj.getString("createdby"));
									orgData.setLocal_id(orgmember.get(0).getLocal_id());
								}
								list.add(orgData);
							}
						}
					}
					System.out.println("list...." + list.toString());
					if (status == 106) {

						response.put("StatusCode", 200);
						response.put("response", list);
						response.put("Message", "Data saved!!!");
					}

					else if (status == 107) {
						response.put("StatusCode", 204);
						response.put("response", list);
						response.put("Message", "Data updated!!!");
					}

					else if (status == 404) {
						response.put("StatusCode", 404);
						response.put("Message", "Something went wrong!!!");
					}

				} catch (Exception e) {
					// TODO: handle exception
				}
			}
		} else if (stat == 107) {
			response.put("StatusCode", 405);
			response.put("Message", "UnAuthorized user!!!");
		}
		json = mapper.writeValueAsString(response);
		return json;
	}

	@RequestMapping(value = "/downloadOrganizationDetails", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public String downloadOrganizationDetails(@RequestBody List<CreateOrganization> createorg) throws Exception {

		Map<String, Object> response = new HashMap<String, Object>();
		Map<String, Object> result = new HashMap<String, Object>();
		CreateOrganization data = createorg.get(0);
		List<CreateOrganization> list = new ArrayList<CreateOrganization>();

		Map<String, Object> verification = businessDAO.downloadToken(data.getUid(), data.getDeviceid());
		int stat = (int) verification.get("status");
		System.out.println(stat);
		if (stat == 106) {
			String token = (String) verification.get("token");
			String detoken = encry.decrypt(data.getToken());

			if ((detoken).equals(token) && token != null && !token.equals("")) {
				String orgunique, orguniqueEnc;
				result = businessDAO.downloadOrganizationDetails(data);
				int status = (int) result.get("o_status");
				json = mapper.writeValueAsString(result);
				JSONObject jsonObj = new JSONObject(json);
				JSONArray jsonArray = jsonObj.getJSONArray("#result-set-1");
				if (jsonArray.length() > 0) {
					for (int i = 0; i < jsonArray.length(); i++) {
						JSONObject MainObj = jsonArray.getJSONObject(i);
						CreateOrganization orgdetails = new CreateOrganization();
						orgdetails.setOrgid(MainObj.getInt("orgid"));
						orgdetails.setOrgname(MainObj.getString("orgname"));
						orgdetails.setOrgcode(MainObj.getString("orgcode"));
						orgdetails.setOrguniqueid(MainObj.getString("orguniqueid"));
						orgunique = (MainObj.getString("orguniqueid"));
						orguniqueEnc = encry.encrypt(orgunique);
						orgdetails.setOrguniqueid(orguniqueEnc);
						String uid = encry.encrypt(MainObj.getString("uid"));
						orgdetails.setUid(uid);
						orgdetails.setCity(MainObj.getString("city"));
						orgdetails.setZipcode(MainObj.getString("zipcode"));
						orgdetails.setUdf1(MainObj.getString("udf1"));
						orgdetails.setState(MainObj.getString("state"));
						orgdetails.setCountry(MainObj.getString("country"));
						orgdetails.setUserMobileNo(MainObj.getString("userMobileNo"));
						orgdetails.setLogo(MainObj.getString("logo"));
						orgdetails.setDomain(MainObj.getString("domain"));
						orgdetails.setEmpstrength(MainObj.getString("empstrength"));
						orgdetails.setLongitude(MainObj.getString("longitude"));
						orgdetails.setLatitude(MainObj.getString("latitude"));
						orgdetails.setDeviceid(MainObj.getString("deviceid"));
						orgdetails.setCreatedBy(MainObj.getString("createdby"));
						list.add(orgdetails);

					}
					response.put("StatusCode", 200);
					response.put("response", list);
					response.put("Message", "Organization Details data found!!!");
				} else {
					response.put("StatusCode", 204);
					response.put("Message", "No Data found");

				}
			}
		} else if (stat == 107) {
			response.put("StatusCode", 405);
			response.put("Message", "UnAuthorized user!!!");
		}
		json = mapper.writeValueAsString(response);
		return json;
	}

	// @RequestMapping(value = "/downloadOrganizationMemberList", method =
	// RequestMethod.POST, consumes = "application/json", produces =
	// "application/json")
	// public String downloadOrganizationMemberList(@RequestBody
	// List<OrganizationMemberModel> orgmember)
	// throws Exception {
	//
	// Map<String, Object> response = new HashMap<String, Object>();
	// Map<String, Object> result = new HashMap<String, Object>();
	// OrganizationMemberModel data = orgmember.get(0);
	// List<OrganizationMemberModel> list = new
	// ArrayList<OrganizationMemberModel>();
	// Map<String, Object> verification =
	// businessDAO.downloadToken(data.getUid(), data.getDeviceid());
	// int stat = (int) verification.get("status");
	// System.out.println(stat);
	// if (stat == 106) {
	// String token = (String) verification.get("token");
	// String detoken = encry.decrypt(data.getToken());
	//
	// if ((detoken).equals(token) && token != null && !token.equals("")) {
	// String orgunique, orguniqueEnc;
	// result = businessDAO.downloadOrganizationMemberList(data);
	// int status = (int) result.get("o_status");
	// System.out.println(status);
	// json = mapper.writeValueAsString(result);
	// JSONObject jsonObj = new JSONObject(json);
	// JSONArray jsonArray = jsonObj.getJSONArray("#result-set-1");
	// if (jsonArray.length() > 0) {
	// for (int i = 0; i < jsonArray.length(); i++) {
	//
	// JSONObject MainObj = jsonArray.getJSONObject(i);
	// OrganizationMemberModel orgdetails = new OrganizationMemberModel();
	// orgdetails.setOrgmemberid(MainObj.getInt("orgmemberid"));
	// orgdetails.setOrgcode(MainObj.getString("orgcode"));
	// // orgdetails.setOrguniqueid(MainObj.getString("orguniqueid"));
	//
	// orgunique = (MainObj.getString("orguniqueid"));
	// orguniqueEnc = encry.encrypt(orgunique);
	// orgdetails.setOrguniqueid(orguniqueEnc);
	//
	// orgdetails.setRoleId(MainObj.getString("roleId"));
	// orgdetails.setAddress(MainObj.getString("address"));
	// orgdetails.setUsermobileno(MainObj.getString("usermobileno"));
	// String uid = encry.encrypt(MainObj.getString("uid"));
	// orgdetails.setUid(uid);
	// orgdetails.setEmailid(MainObj.getString("emailid"));
	// orgdetails.setStatus(MainObj.getString("status"));
	// orgdetails.setMembername(MainObj.getString("membername"));
	// orgdetails.setDeviceid(MainObj.getString("deviceid"));
	// list.add(orgdetails);
	//
	// }
	// response.put("StatusCode", 200);
	// response.put("response", list);
	// response.put("Message", "Organization Member Details data found!!!");
	// } else {
	// response.put("StatusCode", 204);
	// response.put("Message", "No Data found");
	//
	// }
	// }
	// } else if (stat == 107) {
	// response.put("StatusCode", 405);
	// response.put("Message", "UnAuthorized user!!!");
	// }
	// json = mapper.writeValueAsString(response);
	// return json;
	// }

	@RequestMapping(value = "/downloadOrganizationMemberList", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public String downloadOrganizationMemberList(@RequestBody List<OrganizationMemberModel> orgmember)
			throws Exception {
		System.out.println(encry.decrypt(orgmember.get(0).getUid()));
		Map<String, Object> response = new HashMap<String, Object>();
		Map<String, Object> result = new HashMap<String, Object>();
		OrganizationMemberModel data = orgmember.get(0);
		List<OrganizationMemberModel> list = new ArrayList<OrganizationMemberModel>();
		Map<String, Object> verification = businessDAO.downloadToken(data.getUid(), data.getDeviceid());
		int stat = (int) verification.get("status");
		System.out.println(stat);
		if (stat == 106) {
			String token = (String) verification.get("token");
			String detoken = encry.decrypt(data.getToken());

			if ((detoken).equals(token) && token != null && !token.equals("")) {
				String orgunique, orguniqueEnc;
				result = businessDAO.downloadOrganizationMemberList(data);
				int status = (int) result.get("o_status");
				System.out.println(status);
				json = mapper.writeValueAsString(result);
				JSONObject jsonObj = new JSONObject(json);
				JSONArray jsonArray = jsonObj.getJSONArray("#result-set-1");
				if (jsonArray.length() > 0) {
					for (int i = 0; i < jsonArray.length(); i++) {

						JSONObject MainObj = jsonArray.getJSONObject(i);
						OrganizationMemberModel orgdetails = new OrganizationMemberModel();
						orgdetails.setOrgmemberid(MainObj.getInt("orgmemberid"));
						orgdetails.setOrgcode(MainObj.getString("orgcode"));
						// orgdetails.setOrguniqueid(MainObj.getString("orguniqueid"));

						orgunique = (MainObj.getString("orguniqueid"));
						orguniqueEnc = encry.encrypt(orgunique);
						orgdetails.setOrguniqueid(orguniqueEnc);

						orgdetails.setRoleId(MainObj.getString("roleId"));
						orgdetails.setAddress(MainObj.getString("address"));
						orgdetails.setInvitationto(MainObj.getString("invitationto"));
						orgdetails.setInvitedfrom(MainObj.getString("invitedfrom"));
						String uid = encry.encrypt(MainObj.getString("uid"));
						orgdetails.setUid(uid);
						orgdetails.setEmailid(MainObj.getString("emailid"));
						orgdetails.setStatus(MainObj.getString("status"));
						orgdetails.setMembername(MainObj.getString("membername"));
						orgdetails.setDeviceid(MainObj.getString("deviceid"));
						list.add(orgdetails);

					}
					response.put("StatusCode", 200);
					response.put("response", list);
					response.put("Message", "Organization Member Details data found!!!");
				} else {
					response.put("StatusCode", 204);
					response.put("Message", "No Data found");

				}
			}
		} else if (stat == 107) {
			response.put("StatusCode", 405);
			response.put("Message", "UnAuthorized user!!!");
		}
		json = mapper.writeValueAsString(response);
		return json;
	}

	@RequestMapping(value = "/downloadorganiztionwithorgcode", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public String downloadorganiztionwithorgcode(@RequestBody List<CreateOrganization> createorg) throws Exception {

		Map<String, Object> response = new HashMap<String, Object>();
		Map<String, Object> result = new HashMap<String, Object>();
		CreateOrganization data = createorg.get(0);
		List<CreateOrganization> list = new ArrayList<CreateOrganization>();
		Map<String, Object> verification = businessDAO.downloadToken(data.getUid(), data.getDeviceid());
		int stat = (int) verification.get("status");
		System.out.println(stat);
		if (stat == 106) {
			String token = (String) verification.get("token");
			String detoken = encry.decrypt(createorg.get(0).getToken());

			if ((detoken).equals(token) && token != null && !token.equals("")) {
				String orgunique, orguniqueEnc;
				result = businessDAO.downloadorganiztionwithorgcode(data);
				int status = (int) result.get("o_status");
				json = mapper.writeValueAsString(result);
				JSONObject jsonObj = new JSONObject(json);
				JSONArray jsonArray = jsonObj.getJSONArray("#result-set-1");
				if (jsonArray.length() > 0) {
					for (int i = 0; i < jsonArray.length(); i++) {

						JSONObject MainObj = jsonArray.getJSONObject(i);
						CreateOrganization orgdetails = new CreateOrganization();
						orgdetails.setOrgid(MainObj.getInt("orgid"));
						orgdetails.setOrgname(MainObj.getString("orgname"));
						orgdetails.setOrgcode(MainObj.getString("orgcode"));
						orgdetails.setOrguniqueid(MainObj.getString("orguniqueid"));
						orgunique = (MainObj.getString("orguniqueid"));
						orguniqueEnc = encry.encrypt(orgunique);
						orgdetails.setOrguniqueid(orguniqueEnc);
						String uid = encry.encrypt(MainObj.getString("uid"));
						orgdetails.setUid(uid);
						orgdetails.setCity(MainObj.getString("city"));
						orgdetails.setZipcode(MainObj.getString("zipcode"));
						orgdetails.setUdf1(MainObj.getString("udf1"));
						orgdetails.setState(MainObj.getString("state"));
						orgdetails.setCountry(MainObj.getString("country"));
						orgdetails.setUserMobileNo(MainObj.getString("userMobileNo"));
						orgdetails.setLogo(MainObj.getString("logo"));
						orgdetails.setDomain(MainObj.getString("domain"));
						orgdetails.setEmpstrength(MainObj.getString("empstrength"));
						orgdetails.setLongitude(MainObj.getString("longitude"));
						orgdetails.setLatitude(MainObj.getString("latitude"));
						orgdetails.setDeviceid(MainObj.getString("deviceid"));
						orgdetails.setCreatedBy(MainObj.getString("createdby"));
						list.add(orgdetails);

					}
					response.put("StatusCode", 200);
					response.put("response", list);
					response.put("Message", "Organization Details data found!!!");
				} else {
					response.put("StatusCode", 204);
					response.put("Message", "No Data found");

				}
			}
		} else if (stat == 107) {
			response.put("StatusCode", 405);
			response.put("Message", "UnAuthorized user!!!");
		}
		json = mapper.writeValueAsString(response);
		return json;
	}

	@RequestMapping(value = "/checkOrgCodeAvailability", method = RequestMethod.GET, produces = "application/json")
	public String checkOrgCodeAvailability(@RequestParam("orgcode") String orgcode)
			throws JSONException, JsonProcessingException {

		Map<String, Object> response = new HashMap<String, Object>();
		Map<String, Object> result = new HashMap<String, Object>();
		List<CreateOrganization> list = new ArrayList<CreateOrganization>();

		result = businessDAO.checkOrgCodeAvailability(orgcode);
		int status = (int) result.get("o_status");
		if (status == 106) {
			CreateOrganization org = new CreateOrganization();
			org.getLocal_id();
			response.put("StatusCode", 200);
			response.put("response", org);
			response.put("Message", " Group is available");
		} else {
			CreateOrganization org = new CreateOrganization();
			org.getLocal_id();
			response.put("StatusCode", 204);
			response.put("response", org);
			response.put("Message", "No Group Available");
		}
		json = mapper.writeValueAsString(response);
		return json;
	}

	/*****************************************
	 * Create Organization
	 * 
	 * @throws Exception
	 ***************************************/
	@RequestMapping(value = "/addorganizationwithoutname", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public String addorganizationwithoutname(@RequestBody List<CreateOrganization> createorg) throws Exception {

		System.out.println("uid......" + encry.encrypt(createorg.get(0).getUid()));
		System.out.println("token...." + encry.encrypt(createorg.get(0).getToken()));
		Map<String, Object> result = new HashMap<String, Object>();
		Map<String, Object> response = new HashMap<String, Object>();
		CreateOrganization data = createorg.get(0);
		JSONObject jsonObject = null, jObj = null;
		JSONArray jArr = null;
		CreateOrganization model = null;
		List<CreateOrganization> list = new ArrayList<CreateOrganization>();
		Map<String, Object> verification = businessDAO.downloadToken(data.getUid(), data.getDeviceid());
		int stat = (int) verification.get("status");
		System.out.println(stat);
		if (stat == 106) {
			String token = (String) verification.get("token");
			String detoken = encry.decrypt(data.getToken());

			if ((detoken).equals(token) && token != null && !token.equals("")) {
				int status = 0;
				String enc, datain;
				try {

					result = businessDAO.addorganizationwithoutname(data);
					System.out.println("result...." + result);

					status = (int) result.get("o_status");
					System.out.println(status);

					if (status == 106) {

						// converting map object into JSONArray Object
						json = mapper.writeValueAsString(result);
						jsonObject = new JSONObject(json);
						jArr = jsonObject.getJSONArray("#result-set-1");
						if (jArr.length() > 0) {
							for (int i = 0; i < jArr.length(); i++) {
								model = new CreateOrganization();
								jObj = jArr.getJSONObject(i);
								String orguniqueid = encry.encrypt(jObj.getString("orguniqueid"));
								model.setOrguniqueid(orguniqueid);
								String createdby = encry.encrypt(jObj.getString("createdby"));
								model.setCreatedBy(createdby);
								model.setOrgcode(jObj.getString("orgcode"));
								model.setOrgid(jObj.getInt("orgid"));
								model.setOrgname(jObj.getString("orgname"));
								model.setLocal_id(data.getLocal_id());

							}
							list.add(model);
						}
						response.put("StatusCode", 200);
						response.put("response", "Data Added");
						response.put("response", list);

						// String orguniqueid = (String)
						// result.get("o_orguniqueid");
						// int orgId = (int) result.get("i_maxid");
						// System.out.println(orguniqueid);
						// CreateOrganization orgData = new
						// CreateOrganization();
						// orgData.setOrgid(orgId);
						// orgData.setOrguniqueid(data.getOrguniqueid());
						// orgData.setOrgcode(data.getOrgcode());
						// orgData.setLocal_id(data.getLocal_id());
						// // enc = (jsonObject.getString("uid"));
						// datain = encry.encrypt(orguniqueid);
						// System.out.println(datain);
						// orgData.setOrguniqueid(datain);
						// // regData.setRegId(maxid);
						// // orgData.setLocal_id(model.getLocal_id());
						// response.put("StatusCode", 200);
						// response.put("response", orgData);
						// response.put("Message", "Data saved!!!");
					} else if (status == 107) {
						CreateOrganization orgData = new CreateOrganization();
						orgData.setOrgid(data.getOrgid());
						orgData.setOrguniqueid(data.getOrguniqueid());
						orgData.setOrgcode(data.getOrgcode());
						orgData.setLocal_id(data.getLocal_id());
						response.put("StatusCode", 204);
						response.put("response", orgData);
						response.put("Message", "Data updated!!!");
					}

					else if (status == 404) {
						response.put("StatusCode", 404);
						response.put("Message", "Something went wrong!!!");
					}

				} catch (Exception e) {

				}
			}
		} else if (stat == 107) {
			response.put("StatusCode", 405);
			response.put("Message", "UnAuthorized user!!!");
		}
		json = mapper.writeValueAsString(response);
		return json;
	}

	@RequestMapping(value = "/joinMember", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public String joinMember(@RequestBody List<OrganizationMemberModel> createorg) throws Exception {
		Map<String, Object> response = new HashMap<String, Object>();
		Map<String, Object> result = new HashMap<String, Object>();
		OrganizationMemberModel data = createorg.get(0);
		List<OrganizationMemberModel> list = new ArrayList<OrganizationMemberModel>();
		Map<String, Object> verification = businessDAO.downloadToken(data.getUid(), data.getDeviceid());
		int stat = (int) verification.get("status");
		if (stat == 106) {
			String token = (String) verification.get("token");
			String detoken = encry.decrypt(createorg.get(0).getToken());

			if ((detoken).equals(token) && token != null && !token.equals("")) {
				String orgunique, orguniqueEnc;
				result = businessDAO.joinMember(data);
				int status = (int) result.get("o_status");
				json = mapper.writeValueAsString(result);
				JSONObject jsonObj = new JSONObject(json);
				JSONArray jsonArray = jsonObj.getJSONArray("#result-set-1");
				if (jsonArray.length() > 0) {
					for (int i = 0; i < jsonArray.length(); i++) {

						JSONObject MainObj = jsonArray.getJSONObject(i);
						OrganizationMemberModel orgdetails = new OrganizationMemberModel();
						orgdetails.setOrgmemberid(MainObj.getInt("orgmemberid"));
						orgdetails.setInvitationto(MainObj.getString("invitationto"));
						orgdetails.setOrgcode(MainObj.getString("orgcode"));
						orgdetails.setOrguniqueid(MainObj.getString("orguniqueid"));
						orgunique = (MainObj.getString("orguniqueid"));
						orguniqueEnc = encry.encrypt(orgunique);
						orgdetails.setOrguniqueid(orguniqueEnc);
						String uid = encry.encrypt(MainObj.getString("uid"));
						orgdetails.setUid(uid);

						list.add(orgdetails);

					}
					response.put("StatusCode", 200);
					response.put("response", list);
					response.put("Message", "Successfully Join Organization!!!");
				} else {
					response.put("StatusCode", 204);
					response.put("Message", "No Data found");

				}
			}
		} else if (stat == 107) {
			response.put("StatusCode", 405);
			response.put("Message", "UnAuthorized user!!!");
		}
		json = mapper.writeValueAsString(response);
		return json;
	}

	// @RequestMapping(value = "/downloadorganiztionDetailwithUid", method =
	// RequestMethod.POST, consumes = "application/json", produces =
	// "application/json")
	// public String downloadorganiztionDetailwithUid(@RequestBody
	// List<CreateOrganization> createorg) throws Exception {
	//
	// Map<String, Object> response = new HashMap<String, Object>();
	// Map<String, Object> result = new HashMap<String, Object>();
	// CreateOrganization data = createorg.get(0);
	// List<CreateOrganization> list = new ArrayList<CreateOrganization>();
	// Map<String, Object> verification =
	// businessDAO.downloadToken(data.getUid(), data.getDeviceid());
	// int stat = (int) verification.get("status");
	// System.out.println(stat);
	// if (stat == 106) {
	// String token = (String) verification.get("token");
	// String detoken = encry.decrypt(createorg.get(0).getToken());
	//
	// if ((detoken).equals(token) && token != null && !token.equals("")) {
	// String orgunique, orguniqueEnc;
	// result = businessDAO.downloadorganiztionDetailwithUid(data);
	// int status = (int) result.get("o_status");
	// json = mapper.writeValueAsString(result);
	// JSONObject jsonObj = new JSONObject(json);
	// JSONArray jsonArray = jsonObj.getJSONArray("#result-set-1");
	// if (jsonArray.length() > 0) {
	// for (int i = 0; i < jsonArray.length(); i++) {
	//
	// JSONObject MainObj = jsonArray.getJSONObject(i);
	// CreateOrganization orgdetails = new CreateOrganization();
	// orgdetails.setOrgid(MainObj.getInt("orgid"));
	// orgdetails.setOrgname(MainObj.getString("orgname"));
	// orgdetails.setOrgcode(MainObj.getString("orgcode"));
	// orgdetails.setOrguniqueid(MainObj.getString("orguniqueid"));
	// orgunique = (MainObj.getString("orguniqueid"));
	// orguniqueEnc = encry.encrypt(orgunique);
	// orgdetails.setOrguniqueid(orguniqueEnc);
	// String uid = encry.encrypt(MainObj.getString("uid"));
	// orgdetails.setUid(uid);
	//
	//
	// orgdetails.setOrgcode1(MainObj.getString("orgcode1"));
	// orgdetails.setOrguniid(MainObj.getString("orguniid"));
	//
	// orgdetails.setCity(MainObj.getString("city"));
	// orgdetails.setZipcode(MainObj.getString("zipcode"));
	// orgdetails.setUdf1(MainObj.getString("udf1"));
	// orgdetails.setState(MainObj.getString("state"));
	// orgdetails.setCountry(MainObj.getString("country"));
	// orgdetails.setUserMobileNo(MainObj.getString("userMobileNo"));
	// orgdetails.setLogo(MainObj.getString("logo"));
	// orgdetails.setDomain(MainObj.getString("domain"));
	// orgdetails.setEmpstrength(MainObj.getString("empstrength"));
	// orgdetails.setLongitude(MainObj.getString("longitude"));
	// orgdetails.setLatitude(MainObj.getString("latitude"));
	// orgdetails.setDeviceid(MainObj.getString("deviceid"));
	// orgdetails.setCreatedBy(MainObj.getString("createdby"));
	// list.add(orgdetails);
	//
	// }
	// response.put("StatusCode", 200);
	// response.put("response", list);
	// response.put("Message", "Organization Details data found on The basis of
	// UID!!!");
	// } else {
	// response.put("StatusCode", 204);
	// response.put("Message", "No Data found");
	//
	// }
	// }
	// } else if (stat == 107) {
	// response.put("StatusCode", 405);
	// response.put("Message", "UnAuthorized user!!!");
	// }
	// json = mapper.writeValueAsString(response);
	// return json;
	// }

	@RequestMapping(value = "/downloadorganiztionDetailwithUid", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public String downloadorganiztionDetailwithUid(@RequestBody List<CreateOrganization> createorg) throws Exception {

		String uidenc = encry.decrypt("ucR+p2IUmE7TwSlNFGVxLaRpgzxQnmXygve4BIOTCjhhQCLCxtGr5KfJAWFJZfW1\n");
		System.out.println(uidenc);
		String tokenenc = encry.encrypt("ad222f63-0a08-46c7-9e64-b96dc7184e31");
		System.out.println(tokenenc);

		Map<String, Object> response = new HashMap<String, Object>();
		Map<String, Object> result = new HashMap<String, Object>();
		CreateOrganization data = createorg.get(0);
		List<CreateOrganization> list = new ArrayList<CreateOrganization>();
		List<CreateOrganization1> list1 = new ArrayList<CreateOrganization1>();
		Map<String, Object> verification = businessDAO.downloadToken(data.getUid(), data.getDeviceid());
		int stat = (int) verification.get("status");
		System.out.println(stat);
		if (stat == 106) {
			String token = (String) verification.get("token");
			String detoken = encry.decrypt(createorg.get(0).getToken());

			if ((detoken).equals(token) && token != null && !token.equals("")) {
				String orgunique, orguniqueEnc, orgunique1, orguniqueEnc1;
				result = businessDAO.downloadorganiztionDetailwithUid(data);
				int status = (int) result.get("o_status");
				json = mapper.writeValueAsString(result);
				JSONObject jsonObj = new JSONObject(json);
				JSONArray jsonArray = jsonObj.getJSONArray("#result-set-1");
				if (jsonArray.length() > 0) {
					for (int i = 0; i < jsonArray.length(); i++) {

						JSONObject MainObj = jsonArray.getJSONObject(i);
						CreateOrganization orgdetails = new CreateOrganization();
						CreateOrganization1 orgdetails1 = new CreateOrganization1();
						orgdetails.setOrgid(MainObj.getInt("orgid"));
						orgdetails.setOrgname(MainObj.getString("orgname"));
						orgdetails.setOrgcode(MainObj.getString("orgcode"));
						orgdetails.setOrguniqueid(MainObj.getString("orguniqueid"));
						orgunique = (MainObj.getString("orguniqueid"));
						orguniqueEnc = encry.encrypt(orgunique);
						orgdetails.setOrguniqueid(orguniqueEnc);
						String uid = encry.encrypt(MainObj.getString("uid"));
						orgdetails.setUid(uid);
						orgdetails.setCity(MainObj.getString("city"));
						orgdetails.setZipcode(MainObj.getString("zipcode"));
						orgdetails.setUdf1(MainObj.getString("udf1"));
						orgdetails.setState(MainObj.getString("state"));
						orgdetails.setCountry(MainObj.getString("country"));
						orgdetails.setUserMobileNo(MainObj.getString("userMobileNo"));
						orgdetails.setLogo(MainObj.getString("logo"));
						orgdetails.setDomain(MainObj.getString("domain"));
						orgdetails.setEmpstrength(MainObj.getString("empstrength"));
						orgdetails.setLongitude(MainObj.getString("longitude"));
						orgdetails.setLatitude(MainObj.getString("latitude"));
						orgdetails.setDeviceid(MainObj.getString("deviceid"));
						String created = encry.encrypt(MainObj.getString("createdby"));
						orgdetails.setCreatedBy(created);
						list.add(orgdetails);

						String orgid = MainObj.getString("orgid1");
						if (orgid == "null") {
							orgdetails1.setOrgid1("0");
						} else {
							orgdetails1.setOrgid1(MainObj.getString("orgid1"));
						}

						// orgdetails1.setOrgid1(MainObj.getString("orgid1"));
						orgdetails1.setOrgname1(MainObj.getString("orgname1"));
						orgdetails1.setOrgcode1(MainObj.getString("orgcode1"));
						orgunique1 = (MainObj.getString("orguniid1"));
						orguniqueEnc1 = encry.encrypt(orgunique1);
						orgdetails1.setOrguniid1(orguniqueEnc1);

						String uid1 = encry.encrypt(MainObj.getString("uid1"));
						orgdetails1.setUid1(uid1);

						orgdetails1.setCity1(MainObj.getString("city1"));

						orgdetails1.setZipcode1(MainObj.getString("zipcode1"));
						orgdetails1.setState1(MainObj.getString("state1"));
						orgdetails1.setCountry1(MainObj.getString("country1"));
						orgdetails1.setUserMobileNo1(MainObj.getString("userMobileNo1"));
						orgdetails1.setLogo1(MainObj.getString("logo1"));
						orgdetails1.setDomain1(MainObj.getString("domain1"));
						orgdetails1.setEmpstrength1(MainObj.getString("empstrength1"));
						orgdetails1.setLatitude1(MainObj.getString("latitude1"));
						orgdetails1.setLongitude1(MainObj.getString("longitude1"));
						orgdetails1.setDeviceid1(MainObj.getString("deviceid1"));
						String created1 = encry.encrypt(MainObj.getString("createdby1"));
						orgdetails1.setCreatedby1(created1);

						list1.add(orgdetails1);

					}
					response.put("StatusCode", 200);
					response.put("response", list);
					response.put("response1", list1);
					response.put("Message", "Organization Details data found on The basis of UID!!!");
				} else {
					response.put("StatusCode", 204);
					response.put("Message", "No Data found");

				}
			}
		} else if (stat == 107) {
			response.put("StatusCode", 405);
			response.put("Message", "UnAuthorized user!!!");
		}
		json = mapper.writeValueAsString(response);
		return json;
	}

	/**********************************
	 * checkdailylogin
	 * 
	 * @throws Exception
	 ********************************/
	@RequestMapping(value = "/checkdailylogin", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public String checkdailylogin(@RequestBody List<CRMmodel> data1) throws Exception {
		System.out.println("token.............." + encry.encrypt(data1.get(0).getToken()));
		Map<String, Object> response = new HashMap<String, Object>();
		Map<String, Object> result = new HashMap<String, Object>();
		List<LeadTagsModel> list = new ArrayList<LeadTagsModel>();

		String uid = encry.decrypt(data1.get(0).getUid());
		String orguniqueid = encry.decrypt(data1.get(0).getOrguniqueid());
		String deviceid = data1.get(0).getDeviceid();
		String detoken = encry.decrypt(data1.get(0).getToken());

		Map<String, Object> verification = businessDAO.downloadToken(data1.get(0).getUid(), deviceid);
		int stat = (int) verification.get("status");
		String token = (String) verification.get("token");
		System.out.println(stat);
		if ((detoken).equals(token) && token != null && !token.equals("")) {
			result = businessDAO.checkdailylogin(uid, orguniqueid, deviceid);
			json = mapper.writeValueAsString(result);
			JSONObject jsonObj = new JSONObject(json);
			if (jsonObj.has("#result-set-1")) {
				JSONArray jsonArray = jsonObj.getJSONArray("#result-set-1");
				if (jsonArray.length() > 0) {

					response.put("StatusCode", 200);
					response.put("Message", "Daily Login  Successful");
				} else {
					response.put("StatusCode", 204);
					response.put("Message", "No data found!!!");
				}
			} else {
				response.put("StatusCode", 204);
				response.put("Message", "No data found!!!");
			}

		} else if (stat == 107) {
			response.put("StatusCode", 405);
			response.put("Message", "UnAuthorized user!!!");
		}
		json = mapper.writeValueAsString(response);
		return json;
	}

	/*******************************************
	 * Download Delete Junior Join
	 * 
	 * @throws Exception
	 *****************************************/

	@RequestMapping(value = "/deleteMemberJoin", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public String deleteJuniorJoin(@RequestBody List<OrganizationMemberModel> detail) throws Exception {
		Map<String, Object> response = new HashMap<String, Object>();
		Map<String, Object> result = new HashMap<String, Object>();
		// Map<String, Object> verification = new HashMap<String, Object>();

		OrganizationMemberModel data = detail.get(0);
		String orguniid = encry.encrypt("10c99814-3f31-48fc-90d3-6aa65e91c107");
		System.out.println(orguniid);
		String uidenc = encry.encrypt("83b5c3b8-e8c0-4cbb-840f-3c9133b79396");
		System.out.println(uidenc);
		String tokenenc = encry.encrypt("144d6f11-8d4a-4641-87df-c7f5273a8db3");
		System.out.println(tokenenc);

		Map<String, Object> verification = businessDAO.downloadToken(data.getUid(), data.getDeviceid());
		int stat = (int) verification.get("status");
		if (stat == 106) {
			String token = (String) verification.get("token");
			String detoken = encry.decrypt(detail.get(0).getToken());

			if ((detoken).equals(token) && token != null && !token.equals("")) {

				result = businessDAO.deleteMemberJoin(data);

				int status = (int) result.get("o_status");

				if (status == 107) {
					data.setOrgmemberid(detail.get(0).getOrgmemberid());
					data.setLocal_id(detail.get(0).getLocal_id());
					response.put("response", data);
					response.put("StatusCode", 200);
					response.put("Message", "Data deleted successfully");
				} else if (status == 404) {
					response.put("StatusCode", 404);
					response.put("Message", "Data deletion failed");
				}
			} else {
				response.put("StatusCode", 405);
				response.put("Message", "UnAuthorized user!!!");
			}

		} else if (stat == 107) {
			response.put("StatusCode", 405);
			response.put("Message", "UnAuthorized user!!!");
		}

		json = mapper.writeValueAsString(response);
		return json;

	}

	/**********************************
	 * Add Last Seen
	 * 
	 * @throws Exception
	 ********************************/
	@RequestMapping(value = "/SaveLastSeenDetails", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public String SaveLastSeenDetails(@RequestBody List<LastSeenModel> lastSeenModel) throws Exception {

		Map<String, Object> response = new HashMap<String, Object>();
		Map<String, Object> result = new HashMap<String, Object>();
		LastSeenModel data = lastSeenModel.get(0);
		int maxid;

		Map<String, Object> verification = businessDAO.downloadToken(data.getUid(), data.getDeviceid());
		int stat = (int) verification.get("status");
		System.out.println(stat);
		if (stat == 106) {
			String token = (String) verification.get("token");
			String detoken = encry.decrypt(data.getToken());
			System.out.println(data.getToken());

			if ((detoken).equals(token) && token != null && !token.equals("")) {

				result = businessDAO.SaveLastSeenDetails(data);

				int status = (int) result.get("o_status");
				maxid = (int) result.get("i_maxid");
				System.out.println("status in controller" + status);
				if (status == 106) {
					LastSeenModel data1 = new LastSeenModel();
					data1.setLocal_id(lastSeenModel.get(0).getLocal_id());
					data1.setId(maxid);
					response.put("StatusCode", 200);
					response.put("response", "Data Added");
					response.put("response", data1);
				} else if (status == 107) {
					response.put("StatusCode", 204);
					response.put("response", "Data Updated !!!");
				} else if (status == 404) {
					response.put("StatusCode", 404);
					response.put("response", "Server Error!!!");
				}
			}
		} else if (stat == 107) {
			response.put("StatusCode", 405);
			response.put("Message", "UnAuthorized user!!!");
		}

		json = mapper.writeValueAsString(response);
		return json;
	}

	/**********************************
	 * Download Access Token
	 * 
	 * @throws Exception
	 ********************************/
	@RequestMapping(value = "/accesstoken", method = RequestMethod.GET, produces = "application/json")
	public String accesstoken() throws Exception {

		String result = "";
		String url = "https://graph.facebook.com/oauth/access_token?client_id=653820525174816&client_secret=16d965d8eb28dd46c48118f2218fe62a&grant_type=client_credentials";
		URL urlString = new URL(url);
		HttpURLConnection uc = (HttpURLConnection) urlString.openConnection();
		uc.setRequestMethod("GET");
		uc.connect();

		int code = uc.getResponseCode();
		String data = uc.getResponseMessage();

		BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream()));

		String inputLine;

		StringBuffer response1 = new StringBuffer();

		while ((inputLine = br.readLine()) != null) {
			response1.append(inputLine);
		}

		br.close();

		result = response1.toString();
		return result;
	}

	/***********************************
	 * Check app version
	 * 
	 * @throws Exception
	 ***********************************/

	@RequestMapping(value = "/checkappversion", method = RequestMethod.POST, produces = "application/json")
	public String checkAppVersion(@RequestBody List<AppVersionDetails> appversionDetails) throws Exception {

		Map<String, Object> result = new HashMap<String, Object>();
		Map<String, Object> response = new HashMap<String, Object>();
		// List<VTPDetails> list = new ArrayList<VTPDetails>();

		AppVersionDetails data = appversionDetails.get(0);

		Map<String, Object> verification = businessDAO.downloadToken(data.getUid(), data.getDeviceid());
		int stat = (int) verification.get("status");
		System.out.println(stat);
		if (stat == 106) {
			String token = (String) verification.get("token");
			String detoken = encry.decrypt(data.getToken());
			System.out.println(data.getToken());

			if ((detoken).equals(token) && token != null && !token.equals("")) {

				result = businessDAO.checkVersion(data);
				int status = (int) result.get("o_status");

				if (status == 1) {
					response.put("StatusCode", 1);
					response.put("response", "No Update Available");

				} else if (status == 2) {
					response.put("StatusCode", 2);
					response.put("response", "Update Available !!!");
				} else if (status == 3) {
					response.put("StatusCode", 3);
					response.put("response", "Update Mendatory !!!");
				}
			} else {
				response.put("StatusCode", 405);
				response.put("Message", "UnAuthorized user!!!");
			}
		} else if (stat == 107) {
			response.put("StatusCode", 405);
			response.put("Message", "UnAuthorized user!!!");
		}
		// } else if (priority == 0) {
		// response.put("StatusCode", 204);
		// response.put("Message", "No need to update!!!");
		// }

		json = mapper.writeValueAsString(response);
		return json;
	}

	@RequestMapping(value = "/generatetoken", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public String generatetoken(@RequestBody List<PaymentTokenModel> paymentmodel) throws Exception {
		Map<String, Object> response1 = new HashMap<String, Object>();
		String OrderId = paymentmodel.get(0).getOrderId();
		int OrderAmount = paymentmodel.get(0).getOrderAmount();
		String OrderCurrency = paymentmodel.get(0).getOrderCurrency();

		System.out.println(OrderCurrency);

		String result = "";
		HttpPost post = new HttpPost("https://test.cashfree.com/api/v2/cftoken/order");
		post.addHeader("content-type", "application/json");
		post.addHeader("X-Client-Id", "7434d54acf4c0c0d38413fcd4347");
		post.addHeader("X-Client-Secret", "9e5ba64c06aa56979ce1d844f0a1bf68a7f0ea58");

		StringBuilder entity = new StringBuilder();

		entity.append("{");
		entity.append("\"orderId\":\"" + OrderId + "\",");
		entity.append("\"orderAmount\":" + OrderAmount + ",");
		entity.append("\"orderCurrency\":\"" + OrderCurrency + "\"");
		entity.append("}");

		post.setEntity(new StringEntity(entity.toString()));

		try (CloseableHttpClient httpClient = HttpClients.createDefault();
				CloseableHttpResponse response = httpClient.execute(post)) {

			result = EntityUtils.toString(response.getEntity());

		}
		return result;
		// response1.put(result, result);

	}

	@RequestMapping(value = "/randomNoGenaeration", method = RequestMethod.GET, produces = "application/json")
	public String randomNoGenaeration() throws JSONException, JsonProcessingException {

		Map<String, Object> response = new HashMap<String, Object>();
		Map<String, Object> result = new HashMap<String, Object>();

		result = businessDAO.randomNoGenaeration();
		int status = (int) result.get("o_status");
		int id = (int) result.get("o_maxid");
		if (status == 106) {

			response.put("StatusCode", 200);
			response.put("OrderID", id);
		} else {

			response.put("StatusCode", 204);

		}
		json = mapper.writeValueAsString(response);
		return json;
	}

	/**********
	 * OTP VERIFICATION STATUS FOR FIREBASE
	 *
	 * @throws Exception
	 ************/
	@RequestMapping(value = "/otpverifyfirebase", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public String otpverifyfirebase(@RequestBody List<CRMmodel> data1) throws Exception {
		/*
		 * String uid = encry.encrypt("af815af1-0b98-46c6-8098-203a5711af3b");
		 * System.out.println(uid);
		 */
		int res = businessDAO.otpverifyfirebase(data1);
		System.out.println(res);
		List<CRMmodel> data = businessDAO.getData(data1);

		Map<String, Object> map = new HashMap<String, Object>();

		if (res == 106) {
			map.put("StatusCode", StaticData.OTP_VERIFIED);
			map.put("response", data);
			map.put("message", "OTP Verification Success");
		} else if (res == 103) {
			map.put("StatusCode", StaticData.OTP_NOT_VERIFIED);
			map.put("message", "OTP Verification Failed");
		} else {
			map.put("StatusCode", HttpURLConnection.HTTP_INTERNAL_ERROR);
			map.put("message", "error");
		}

		json = mapper.writeValueAsString(map);
		return json;
	}

	@RequestMapping(value = "/randomCodeGenaerationForTypeForm", method = RequestMethod.GET, produces = "application/json")
	public String randonIdGenaeration() throws JSONException, JsonProcessingException {

		Map<String, Object> response = new HashMap<String, Object>();
		Map<String, Object> result = new HashMap<String, Object>();
		String typeuniquecode = "";
		typeuniquecode = UUID.randomUUID().toString();
		response.put("TypeUniqueCode", typeuniquecode);

		json = mapper.writeValueAsString(response);
		return json;
	}

	/*******************************************
	 * Download details of same organization member
	 *
	 * @throws Exception
	 *****************************************/

	@RequestMapping(value = "/downloadDetailsofSameOrganizationMemberLeads", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public String downloadDetailsofSameOrganizationMemberLeads(@RequestBody List<MemberDetailsOfSameOrg> crmdata)
			throws Exception {
		Map<String, Object> response = new HashMap<String, Object>();
		Map<String, Object> result = new HashMap<String, Object>();
		List<MemberDetailsOfSameOrg> list = new ArrayList<MemberDetailsOfSameOrg>();
		String uid = encry.encrypt("83b5c3b8-e8c0-4cbb-840f-3c9133b79396");
		String org = encry.encrypt("904cc157-a8d0-45d5-9e83-30728728b766");
		String token1 = encry.encrypt("144d6f11-8d4a-4641-87df-c7f5273a8db3");

		System.out.println("uid  " + uid);
		System.out.println("org  " + org);
		System.out.println("token  " + token1);

		String orgunique, orguniqueEnc;
		MemberDetailsOfSameOrg data = crmdata.get(0);
		Map<String, Object> verification = businessDAO.downloadToken(data.getUid(), data.getDeviceid());
		int stat = (int) verification.get("status");
		System.out.println(stat);
		if (stat == 106) {
			String token = (String) verification.get("token");
			String detoken = encry.decrypt(crmdata.get(0).getToken());

			if ((detoken).equals(token) && token != null && !token.equals("")) {

				result = businessDAO.downloadDetailsofSameOrganizationMemberLeads(data);
				int status = (int) result.get("o_status");
				json = mapper.writeValueAsString(result);
				JSONObject jsonObj = new JSONObject(json);
				if (jsonObj.has("#result-set-1")) {
					JSONArray jsonArray = jsonObj.getJSONArray("#result-set-1");
					if (jsonArray.length() > 0) {
						for (int i = 0; i < jsonArray.length(); i++) {
							JSONObject jObj = jsonArray.getJSONObject(i);
							MemberDetailsOfSameOrg crmdetail = new MemberDetailsOfSameOrg();
							// crmdetail.setOrgcode(jObj.getString("orgcode"));
							crmdetail.setLeadid(jObj.getInt("leadid"));
							orgunique = (jObj.getString("orguniqueid"));
							orguniqueEnc = encry.encrypt(orgunique);
							crmdetail.setOrguniqueid(orguniqueEnc);

							crmdetail.setFirstname(jObj.getString("firstname"));
							crmdetail.setLastname(jObj.getString("lastname"));
							crmdetail.setEmailid(jObj.getString("emailid"));
							crmdetail.setPhonenocountrycode(jObj.getString("phonenocountrycode"));
							crmdetail.setPhoneno(jObj.getString("phoneno"));

							String uid1 = encry.encrypt(jObj.getString("uid"));
							crmdetail.setUid(uid);
							crmdetail.setWhatsupcountrycode(jObj.getString("whatsupcountrycode"));
							crmdetail.setWhatappno(jObj.getString("whatappno"));
							crmdetail.setLeadsourceid(jObj.getString("leadsourceid"));
							crmdetail.setDesignation(jObj.getString("designation"));
							crmdetail.setLeadorgname(jObj.getString("leadorgname"));
							crmdetail.setDomain(jObj.getString("domain"));
							crmdetail.setLeadstageid(jObj.getString("leadstageid"));
							crmdetail.setPriority(jObj.getString("priority"));
							crmdetail.setTags(jObj.getString("tags"));
							crmdetail.setAssignedto(jObj.getString("assignedto"));
							// crmdetail.setOrguniqueid(jObj.getString("orguniqueid"));
							crmdetail.setDeviceid(jObj.getString("deviceid"));
							crmdetail.setCountry(jObj.getString("country"));
							crmdetail.setState(jObj.getString("state"));
							crmdetail.setCity(jObj.getString("city"));

							list.add(crmdetail);
						}

						response.put("response", list);
						response.put("StatusCode", 200);
						response.put("Message", "Download Completed");
					} else {
						response.put("StatusCode", 204);
						response.put("response", list);
						response.put("Message", "No data found!!!");
					}
				} else {
					response.put("StatusCode", 204);
					response.put("Message", "No data found!!!");
				}
			}
		} else if (stat == 107) {
			response.put("StatusCode", 405);
			response.put("Message", "UnAuthorized user!!!");
		}
		json = mapper.writeValueAsString(response);
		return json;
	}

	/**********************************
	 * Download Access Token
	 * 
	 * @throws Exception
	 ********************************/
	@RequestMapping(value = "/fbgenratetoken", method = RequestMethod.GET, produces = "application/json")
	public String genratetoken(@RequestParam("fb_exchange_token") String fb_exchange_token) throws Exception {
		Map<String, Object> result1 = new HashMap<String, Object>();
		int error = 0;
		String result = "";
		String url = "https://graph.facebook.com/oauth/access_token?grant_type=fb_exchange_token&client_id=653820525174816&client_secret=16d965d8eb28dd46c48118f2218fe62a&fb_exchange_token="+fb_exchange_token;
		URL urlString = new URL(url);

		HttpURLConnection uc = (HttpURLConnection) urlString.openConnection();
		uc.setRequestMethod("GET");
		uc.connect();

		int code = uc.getResponseCode();
		String data = uc.getResponseMessage();

		if (uc.getResponseCode() >= 300) {
			error = uc.getResponseCode();
		}
		if (error == 400) {
			result1.put("StatusCode", 204);
			result1.put("Message", "No data found!!!");
			json = mapper.writeValueAsString(result1);
			return json;
			
		} else {
			BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream()));

			String inputLine;

			StringBuffer response1 = new StringBuffer();

			while ((inputLine = br.readLine()) != null) {
				response1.append(inputLine);
			}

			br.close();

			result = response1.toString();

		}

		return result;
		
	}

}
