package com.controller;

import java.io.IOException;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

public class HttpClientExample3_2 {
	
	public static void main(String[] args) {

		 

        try {
            String result = blockIP("1.1.1.1");
            System.out.println(result);
        } catch (IOException e) {
            e.printStackTrace();
        }

 

    }

 

    private static String blockIP(String ip) throws IOException {

 

        String result = "";

 

        HttpPost post = new HttpPost("https://test.cashfree.com/api/v2/cftoken/order");
        post.addHeader("content-type", "application/json");
        post.addHeader("X-Client-Id", "7434d54acf4c0c0d38413fcd4347");
        post.addHeader("X-Client-Secret", "9e5ba64c06aa56979ce1d844f0a1bf68a7f0ea58");

 

        String block = "{\"target\":\"ip\",\"value\":\"" + ip + "\"}";

 

        StringBuilder entity = new StringBuilder();
        entity.append("{");
        entity.append("\"mode\":\"block\",");
        entity.append("\"configuration\":" + block + ",");
        entity.append("\"notes\":\"hello\"");
        entity.append("}");

 

        // send a JSON data
        post.setEntity(new StringEntity(entity.toString()));

 

        try (CloseableHttpClient httpClient = HttpClients.createDefault();
             CloseableHttpResponse response = httpClient.execute(post)) {

 

            result = EntityUtils.toString(response.getEntity());
        }

 

        return result;

 

    }

}
