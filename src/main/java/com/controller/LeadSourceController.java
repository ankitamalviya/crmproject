package com.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dao.BusinessDAO;
import com.dao.LeadSourceDao;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.model.LeadModel;
import com.model.LeadSourceModel;
import com.model.StaticData;
import com.security.EncryptDecryptStringWithAES;

@RestController
public class LeadSourceController {
	StaticData staticData = new StaticData(); // calling static class
	@Autowired
	LeadSourceDao leadSourceDao;
	@Autowired
	BusinessDAO businessDAO;
	ObjectMapper objectMapper = new ObjectMapper();
	String json = "";
	String json1 = "";
	EncryptDecryptStringWithAES encry = new EncryptDecryptStringWithAES();

	/**********************************
	 * Add and Update Lead Source
	 * 
	 * @throws Exception
	 ********************************/
	@RequestMapping(value = "/addleadsource", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public String addleadsource(@RequestBody List<LeadSourceModel> leadSourceModel) throws Exception {

		Map<String, Object> response = new HashMap<String, Object>();
		Map<String, Object> result = new HashMap<String, Object>();
		List<LeadSourceModel> list = new ArrayList<LeadSourceModel>();
		LeadSourceModel model = null;
		System.out.println("inside service......");
		int status = 0;
		Map<String, Object> verification = businessDAO.downloadToken(leadSourceModel.get(0).getUid(),
				leadSourceModel.get(0).getDeviceid());
		int stat = (int) verification.get("status");
		if (stat == 106) {
			String token = (String) verification.get("token");
			String detoken = encry.decrypt(leadSourceModel.get(0).getToken());

			if ((detoken).equals(token) && token != null && !token.equals("")) {
				System.out.println("inside token..........");
				JSONObject jsonObject = null, jObj = null;
				JSONArray jArr = null;
				try {
					for (int j = 0; j < leadSourceModel.size(); j++) {
						LeadSourceModel data = leadSourceModel.get(0);
						result = leadSourceDao.addleadsource(data);
						status = (int) result.get("status");

						if (status == 106) {
							System.out.println("status inside 106............");

							// converting map object into JSONArray Object
							json = objectMapper.writeValueAsString(result);
							jsonObject = new JSONObject(json);
							jArr = jsonObject.getJSONArray("#result-set-1");
							if (jArr.length() > 0) {
								for (int i = 0; i < jArr.length(); i++) {
									model = new LeadSourceModel();
									jObj = jArr.getJSONObject(i);
									model.setSourceid(jObj.getInt("sourceid"));
									model.setSourcename(jObj.getString("sourcename"));
									String uid = encry.encrypt(jObj.getString("uid"));
									model.setUid(uid);
									String orguniquied = encry.encrypt(jObj.getString("orguniqueid"));
									model.setOrguniqueid(orguniquied);
									model.setStatus(jObj.getInt("status"));
									model.setCreateddate(jObj.getString("createddate"));
									model.setCreatedby(jObj.getString("createdby"));
									model.setDeviceid(jObj.getString("deviceid"));
									model.setModifiedby(jObj.getString("modifiedby"));
									model.setModifieddate(jObj.getString("modifieddate"));
									model.setLocaltb_id(leadSourceModel.get(j).getLocaltb_id());

								}
								list.add(model);
							}

						}
					}

					if (status == 106) {
						response.put("StatusCode", 200);
						response.put("response", "Data Added");
						response.put("response", list);
					} else if (status == 107) {
						response.put("StatusCode", 204);
						response.put("response", "Data Updated !!!");
						response.put("response", list);
					} else if (status == 404) {
						response.put("StatusCode", 404);
						response.put("response", "Server Error!!!");
					}

				} catch (Exception e) {
					// TODO: handle exception
				}
			}
		} else if (stat == 107) {
			response.put("StatusCode", 405);
			response.put("Message", "UnAuthorized user!!!");
		}

		json = objectMapper.writeValueAsString(response);
		return json;
	}

	/**********************************
	 * Download Lead details
	 * 
	 * @throws Exception
	 ********************************/
	@RequestMapping(value = "/downloadleadsource", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public String downloadleadsource(@RequestBody List<LeadSourceModel> leadSourceModel) throws Exception {
		Map<String, Object> response = new HashMap<String, Object>();
		Map<String, Object> result = new HashMap<String, Object>();
		List<LeadSourceModel> list = new ArrayList<LeadSourceModel>();
		System.out.println("token...." + encry.encrypt(leadSourceModel.get(0).getToken()));
		System.out.println("uid......"+encry.decrypt(leadSourceModel.get(0).getUid()));

		Map<String, Object> verification = businessDAO.downloadToken(leadSourceModel.get(0).getUid(),
				leadSourceModel.get(0).getDeviceid());
		int stat = (int) verification.get("status");
		System.out.println(stat);
		if (stat == 106) {
			System.out.println("inside 106.............");
			String token = (String) verification.get("token");
			String detoken = encry.decrypt(leadSourceModel.get(0).getToken());

			if ((detoken).equals(token) && token != null && !token.equals("")) {
				result = leadSourceDao.downloadleadsource(leadSourceModel.get(0).getOrguniqueid());
				json = objectMapper.writeValueAsString(result);
				JSONObject jsonObj = new JSONObject(json);

				if (jsonObj.has("#result-set-1")) {
					JSONArray jsonArray = jsonObj.getJSONArray("#result-set-1");
					if (jsonArray.length() > 0) {
						for (int i = 0; i < jsonArray.length(); i++) {
							JSONObject MainObj = jsonArray.getJSONObject(i);
							LeadSourceModel model = new LeadSourceModel();
							model.setSourceid(MainObj.getInt("sourceid"));
							model.setSourcename(MainObj.getString("sourcename"));
							String uid = encry.encrypt(MainObj.getString("uid"));
							model.setUid(uid);
							String orguniquied = encry.encrypt(MainObj.getString("orguniqueid"));
							model.setOrguniqueid(orguniquied);
							model.setStatus(MainObj.getInt("status"));
							model.setCreateddate(MainObj.getString("createddate"));
							model.setCreatedby(MainObj.getString("createdby"));
							model.setDeviceid(MainObj.getString("deviceid"));
							model.setModifiedby(MainObj.getString("modifiedby"));
							model.setModifieddate(MainObj.getString("modifieddate"));
							list.add(model);
						}

						response.put("response", list);
						response.put("StatusCode", 200);
						response.put("Message", "Download Completed");
					} else {
						response.put("StatusCode", 204);
						response.put("response", list);
						response.put("Message", "No data found!!!");
					}
				} else {
					response.put("StatusCode", 204);
					response.put("Message", "No data found!!!");
				}
			}
		} else if (stat == 107) {
			response.put("StatusCode", 405);
			response.put("Message", "UnAuthorized user!!!");
		}
		json = objectMapper.writeValueAsString(response);
		return json;
	}

}
