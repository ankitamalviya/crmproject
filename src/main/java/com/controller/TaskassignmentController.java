package com.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dao.BusinessDAO;
import com.dao.LeadSourceDao;
import com.dao.TaskassignmentDao;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.model.LeadSourceModel;
import com.model.StaticData;
import com.model.TaskassignmentModel;
import com.security.EncryptDecryptStringWithAES;
import com.security.SendNotification;

@RestController
public class TaskassignmentController {
	StaticData staticData = new StaticData(); // calling static class
	@Autowired
	TaskassignmentDao taskassignmentDao;
	@Autowired
	BusinessDAO businessDAO;
	ObjectMapper objectMapper = new ObjectMapper();
	String json = "";
	String json1 = "";
	EncryptDecryptStringWithAES encry = new EncryptDecryptStringWithAES();
	SendNotification send = new SendNotification();

	/**********************************
	 * Add and Update Lead Source
	 * 
	 * @throws Exception
	 ********************************/
	@RequestMapping(value = "/addtaskassignment", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public String addtaskassignment(@RequestBody List<TaskassignmentModel> taskassignmentModel) throws Exception {

		Map<String, Object> response = new HashMap<String, Object>();
		Map<String, Object> result = new HashMap<String, Object>();
		List<TaskassignmentModel> list = new ArrayList<TaskassignmentModel>();
		TaskassignmentModel model = null;
		String sendnotification = null;

		int status = 0;

		Map<String, Object> verification = businessDAO.downloadToken(taskassignmentModel.get(0).getUid(),
				taskassignmentModel.get(0).getDeviceid());
		int stat = (int) verification.get("status");
		System.out.println(stat);
		if (stat == 106) {
			String token = (String) verification.get("token");
			String detoken = encry.decrypt(taskassignmentModel.get(0).getToken());
			System.out.println("outside detoken");
			if ((detoken).equals(token) && token != null && !token.equals("")) {
				System.out.println("inside detoken");
				JSONObject jsonObject = null, jObj = null;
				JSONArray jArr = null;
				try {
					for (int j = 0; j < taskassignmentModel.size(); j++) {
						TaskassignmentModel data = taskassignmentModel.get(0);
						result = taskassignmentDao.addtaskassignment(data);

						status = (int) result.get("status");

						if (status == 106) {
							// converting map object into JSONArray Object
							json = objectMapper.writeValueAsString(result);
							jsonObject = new JSONObject(json);
							jArr = jsonObject.getJSONArray("#result-set-1");

							if (jArr.length() > 0) {
								for (int i = 0; i < jArr.length(); i++) {
									model = new TaskassignmentModel();
									jObj = jArr.getJSONObject(i);

									model.setTaskid(jObj.getInt("taskid"));
									model.setType(jObj.getInt("tasktype"));
									model.setLeadid(jObj.getInt("leadid"));
									model.setLeadorgname(jObj.getString("leadorgname"));
									model.setTaskname(jObj.getString("taskname"));
									model.setTaskdescription(jObj.getString("taskdescription"));
									model.setAssigendby(jObj.getString("assigendby"));
									model.setAssigendto(jObj.getString("assigendto"));
									model.setReminderdate(jObj.getString("reminder_date"));
									model.setRemindertime(jObj.getString("reminder_time"));
									model.setDuedate(jObj.getString("duedate"));
									model.setDuetime(jObj.getString("duetime"));
									String orguniqueid = encry.encrypt(jObj.getString("orguniqueid"));
									model.setOrguniqueid(orguniqueid);
									model.setDeviceid(jObj.getString("deviceid"));
									model.setCreatedby(jObj.getString("createdby"));
									model.setShort_description(jObj.getString("short_description"));
									model.setLong_description(jObj.getString("long_description"));
									String uid = encry.encrypt(jObj.getString("uid"));
									model.setUid(uid);
									model.setModifiedby(jObj.getString("modifiedby"));
									model.setLocal_id(taskassignmentModel.get(j).getLocal_id());

								}
								list.add(model);
							}

						}

					}

					if (status == 106) {

						// String assignto =
						// taskassignmentModel.get(0).getAssigendto();
						// String uid =
						// encry.decrypt(taskassignmentModel.get(0).getUid());
						//
						// if (!assignto.equals("")) {
						// assignto =
						// encry.decrypt(taskassignmentModel.get(0).getAssigendto());
						// }
						//
						String uid = "";
						String body = "";

						String assignto = taskassignmentModel.get(0).getAssigendto();
						if (!assignto.equals("")) {
							assignto = encry.decrypt(taskassignmentModel.get(0).getAssigendto());
						}
						if (assignto.equals("")) {
							uid = encry.decrypt(taskassignmentModel.get(0).getUid());
							body = "Task assigned to you";

						} else {
							uid = assignto;
							body = "assinged a task to you";
						}

						System.out.println("body....." + body);

						sendnotification = send.Notification(assignto, "Task assigned", "assigned a task to You.");
						System.out.println("sendnotification.........." + sendnotification);

						response.put("StatusCode", 200);
						response.put("response", "Data Added");
						response.put("response", list);
						response.put("Notification Status", sendnotification);

						// response.put("StatusCode", 200);
						// response.put("response", "Data Added");
						// response.put("response", list);
					} else if (status == 107) {
						response.put("StatusCode", 204);
						response.put("response", "Data Updated !!!");
						response.put("response", list);
					} else if (status == 404) {
						response.put("StatusCode", 404);
						response.put("response", "Server Error!!!");
					}

				} catch (Exception e) {
					// TODO: handle exception
				}
			}
		} else if (stat == 107) {
			response.put("StatusCode", 405);
			response.put("Message", "UnAuthorized user!!!");
		}

		json = objectMapper.writeValueAsString(response);
		return json;
	}

	/**********************************
	 * Download Lead details
	 * 
	 * @throws Exception
	 ********************************/
	@RequestMapping(value = "/downloadtaskassignment", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public String downloadtaskassignment(@RequestBody List<TaskassignmentModel> taskassignmentModel) throws Exception {
		Map<String, Object> response = new HashMap<String, Object>();
		Map<String, Object> result = new HashMap<String, Object>();
		List<TaskassignmentModel> list = new ArrayList<TaskassignmentModel>();
		TaskassignmentModel model = null;

		Map<String, Object> verification = businessDAO.downloadToken(taskassignmentModel.get(0).getUid(),
				taskassignmentModel.get(0).getDeviceid());
		int stat = (int) verification.get("status");
		System.out.println(stat);
		if (stat == 106) {
			String token = (String) verification.get("token");
			String detoken = encry.decrypt(taskassignmentModel.get(0).getToken());

			if ((detoken).equals(token) && token != null && !token.equals("")) {

				result = taskassignmentDao.downloadtaskassignment(taskassignmentModel.get(0).getOrguniqueid(),
						taskassignmentModel.get(0).getUid());

				json = objectMapper.writeValueAsString(result);
				JSONObject jsonObj = new JSONObject(json);

				if (jsonObj.has("#result-set-1")) {
					JSONArray jsonArray = jsonObj.getJSONArray("#result-set-1");
					if (jsonArray.length() > 0) {
						for (int i = 0; i < jsonArray.length(); i++) {
							model = new TaskassignmentModel();
							jsonObj = jsonArray.getJSONObject(i);
							model.setTaskid(jsonObj.getInt("taskid"));
							model.setType(jsonObj.getInt("tasktype"));
							model.setLeadid(jsonObj.getInt("leadid"));
							model.setLeadorgname(jsonObj.getString("leadorgname"));
							model.setTaskname(jsonObj.getString("taskname"));
							model.setTaskdescription(jsonObj.getString("taskdescription"));
							model.setAssigendby(jsonObj.getString("assigendby"));
							model.setAssigendto(jsonObj.getString("assigendto"));
							model.setReminderdate(jsonObj.getString("reminder_date"));
							model.setRemindertime(jsonObj.getString("reminder_time"));
							model.setDuedate(jsonObj.getString("duedate"));
							model.setDuetime(jsonObj.getString("duetime"));
							String orguniqueid = encry.encrypt(jsonObj.getString("orguniqueid"));
							model.setOrguniqueid(orguniqueid);
							model.setDeviceid(jsonObj.getString("deviceid"));
							model.setCreatedby(jsonObj.getString("createdby"));
							String uid = encry.encrypt(jsonObj.getString("uid"));
							model.setUid(uid);
							model.setModifiedby(jsonObj.getString("modifiedby"));
							list.add(model);
						}

						response.put("response", list);
						response.put("StatusCode", 200);
						response.put("Message", "Download Completed");
					} else {
						response.put("StatusCode", 204);
						response.put("response", list);
						response.put("Message", "No data found!!!");
					}
				} else {
					response.put("StatusCode", 204);
					response.put("Message", "No data found!!!");
				}
			}
		} else if (stat == 107) {
			response.put("StatusCode", 405);
			response.put("Message", "UnAuthorized user!!!");
		}
		json = objectMapper.writeValueAsString(response);
		return json;
	}

	/*******************************************
	 * Delate Task Assignment Info
	 *
	 * @throws Exception
	 *****************************************/

	@RequestMapping(value = "/deleteTaskInfo", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public String deleteTaskInfo(@RequestBody List<TaskassignmentModel> detail) throws Exception {
		Map<String, Object> response = new HashMap<String, Object>();
		Map<String, Object> result = new HashMap<String, Object>();
		// Map<String, Object> verification = new HashMap<String, Object>();

		TaskassignmentModel data = detail.get(0);

		Map<String, Object> verification = businessDAO.downloadToken(data.getUid(), data.getDeviceid());
		int stat = (int) verification.get("status");
		if (stat == 106) {
			String token = (String) verification.get("token");
			String detoken = encry.decrypt(detail.get(0).getToken());

			if ((detoken).equals(token) && token != null && !token.equals("")) {

				result = taskassignmentDao.deleteTaskInfo(data);

				int status = (int) result.get("o_status");

				if (status == 107) {
					data.setTaskid(detail.get(0).getTaskid());
					data.setLocal_id(detail.get(0).getLocal_id());
					response.put("response", data);
					response.put("StatusCode", 200);
					response.put("Message", "Data deleted successfully");
				} else if (status == 404) {
					response.put("StatusCode", 404);
					response.put("Message", "Data deletion failed");
				}
			} else {
				response.put("StatusCode", 405);
				response.put("Message", "UnAuthorized user!!!");
			}

		} else if (stat == 107) {
			response.put("StatusCode", 405);
			response.put("Message", "UnAuthorized user!!!");
		}

		json = objectMapper.writeValueAsString(response);
		return json;

	}

}
