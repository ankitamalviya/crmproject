package com.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.QueryParam;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dao.BusinessDAO;
import com.dao.LeadDao;
import com.dao.LeadTagsDao;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.model.CreateOrganization;
import com.model.LeadModel;
import com.model.LeadSourceModel;
import com.model.LeadTagsModel;
import com.model.StaticData;
import com.security.EncryptDecryptStringWithAES;
import com.security.SendNotification;

@RestController
public class LeadController {
	StaticData staticData = new StaticData(); // calling static class
	@Autowired
	LeadDao leadDao;
	@Autowired
	BusinessDAO businessDAO;
	@Autowired
	LeadTagsDao leadTagsDao;
	ObjectMapper objectMapper = new ObjectMapper();
	String json = "";
	String json1 = "";
	EncryptDecryptStringWithAES encry = new EncryptDecryptStringWithAES();
	SendNotification send = new SendNotification();

	/**********************************
	 * Add and Update Lead details
	 * 
	 * @throws Exception
	 ********************************/

	@RequestMapping(value = "/addleaddetails", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public String addleaddetails(@RequestBody List<LeadModel> leadModel) throws Exception {
		System.out.println("asstn to " + encry.decrypt(leadModel.get(0).getAssignedto()));
		Map<String, Object> response = new HashMap<String, Object>();
		Map<String, Object> result = new HashMap<String, Object>();
		Map<String, Object> result1 = new HashMap<String, Object>();
		Map<String, Object> result2 = new HashMap<String, Object>();
		String sendnotification = null;
		List<LeadModel> list = new ArrayList<LeadModel>();
		List<LeadTagsModel> listt = new ArrayList<LeadTagsModel>();
		 System.out.println("uid...."+encry.decrypt(leadModel.get(0).getAssignedto()));
		 System.out.println("token...."+encry.decrypt(leadModel.get(0).getUid()));
		 System.out.println("token...."+encry.decrypt(leadModel.get(0).getToken()));

		LeadModel model = null;

		Map<String, Object> verification = businessDAO.downloadToken(leadModel.get(0).getUid(),
				leadModel.get(0).getDeviceid());
		int stat = (int) verification.get("status");
		System.out.println(stat);
		if (stat == 106) {
			String token = (String) verification.get("token");
			String detoken = encry.decrypt(leadModel.get(0).getToken());
			System.out.println("tokendhgkdfjg....."+token);

			if ((detoken).equals(token) && token != null && !token.equals("")) {

				int status = 0;
				JSONObject jsonObject = null, jObj = null;
				JSONArray jArr = null;
				try {
					for (int j = 0; j < leadModel.size(); j++) {
						LeadModel data = leadModel.get(j);
						result = leadDao.addleaddetails(data);
						status = (int) result.get("status");

						if (status == 106) {
							json = objectMapper.writeValueAsString(result);
							jsonObject = new JSONObject(json);
							jArr = jsonObject.getJSONArray("#result-set-1");
							if (jArr.length() > 0) {
								for (int i = 0; i < jArr.length(); i++) {
									model = new LeadModel();
									jObj = jArr.getJSONObject(i);
									model.setLeadid(jObj.getInt("leadid"));

									for (int l = 0; l < data.getLeadtags().size(); l++) {
										result1 = leadTagsDao.addleadtags(data.getLeadtags().get(l),
												jObj.getInt("leadid"));
										result2 = leadTagsDao.downloadleadstage(jObj.getInt("leadid"));

										json1 = objectMapper.writeValueAsString(result2);
										JSONObject jsonObject1 = new JSONObject(json1);
										JSONArray jArr1 = jsonObject1.getJSONArray("#result-set-1");
										listt = new ArrayList<LeadTagsModel>();
										if (jArr1.length() > 0) {
											for (int k = 0; k < jArr1.length(); k++) {
												LeadTagsModel model1 = new LeadTagsModel();
												JSONObject jObj1 = jArr1.getJSONObject(k);
												model1.setLeadtagid(jObj1.getInt("leadtagid"));
												model1.setLeadid(jObj1.getInt("leadid"));
												model1.setTagid(jObj1.getInt("leadid"));
												model1.setDeviceid(jObj1.getString("deviceid"));
												// model1.setLeadtagname(jObj1.getString("leadtagname"));
												// model1.setColor(jObj1.getString("color"));
												model1.setCreatedby(jObj1.getString("createdby"));
												model1.setCreateddate(jObj1.getString("createddate"));
												model1.setModifiedby(jObj1.getString("modifiedby"));
												model1.setModifeddate(jObj1.getString("modifieddate"));
												model1.setStatus(jObj1.getString("status"));
												// model1.setTagcolor(jObj1.getString("tagcolor"));
												// model1.setTagname(jObj1.getString("tagname"));
												model1.setLocaltag_id(data.getLeadtags().get(l).getLocaltag_id());
												listt.add(model1);
											}

										}
									}

									model.setLeadtags(listt);

									String uid = encry.encrypt(jObj.getString("uid"));
									String orguniqueid = encry.encrypt(jObj.getString("orguniqueid"));
									String firstname = encry.encrypt(jObj.getString("firstname"));
									String lastname = encry.encrypt(jObj.getString("lastname"));
									String emailid = encry.encrypt(jObj.getString("emailid"));
									String phonenocountrycode = encry.encrypt(jObj.getString("phonenocountrycode"));
									String phoneno = encry.encrypt(jObj.getString("phoneno"));
									String whatsupcountrycode = encry.encrypt(jObj.getString("whatsupcountrycode"));
									String whatappno = encry.encrypt(jObj.getString("whatappno"));
									String assignto = encry.encrypt(jObj.getString("assignedto"));

									model.setFirstname(firstname);
									model.setLastname(lastname);
									model.setEmailid(emailid);
									model.setPhonenocountrycode(phonenocountrycode);
									model.setPhoneno(phoneno);
									model.setWhatsupcountrycode(whatsupcountrycode);
									model.setWhatappno(whatappno);
									model.setLeadsourceid(jObj.getInt("leadsourceid"));
									model.setDesignation(jObj.getString("designation"));
									model.setLeadorgname(jObj.getString("leadorgname"));
									model.setDomain(jObj.getString("domain"));
									model.setLeadstageid(jObj.getInt("leadstageid"));
									model.setPriority(jObj.getInt("priority"));
									model.setTags(jObj.getInt("tags"));
									model.setAssignedto(assignto);

									model.setOrguniqueid(orguniqueid);
									model.setDeviceid(jObj.getString("deviceid"));
									model.setCountry(jObj.getString("country"));
									model.setState(jObj.getString("state"));
									model.setCity(jObj.getString("city"));

									model.setUid(uid);
									model.setCreatedby(jObj.getString("createdby"));
									model.setModifiedby(jObj.getString("modifiedby"));
									model.setLocal_id(leadModel.get(j).getLocal_id());

								}
								list.add(model);
							}

						}
					}

					if (status == 106) {
						String name = encry.decrypt(leadModel.get(0).getFirstname());
						System.out.println(name);
						String uid = "";
						String body = "";
						String assignto = leadModel.get(0).getAssignedto();
						if (!assignto.equals("")) {
							assignto = encry.decrypt(leadModel.get(0).getAssignedto());
						}
						if (assignto.equals("")) {
							uid = encry.decrypt(leadModel.get(0).getUid());
							body = "lead assinged to you";

						} else {
							uid = assignto;

							body = name + " assinged a lead to you";
						}

						System.out.println("body....." + body);

						System.out.println("status 106");
						sendnotification = send.Notification(uid, "Lead Assinged", body);
						response.put("StatusCode", 200);
						response.put("response", "Data Added");
						response.put("response", list);
						response.put("Notification Status", sendnotification);

						
					} else if (status == 107) {
						response.put("StatusCode", 204);
						response.put("response", "Data Updated !!!");
						response.put("response", list);
					} else if (status == 404) {
						response.put("StatusCode", 404);
						response.put("response", "Server Error!!!");
					}
				} catch (Exception e) {
					System.out.println(e);
				}
			}
		} else if (stat == 107) {
			response.put("StatusCode", 405);
			response.put("Message", "UnAuthorized user!!!");
		}

		json = objectMapper.writeValueAsString(response);
		return json;
	}

	/**********************************
	 * Download Lead details
	 * 
	 * @throws Exception
	 ********************************/
	@RequestMapping(value = "/downloadleaddetails", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public String downloadleaddetails(@RequestBody List<LeadModel> leadModel) throws Exception {
		Map<String, Object> response = new HashMap<String, Object>();
		Map<String, Object> result = new HashMap<String, Object>();
		List<LeadModel> list = new ArrayList<LeadModel>();
		Map<String, Object> result2 = new HashMap<String, Object>();
		List<LeadTagsModel> listt = new ArrayList<LeadTagsModel>();

		Map<String, Object> verification = businessDAO.downloadToken(leadModel.get(0).getUid(),
				leadModel.get(0).getDeviceid());
		int stat = (int) verification.get("status");
		System.out.println(stat);
		if (stat == 106) {
			String token = (String) verification.get("token");
			String detoken = encry.decrypt(leadModel.get(0).getToken());

			if ((detoken).equals(token) && token != null && !token.equals("")) {

				result = leadDao.downloadleaddetails(leadModel.get(0).getOrguniqueid(), leadModel.get(0).getUid());
				json = objectMapper.writeValueAsString(result);
				JSONObject jsonObj = new JSONObject(json);
				if (jsonObj.has("#result-set-1")) {
					JSONArray jsonArray = jsonObj.getJSONArray("#result-set-1");
					if (jsonArray.length() > 0) {
						for (int i = 0; i < jsonArray.length(); i++) {
							JSONObject MainObj = jsonArray.getJSONObject(i);
							LeadModel model = new LeadModel();
							// detail.setAadharCard(MainObj.getString("adharcard"));
							model.setLeadid(MainObj.getInt("leadid"));

							result2 = leadTagsDao.downloadleadstage(MainObj.getInt("leadid"));
							System.out.println("result2..." + result2.toString());

							json1 = objectMapper.writeValueAsString(result2);
							JSONObject jsonObject1 = new JSONObject(json1);
							JSONArray jArr1 = jsonObject1.getJSONArray("#result-set-1");
							listt = new ArrayList<LeadTagsModel>();
							if (jArr1.length() > 0) {
								for (int k = 0; k < jArr1.length(); k++) {
									LeadTagsModel model1 = new LeadTagsModel();
									JSONObject jObj1 = jArr1.getJSONObject(k);
									model1.setLeadtagid(jObj1.getInt("leadtagid"));
									model1.setLeadid(jObj1.getInt("leadid"));
									model1.setTagid(jObj1.getInt("leadid"));
									model1.setDeviceid(jObj1.getString("deviceid"));
									// model1.setLeadtagname(jObj1.getString("leadtagname"));
									// model1.setColor(jObj1.getString("color"));
									model1.setCreatedby(jObj1.getString("createdby"));
									model1.setCreateddate(jObj1.getString("createddate"));
									model1.setModifiedby(jObj1.getString("modifiedby"));
									model1.setModifeddate(jObj1.getString("modifieddate"));
									model1.setStatus(jObj1.getString("status"));
									model1.setTagcolor(jObj1.getString("tagcolor"));
									model1.setTagname(jObj1.getString("tagname"));
									listt.add(model1);
								}

							}

							model.setLeadtags(listt);

							String uid = encry.encrypt(MainObj.getString("uid"));
							String orguniqueid = encry.encrypt(MainObj.getString("orguniqueid"));
							String firstname = encry.encrypt(MainObj.getString("firstname"));
							String lastname = encry.encrypt(MainObj.getString("lastname"));
							String emailid = encry.encrypt(MainObj.getString("emailid"));
							String phonenocountrycode = encry.encrypt(MainObj.getString("phonenocountrycode"));
							String phoneno = encry.encrypt(MainObj.getString("phoneno"));
							String whatsupcountrycode = encry.encrypt(MainObj.getString("whatsupcountrycode"));
							String whatappno = encry.encrypt(MainObj.getString("whatappno"));
							String assignto = encry.encrypt(MainObj.getString("assignedto"));

							model.setFirstname(firstname);
							model.setLastname(lastname);
							model.setEmailid(emailid);
							model.setPhonenocountrycode(phonenocountrycode);
							model.setPhoneno(phoneno);
							model.setWhatsupcountrycode(whatsupcountrycode);
							model.setWhatappno(whatappno);
							model.setLeadsourceid(MainObj.getInt("leadsourceid"));
							model.setDesignation(MainObj.getString("designation"));
							model.setLeadorgname(MainObj.getString("leadorgname"));
							model.setDomain(MainObj.getString("domain"));
							model.setLeadstageid(MainObj.getInt("leadstageid"));
							model.setPriority(MainObj.getInt("priority"));
							model.setTags(MainObj.getInt("tags"));
							model.setAssignedto(assignto);

							model.setOrguniqueid(orguniqueid);
							model.setDeviceid(MainObj.getString("deviceid"));
							model.setCountry(MainObj.getString("country"));
							model.setState(MainObj.getString("state"));
							model.setCity(MainObj.getString("city"));

							model.setUid(uid);
							model.setCreatedby(MainObj.getString("createdby"));
							model.setModifiedby(MainObj.getString("modifiedby"));
							list.add(model);
						}

						response.put("response", list);
						response.put("StatusCode", 200);
						response.put("Message", "Download Completed");
					} else {
						response.put("StatusCode", 204);
						response.put("response", list);
						response.put("Message", "No data found!!!");
					}
				} else {
					response.put("StatusCode", 204);
					response.put("Message", "No data found!!!");
				}
			}
		} else if (stat == 107) {
			response.put("StatusCode", 405);
			response.put("Message", "UnAuthorized user!!!");
		}
		json = objectMapper.writeValueAsString(response);
		return json;
	}

}
