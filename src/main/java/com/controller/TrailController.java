package com.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dao.BusinessDAO;
import com.dao.TrailDao;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.model.OrganizationTagModel;
import com.model.TrailModel;
import com.security.EncryptDecryptStringWithAES;
import com.security.SendNotification;

@RestController
public class TrailController {
	@Autowired
	TrailDao trailDao;
	@Autowired
	BusinessDAO businessDAO;
	ObjectMapper mapper = new ObjectMapper();
	String json = "";
	String json1 = "";

	EncryptDecryptStringWithAES encry = new EncryptDecryptStringWithAES();

	/**********************************
	 * Add and Update Trails
	 * 
	 * @throws Exception
	 ********************************/
	@RequestMapping(value = "/addTrailInfo", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public String addOrgTags(@RequestBody List<TrailModel> trailmodel) throws Exception {

		Map<String, Object> response = new HashMap<String, Object>();
		Map<String, Object> result = new HashMap<String, Object>();
		List<TrailModel> list = new ArrayList<TrailModel>();
		TrailModel traildata = null;
		String sendnotification = null;
		SendNotification send = new SendNotification();

		Map<String, Object> verification = businessDAO.downloadToken(trailmodel.get(0).getUid(),
				trailmodel.get(0).getDeviceid());
		int stat = (int) verification.get("status");
		System.out.println(stat);
		if (stat == 106) {
			String token = (String) verification.get("token");
			String detoken = encry.decrypt(trailmodel.get(0).getToken());

			if ((detoken).equals(token) && token != null && !token.equals("")) {
				int status = 0;
				String orgunique, orguniqueEnc;
				JSONObject jsonObject = null, jObj = null;
				JSONArray jArr = null;
				try {
					for (int j = 0; j < trailmodel.size(); j++) {
						TrailModel data = trailmodel.get(j);
						result = trailDao.addTrailInfo(data);
						status = (int) result.get("o_status");
						System.out.println(status);
						if (status == 106) {
							json = mapper.writeValueAsString(result);
							jsonObject = new JSONObject(json);
							jArr = jsonObject.getJSONArray("#result-set-1");
							if (jArr.length() > 0) {
								for (int i = 0; i < jArr.length(); i++) {

									jObj = jArr.getJSONObject(i);
									traildata = new TrailModel();
									traildata.setTrailid(jObj.getInt("trailid"));
									traildata.setLeadid(jObj.getInt("leadid"));
									traildata.setTagid(jObj.getInt("tagid"));
									orgunique = (jObj.getString("orguniqueid"));
									orguniqueEnc = encry.encrypt(orgunique);
									traildata.setOrguniqueid(orguniqueEnc);
									String uid = encry.encrypt(jObj.getString("uid"));
									traildata.setUid(uid);
									traildata.setActivitytype(jObj.getInt("activitytype"));
									traildata.setCallnotes(jObj.getString("callnotes"));
									traildata.setStatus(jObj.getString("status"));
									traildata.setActivitydate(jObj.getString("activitydate"));
									traildata.setActivitytime(jObj.getString("activitytime"));
									traildata.setAssociatedwith(jObj.getString("associatedwith"));
									traildata.setCallnotes(jObj.getString("calloutcome"));
									traildata.setDeviceid(jObj.getString("deviceid"));
									
									traildata.setAssociatedby(jObj.getString("associatedby"));
									
									traildata.setCreatedby(jObj.getString("createdby"));
									traildata.setModifiedby(jObj.getString("modifiedby"));
									traildata.setLocal_id(data.getLocal_id());
								}
								list.add(traildata);
							}
						}
					}

					if (status == 106) {

						String uid = encry.decrypt(trailmodel.get(0).getAssociatedwith());
						// result = trailDao.getUserRegisterData(uid);
						// json = mapper.writeValueAsString(result);
						// JSONObject object = new JSONObject(json);
						// JSONArray data =
						// object.getJSONArray("#result-set-1");
						// if (data.length() > 0) {
						//
						// for (int i = 0; i < data.length(); i++) {
						// TrailModel crm = new TrailModel();
						// JSONObject jsonObject1 = data.getJSONObject(i);
						// crm.setFirstname(jsonObject1.getString("firstName"));
						// crm.setLastname(jsonObject1.getString("lastName"));
						// crm.setToken(token);
						// list.add(i, crm);
						// }
						// }
						// System.out.println("status 106");
//						sendnotification = send.Notification(uid, "Notification send", "Notification Send");
						// sendnotification =
						// send.Notification(taskassignmentModel.get(0).getUid());

						System.out.println("sendnotification.........." + sendnotification);

						response.put("StatusCode", 200);
						response.put("response", "Data Added");
						response.put("response", list);
						response.put("Notification Status", sendnotification);

						// response.put("StatusCode", 200);
						// response.put("response", "Data Added");
						// response.put("response", list);
					} else if (status == 107) {
						response.put("StatusCode", 204);
						response.put("response", "Data Updated !!!");
						response.put("response", list);
					} else if (status == 404) {
						response.put("StatusCode", 404);
						response.put("response", "Server Error!!!");
					}

				} catch (Exception e) {
					// TODO: handle exception
				}

			}
		} else if (stat == 107) {
			response.put("StatusCode", 405);
			response.put("Message", "UnAuthorized user!!!");
		}

		json = mapper.writeValueAsString(response);
		return json;
	}

	/**********************************
	 * Download Trail Info
	 * 
	 * @throws Exception
	 ********************************/
	@RequestMapping(value = "/downloadTrailInfo", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public String downloadTrailInfo(@RequestBody List<TrailModel> trailinfonew) throws Exception {
		Map<String, Object> response = new HashMap<String, Object>();
		Map<String, Object> result = new HashMap<String, Object>();
		List<TrailModel> list = new ArrayList<TrailModel>();
		String orgunique, orguniqueEnc;
		TrailModel data = trailinfonew.get(0);
		Map<String, Object> verification = businessDAO.downloadToken(data.getUid(), data.getDeviceid());
		int stat = (int) verification.get("status");
		System.out.println(stat);
		if (stat == 106) {
			String token = (String) verification.get("token");
			String detoken = encry.decrypt(trailinfonew.get(0).getToken());

			if ((detoken).equals(token) && token != null && !token.equals("")) {

				result = trailDao.downloadTrailInfo(data);
				System.out.println("result.." + result);
				int status = (int) result.get("o_status");
				json = mapper.writeValueAsString(result);
				JSONObject jsonObj = new JSONObject(json);
				if (jsonObj.has("#result-set-1")) {
					JSONArray jsonArray = jsonObj.getJSONArray("#result-set-1");
					if (jsonArray.length() > 0) {
						for (int i = 0; i < jsonArray.length(); i++) {
							JSONObject jObj = jsonArray.getJSONObject(i);
							TrailModel traildata = new TrailModel();
							traildata.setTrailid(jObj.getInt("trailid"));
							traildata.setLeadid(jObj.getInt("leadid"));
							traildata.setTagid(jObj.getInt("tagid"));
							orgunique = (jObj.getString("orguniqueid"));
							orguniqueEnc = encry.encrypt(orgunique);
							traildata.setOrguniqueid(orguniqueEnc);
							String uid = encry.encrypt(jObj.getString("uid"));
							traildata.setUid(uid);
							traildata.setActivitytype(jObj.getInt("activitytype"));
							traildata.setCallnotes(jObj.getString("callnotes"));
							traildata.setStatus(jObj.getString("status"));
							traildata.setActivitydate(jObj.getString("activitydate"));
							traildata.setActivitytime(jObj.getString("activitytime"));
							traildata.setAssociatedwith(jObj.getString("associatedwith"));
							traildata.setCalloutcome(jObj.getString("calloutcome"));
							traildata.setDeviceid(jObj.getString("deviceid"));
							traildata.setAssociatedby(jObj.getString("associatedby"));
							traildata.setCreatedby(jObj.getString("createdby"));
							traildata.setModifiedby(jObj.getString("modifiedby"));

							list.add(traildata);
						}

						response.put("response", list);
						response.put("StatusCode", 200);
						response.put("Message", "Download Completed");
					} else {
						response.put("StatusCode", 204);
						response.put("response", list);
						response.put("Message", "No data found!!!");
					}
				} else {
					response.put("StatusCode", 204);
					response.put("Message", "No data found!!!");
				}
			}
		} else if (stat == 107) {
			response.put("StatusCode", 405);
			response.put("Message", "UnAuthorized user!!!");
		}
		json = mapper.writeValueAsString(response);
		return json;
	}

	/*******************************************
	 * Delate Trail Info
	 *
	 * @throws Exception
	 *****************************************/

	@RequestMapping(value = "/deleteTrailInfo", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public String deleteTrailInfo(@RequestBody List<TrailModel> detail) throws Exception {
		Map<String, Object> response = new HashMap<String, Object>();
		Map<String, Object> result = new HashMap<String, Object>();
		// Map<String, Object> verification = new HashMap<String, Object>();

		TrailModel data = detail.get(0);
		String orguniid = encry.encrypt("904cc157-a8d0-45d5-9e83-30728728b766");
		System.out.println(orguniid);
		String uidenc = encry.encrypt("83b5c3b8-e8c0-4cbb-840f-3c9133b79396");
		System.out.println(uidenc);
		String tokenenc = encry.encrypt("144d6f11-8d4a-4641-87df-c7f5273a8db3");
		System.out.println(tokenenc);

		Map<String, Object> verification = businessDAO.downloadToken(data.getUid(), data.getDeviceid());
		int stat = (int) verification.get("status");
		if (stat == 106) {
			String token = (String) verification.get("token");
			String detoken = encry.decrypt(detail.get(0).getToken());

			if ((detoken).equals(token) && token != null && !token.equals("")) {

				result = trailDao.deleteTrailInfo(data);

				int status = (int) result.get("o_status");

				if (status == 107) {
					data.setTrailid(detail.get(0).getTrailid());
					data.setLocal_id(detail.get(0).getLocal_id());
					response.put("response", data);
					response.put("StatusCode", 200);
					response.put("Message", "Data deleted successfully");
				} else if (status == 404) {
					response.put("StatusCode", 404);
					response.put("Message", "Data deletion failed");
				}
			} else {
				response.put("StatusCode", 405);
				response.put("Message", "UnAuthorized user!!!");
			}

		} else if (stat == 107) {
			response.put("StatusCode", 405);
			response.put("Message", "UnAuthorized user!!!");
		}

		json = mapper.writeValueAsString(response);
		return json;

	}

}
