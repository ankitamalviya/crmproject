package com.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dao.FeatureDao;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.model.FeatureModel;
import com.model.PaymentDetailsModel;

@RestController
public class FeatureController {
	@Autowired
	FeatureDao featureDao;
	ObjectMapper mapper = new ObjectMapper();
	String json = "";
	String json1 = "";

	/**********************************
	 * Add and Update Trails
	 * 
	 * @throws Exception
	 ********************************/
	@RequestMapping(value = "/savefeaturedetails", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public String savefeaturedetails(@RequestBody List<FeatureModel> featureModel) throws Exception {

		Map<String, Object> response = new HashMap<String, Object>();
		Map<String, Object> result = new HashMap<String, Object>();
		List<FeatureModel> list = new ArrayList<FeatureModel>();
		FeatureModel featureModel1 = null;

		//
		// Map<String, Object> verification =
		// businessDAO.downloadToken(trailmodel.get(0).getUid(),
		// trailmodel.get(0).getDeviceid());
		// int stat = (int) verification.get("status");
		// if (stat == 106) {
		// String token = (String) verification.get("token");
		// String detoken = encry.decrypt(trailmodel.get(0).getToken());

		// if ((detoken).equals(token) && token != null && !token.equals("")) {

		int status = 0;
		JSONObject jsonObject = null, jObj = null;
		JSONArray jArr = null;
		try {
			for (int j = 0; j < featureModel.size(); j++) {
				FeatureModel data = featureModel.get(j);
				result = featureDao.savefeaturedetails(data);
				status = (int) result.get("o_status");
				System.out.println(status);
				if (status == 106) {
					json = mapper.writeValueAsString(result);
					jsonObject = new JSONObject(json);
					jArr = jsonObject.getJSONArray("#result-set-1");
					if (jArr.length() > 0) {
						for (int i = 0; i < jArr.length(); i++) {

							jObj = jArr.getJSONObject(i);
							featureModel1 = new FeatureModel();
							featureModel1.setFid(jObj.getInt("fid"));
							featureModel1.setAmount(jObj.getString("amount"));
							featureModel1.setDuration(jObj.getString("duration"));
							featureModel1.setDiscount(jObj.getString("discount"));
							featureModel1.setInitialamount(jObj.getString("initialamount"));
							featureModel1.setFinalamount(jObj.getString("finalamount"));
							featureModel1.setCreatedby(jObj.getString("createdby"));
							featureModel1.setModifiedby(jObj.getString("modifiedby"));
							featureModel1.setLocal_id(data.getLocal_id());	

						}
						list.add(featureModel1);
					}
				}
			}

			if (status == 106) {

				response.put("StatusCode", 200);
				response.put("response", "Data Added");
				response.put("response", list);
			} else if (status == 107) {
				response.put("StatusCode", 204);
				response.put("response", "Unable to add data!!!");
				response.put("response", list);
			} else if (status == 404) {
				response.put("StatusCode", 404);
				response.put("response", "Server Error!!!");
			}

		} catch (Exception e) {
			// TODO: handle exception
		}

		// }
		// } else if (stat == 107) {
		// response.put("StatusCode", 405);
		// response.put("Message", "UnAuthorized user!!!");
		// }

		json = mapper.writeValueAsString(response);
		return json;
	}

}





