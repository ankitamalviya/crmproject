package com.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dao.BusinessDAO;
import com.dao.FacebookDao;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.model.FacebookpagesModel;
import com.model.FacebookuserModel;
import com.security.EncryptDecryptStringWithAES;

@RestController
public class FacebookController {
	@Autowired
	FacebookDao facebookdao;
	@Autowired
	BusinessDAO businessDAO;
	ObjectMapper objectMapper = new ObjectMapper();
	String json = "";
	String json1 = "";
	EncryptDecryptStringWithAES encry = new EncryptDecryptStringWithAES();

	/**********************************
	 * Add and Update facebook user
	 * 
	 * @throws Exception
	 ********************************/

	@RequestMapping(value = "/addfacebookuser", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public String addleaddetails(@RequestBody List<FacebookuserModel> facebookuserModel) throws Exception {

		Map<String, Object> response = new HashMap<String, Object>();
		Map<String, Object> result = new HashMap<String, Object>();
		Map<String, Object> result1 = new HashMap<String, Object>();
		Map<String, Object> result2 = new HashMap<String, Object>();
		List<FacebookuserModel> list = new ArrayList<FacebookuserModel>();
		List<FacebookpagesModel> listt = new ArrayList<FacebookpagesModel>();

		FacebookuserModel model = null;

		Map<String, Object> verification = businessDAO.downloadToken(facebookuserModel.get(0).getUid(),
				facebookuserModel.get(0).getDeviceid());
		int stat = (int) verification.get("status");
		System.out.println(stat);
		if (stat == 106) {
			String token = (String) verification.get("token");
			String detoken = encry.decrypt(facebookuserModel.get(0).getToken());

			if ((detoken).equals(token) && token != null && !token.equals("")) {

				int status = 0;
				JSONObject jsonObject = null, jObj = null;
				JSONArray jArr = null;
				try {
					for (int j = 0; j < facebookuserModel.size(); j++) {
						FacebookuserModel data = facebookuserModel.get(j);
						result = facebookdao.addfacebookuser(data);
						status = (int) result.get("status");

						if (status == 106 || status == 105) {
							json = objectMapper.writeValueAsString(result);
							jsonObject = new JSONObject(json);
							jArr = jsonObject.getJSONArray("#result-set-1");
							if (jArr.length() > 0) {
								for (int i = 0; i < jArr.length(); i++) {
									model = new FacebookuserModel();
									jObj = jArr.getJSONObject(i);
									model.setFbuserid(jObj.getString("fbuserid"));
									model.setId(jObj.getInt("id"));

									for (int l = 0; l < data.getPages().size(); l++) {
										result1 = facebookdao.addfacebookpage(data.getPages().get(l));	
										result2 = facebookdao.downloadfacebookpages(jObj.getString("fbuserid"));

										json1 = objectMapper.writeValueAsString(result2);
										JSONObject jsonObject1 = new JSONObject(json1);
										JSONArray jArr1 = jsonObject1.getJSONArray("#result-set-1");
										listt = new ArrayList<FacebookpagesModel>();
										if (jArr1.length() > 0) {
											for (int k = 0; k < jArr1.length(); k++) {
												FacebookpagesModel model1 = new FacebookpagesModel();
												JSONObject jObj1 = jArr1.getJSONObject(k);
												model1.setPid(jObj1.getInt("pid"));;
												model1.setFbuserid(jObj1.getString("fbuserid"));
												model1.setPageid(jObj1.getString("pageid"));
												model1.setPagename(jObj1.getString("pagename"));		
												model1.setDeviceid(jObj1.getString("deviceid"));
												model1.setCreatedby(jObj1.getString("createdby"));
												model1.setCreateddate(jObj1.getString("createddate"));
												model1.setModifiedby(jObj1.getString("modifiedby"));
												model1.setModifieddate(jObj1.getString("modifieddate"));
												model1.setLocal_id(data.getPages().get(l).getLocal_id());
												listt.add(model1);
											}

										}
									}

									model.setPages(listt); 
									model.setFirstname(jObj.getString("firstname"));
									model.setLastname(jObj.getString("lastname"));
									model.setEmail(jObj.getString("email"));
									model.setStatus(jObj.getInt("status"));
									
									String uid = encry.encrypt(data.getUid());
									model.setUid(uid);
									String token1 = encry.encrypt(data.getToken());
									model.setToken(token1);
									String orguniqueid = encry.encrypt(jObj.getString("orguniqueid"));
									model.setOrguniqueid(orguniqueid);
									model.setDeviceid(jObj.getString("deviceid"));
									model.setCreatedby(jObj.getString("createdby"));
									model.setCreateddate(jObj.getString("createddate"));
									model.setModifiedby(jObj.getString("modifiedby"));
									model.setModifieddate(jObj.getString("modifieddate"));
									model.setLocal_id(data.getLocal_id());

									
								}
								list.add(model);
							}

						}
					}

					if (status == 106) {
						response.put("StatusCode", 200);
						response.put("response", "Data Added");
						response.put("response", list);
					} else if (status == 105) {
						response.put("StatusCode", 201);
						response.put("response", "Duplicate Entry !!!");
						response.put("response", list);
					}
					else if (status == 107) {
						response.put("StatusCode", 204);
						response.put("response", "Data Updated !!!");
						response.put("response", list);
					} else if (status == 404) {
						response.put("StatusCode", 404);
						response.put("response", "Server Error!!!");
					}
				} catch (Exception e) {
					System.out.println(e);
				}
			}
		} else if (stat == 107) {
			response.put("StatusCode", 405);
			response.put("Message", "UnAuthorized user!!!");
		}

		json = objectMapper.writeValueAsString(response);
		return json;
	}

}
