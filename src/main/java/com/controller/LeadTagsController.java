package com.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dao.BusinessDAO;
import com.dao.LeadStageDao;
import com.dao.LeadTagsDao;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.model.LeadTagsModel;

@RestController
public class LeadTagsController {
	@Autowired
	LeadTagsDao leadTagsDao;
	@Autowired
	BusinessDAO businessDAO;
	ObjectMapper mapper = new ObjectMapper();
	String json = "";
	String json1 = "";

	/**********************************
	 * Add and Update Lead Source
	 * @throws Exception 
	 ********************************/
	@RequestMapping(value = "/addleadtags", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public String addleadtags(@RequestBody List<LeadTagsModel> leadTagsModel) throws Exception {

		Map<String, Object> response = new HashMap<String, Object>();
		Map<String, Object> result = new HashMap<String, Object>();
		LeadTagsModel data = leadTagsModel.get(0);

		Map<String, Object> verification = businessDAO.downloadToken(data.getUid(), data.getDeviceid());
		int stat = (int) verification.get("status");
		System.out.println(stat);
		if (stat == 106) {
			String token = (String) verification.get("token");
			System.out.println(data.getToken());

			if ((data.getToken()).equals(token) && token != null && !token.equals("")) {

		
		
//		result = leadTagsDao.addleadtags(data);

		int status = (int) result.get("o_status");
		System.out.println("status in controller" + status);
		if (status == 106) {
			System.out.println("status 106");
			response.put("StatusCode", 200);
			response.put("response", "Data Added");
		} else if (status == 107) {
			response.put("StatusCode", 204);
			response.put("response", "Data Updated !!!");
		} else if (status == 404) {
			response.put("StatusCode", 404);
			response.put("response", "Server Error!!!");
		}
			}
		}else if (stat == 107) {
				response.put("StatusCode", 405);
				response.put("Message", "UnAuthorized user!!!");
			}

		json = mapper.writeValueAsString(response);
		return json;
	}

	/**********************************
	 * Download Lead details
	 * 
	 * @throws JsonProcessingException
	 ********************************/
	@RequestMapping(value = "/downloadleadtags", method = RequestMethod.GET, produces = "application/json")
	public String downloadleadsource(@RequestParam("leadid") int leadid, @RequestParam("tagid") int tagid)
			throws JsonProcessingException, JSONException {
		Map<String, Object> response = new HashMap<String, Object>();
		Map<String, Object> result = new HashMap<String, Object>();
		List<LeadTagsModel> list = new ArrayList<LeadTagsModel>();

		
		result = leadTagsDao.downloadleadstage(leadid);
		json = mapper.writeValueAsString(result);
		JSONObject jsonObj = new JSONObject(json);
		if (jsonObj.has("#result-set-1")) {
			JSONArray jsonArray = jsonObj.getJSONArray("#result-set-1");
			if (jsonArray.length() > 0) {
				for (int i = 0; i < jsonArray.length(); i++) {
					JSONObject MainObj = jsonArray.getJSONObject(i);
					LeadTagsModel model = new LeadTagsModel();
					model.setLeadtagid(MainObj.getInt("leadtagid"));
					model.setLeadid(MainObj.getInt("leadid"));
					model.setTagid(MainObj.getInt("leadid"));
					model.setDeviceid(MainObj.getString("deviceid"));
//					model.setColor(MainObj.getString("color"));
					model.setCreatedby(MainObj.getString("createdby"));
					model.setCreateddate(MainObj.getString("createddate"));
					model.setModifiedby(MainObj.getString("modifiedby"));
					model.setModifeddate(MainObj.getString("modifieddate"));
					model.setStatus(MainObj.getString("status"));
					list.add(model);
				}

				response.put("response", list);
				response.put("StatusCode", 200);
				response.put("Message", "Download Completed");
			} else {
				response.put("StatusCode", 204);
				response.put("response", list);
				response.put("Message", "No data found!!!");
			}
		} else {
			response.put("StatusCode", 204);
			response.put("Message", "No data found!!!");
		}
		json = mapper.writeValueAsString(response);
		return json;
	}

}
