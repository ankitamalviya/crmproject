package com.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import javax.net.ssl.HttpsURLConnection;
import javax.ws.rs.QueryParam;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dao.BusinessDAO;
import com.dao.FcmDao;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.model.FcmModel;
import com.model.LeadModel;
import com.security.EncryptDecryptStringWithAES;

@RestController
public class FcmController {
	@Autowired
	FcmDao fcmDao;
	@Autowired
	BusinessDAO businessDAO;
	ObjectMapper objectMapper = new ObjectMapper();
	String json = "";
	String json1 = "";
	EncryptDecryptStringWithAES encry = new EncryptDecryptStringWithAES();

	/**********************************
	 * Add and Update Lead Source
	 * 
	 * @throws Exception
	 ********************************/
	@RequestMapping(value = "/addfmcdata", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public String addfmcdata(@RequestBody List<FcmModel> fcmModel) throws Exception {

		Map<String, Object> response = new HashMap<String, Object>();
		Map<String, Object> result = new HashMap<String, Object>();
		List<FcmModel> list = new ArrayList<FcmModel>();
		int status = 0;
		Map<String, Object> verification = businessDAO.downloadToken(fcmModel.get(0).getUid(),
				fcmModel.get(0).getDeviceid());
		int stat = (int) verification.get("status");
		if (stat == 106) {
			System.out.println("106..............");
			String token = (String) verification.get("token");
			String detoken = encry.decrypt(fcmModel.get(0).getToken());

			if ((detoken).equals(token) && token != null && !token.equals("")) {

				JSONObject jsonObject = null, jObj = null;
				JSONArray jArr = null;
				FcmModel model = null;
				try {
					for (int j = 0; j < fcmModel.size(); j++) {
						FcmModel data = fcmModel.get(j);
						result = fcmDao.addfmcdata(data);
						status = (int) result.get("status");

						if (status == 106) {
							// converting map object into JSONArray Object
							json = objectMapper.writeValueAsString(result);
							jsonObject = new JSONObject(json);
							jArr = jsonObject.getJSONArray("#result-set-1");
							if (jArr.length() > 0) {
								for (int i = 0; i < jArr.length(); i++) {
									JSONObject MainObj = jArr.getJSONObject(i);
									model = new FcmModel();
									model.setTbid(MainObj.getInt("tbid"));
									model.setFcmid(MainObj.getString("fcmid"));
									model.setAppkeyword(MainObj.getString("appkeyword"));
									// model.setLocaltb_id(data.getLocaltb_id());
									model.setMobileno(MainObj.getString("mobileno"));
									model.setUid(MainObj.getString("uid"));
									model.setDeviceid(MainObj.getString("deviceid"));
									model.setCreatedby(MainObj.getString("createdby"));
									model.setModifiedby(MainObj.getString("modifiedby"));

								}
								list.add(model);
							}

						}
					}

					System.out.println("status in controller" + status);
					if (status == 106) {
						System.out.println("status 106");
						response.put("StatusCode", 200);
						response.put("response", "Data Added");
						response.put("response", list);
					} else if (status == 107) {
						response.put("StatusCode", 204);
						response.put("response", "Data Updated !!!");
						response.put("response", list);
					} else if (status == 404) {
						response.put("StatusCode", 404);
						response.put("response", "Server Error!!!");
					}
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
		} else if (stat == 107) {
			response.put("StatusCode", 405);
			response.put("Message", "UnAuthorized user!!!");
		}

		json = objectMapper.writeValueAsString(response);
		return json;
	}

	// FireBaseNotification
	@RequestMapping(value = "/firebasenotification", method = RequestMethod.POST, produces = "application/json;charset=utf-8")

	public String downloadFireBaseMessagesByMobNo(@QueryParam("toMobileNo") String toMobileNo)
			throws JsonProcessingException, JSONException {

		int statusCode = 0;

		Map<String, Object> map = new HashMap<String, Object>();
		ArrayList<String> s = new ArrayList<String>();
		String result = null;
		// int serverId = 0;
		String status = "";
		// String FcmId = "";
		String videoID = "";
		String message = "";
		String userName = "";
		String usermobile = "";
		String ToMobileNo = "";
		String queshionId = "";
		String date = "";
		String time = "";
		String finalString = "";

		map = fcmDao.downloadFireBaseMessagesByMobNo(toMobileNo);
		json = objectMapper.writeValueAsString(map);
		JSONObject jobj = new JSONObject(json);
		JSONArray jArr = jobj.getJSONArray("#result-set-1");
		if (jArr.length() > 0) {
			for (int i = 0; i < jArr.length(); i++) {
				JSONObject jObj1 = jArr.getJSONObject(i);

				if (!org.json.JSONObject.NULL.equals(jObj1.opt("Fcmid")) && jObj1.getString("Fcmid") != null) {
					result = jObj1.getString("Fcmid");
				}

				try {
					String url = "https://fcm.googleapis.com/fcm/send";

					URL url1 = new URL(url);

					HttpsURLConnection con = (HttpsURLConnection) url1.openConnection();
					con.setRequestMethod("POST");
					con.setDoOutput(true);

					int responseCode = con.getResponseCode();

					if (responseCode == HttpsURLConnection.HTTP_OK) {
						BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));

						String inputLine;

						StringBuffer response = new StringBuffer();

						while ((inputLine = br.readLine()) != null) {
							response.append(inputLine);
						}

						br.close();

					}

				} catch (Exception e) {

				}
			}
		}

		return json;

	}

	// ResponseEntity<String>
	@RequestMapping(value = "/send", method = RequestMethod.POST, consumes = "application/json", produces = "application/json;charset=utf-8")
	public String send(@RequestBody FcmModel fireBaseDataModel) throws JSONException, JsonProcessingException {

		int statusCode = 0;

		Map<String, Object> map = new HashMap<String, Object>();
		Map<String, Object> response = new HashMap<String, Object>();
		List<String> fcmList = new ArrayList<String>();
		List<String> respList = new ArrayList<String>();
		String firebaseResponse = "";
		fcmList = fcmDao.downloadFireBaseMessagesByMobNo(fireBaseDataModel);
		for (int i = 0; i < fcmList.size(); i++) {
			String fcm = (String) fcmList.get(i);
			System.out.println("fcm......" + fcm);
			JSONObject body = new JSONObject();
			body.put("to", fcm);
			body.put("priority", "high");

			JSONObject notification = new JSONObject();
			JSONObject notification1 = new JSONObject();
			notification.put("title", "hello...");
			notification.put("body", "body");
			notification1.put("notification", notification);
			body.put("data", notification1);
			// body.put("data", data);
			HttpEntity<String> request = new HttpEntity<>(body.toString());

			CompletableFuture<String> pushNotification = fcmDao.send(request);
			System.out.println("pushNotification..........." + pushNotification);
			CompletableFuture.allOf(pushNotification).join();

			try {
				firebaseResponse = pushNotification.get();
				respList.add(firebaseResponse);
				continue;

			} catch (InterruptedException e) {

			} catch (ExecutionException e) {

			}
		}
		if (firebaseResponse.equals("")) {
			response.put("StatusCode", HttpURLConnection.HTTP_NO_CONTENT);
			// map1.put("SendSMS", );
			response.put("message", "Not Notification");
			statusCode = HttpURLConnection.HTTP_NO_CONTENT;
		} else {
			response.put("StatusCode", HttpURLConnection.HTTP_OK);
			response.put("response", respList);
			response.put("message", "Notification Ok");
		}
		json = objectMapper.writeValueAsString(response);
		// return new ResponseEntity<>(respList, HttpStatus.OK);
		return json;
		// }

	}

	// @Scheduled(cron = "0 */1 * * * ?")
	// public void cronTest() throws JSONException, IOException {
	//
	//
	// int distId = 530;
	// int epId = 30;
	// int lbId = 54321;
	// int wardNo = 0;
	// String uId = "a8ddc8da-86a1-429b-8c8c-d73b120a8b2c";
	//
	// String strRequest = "distId=" + distId + "&epId=" + epId +
	// "&localBodyId=" + lbId + "&wardNo=" + wardNo
	// + "&uId=" + uId;
	// String url = "http://52.183.34.76:8080/Nomination/nominationFilledCnt?";
	// String url1 = "http://52.183.34.76:8080/Nomination/registerednNotCnt?";
	// String url2 = "http://52.183.34.76:8080/Nomination/notInExpenseCnt?";
	// String url3 = "http://52.183.34.76:8080/True_Voter/RoDashboardCnt?";
	//
	// /********************
	// * For Counting Total Number of Candidate
	// ********************************/
	//
	// int totCand = 0;
	//
	// URL stringurl = new URL(url + strRequest);
	// HttpURLConnection uc = (HttpURLConnection) stringurl.openConnection();
	// System.out.println(uc.getResponseMessage());
	// if (uc.getResponseCode() == HttpURLConnection.HTTP_OK) {
	// BufferedReader in = new BufferedReader(new
	// InputStreamReader(uc.getInputStream()));
	// StringBuffer response = new StringBuffer();
	//
	// String inputLine;
	//
	// while ((inputLine = in.readLine()) != null) {
	// response.append(inputLine);
	// }
	//
	// in.close();
	//
	// // System.out.println(response.toString());
	// String response1 = response.toString();
	//
	// JSONObject obj = new JSONObject(response1);
	// JSONArray resArr = obj.getJSONArray("response");
	// JSONObject resObj = resArr.getJSONObject(0);
	// totCand = (int) resObj.get("nomFilled");
	// System.out.println(resObj.get("nomFilled"));
	// } else {
	// System.out.println("Something went wrong!!!");
	// }
	//
	// /*************************
	// * For Counting Number of Candidate Registered
	// *****************************/
	//
	// int registered = 0, notRegistered = 0;
	//
	// URL stringurl1 = new URL(url1 + strRequest);
	// HttpURLConnection uc1 = (HttpURLConnection) stringurl1.openConnection();
	// System.out.println(uc1.getResponseMessage());
	// if (uc1.getResponseCode() == HttpURLConnection.HTTP_OK) {
	// BufferedReader in = new BufferedReader(new
	// InputStreamReader(uc1.getInputStream()));
	// StringBuffer response = new StringBuffer();
	//
	// String inputLine;
	//
	// while ((inputLine = in.readLine()) != null) {
	// response.append(inputLine);
	// }
	//
	// in.close();
	//
	// // System.out.println(response.toString());
	// String response1 = response.toString();
	//
	// JSONObject obj = new JSONObject(response1);
	// JSONArray resArr = obj.getJSONArray("response");
	// JSONObject resObj = resArr.getJSONObject(0);
	//
	// registered = resObj.getInt("registeredCnt");
	// notRegistered = resObj.getInt("notRegisteredCnt");
	// System.out.println(resObj);
	// } else {
	// System.out.println("Something went wrong!!!");
	// }
	//
	// /**************************
	// * Count of Number of Persons Expense not Filled
	// ************************/
	//
	// int expNotFilled = 0;
	//
	// URL stringurl2 = new URL(url2 + strRequest);
	// HttpURLConnection uc2 = (HttpURLConnection) stringurl2.openConnection();
	// System.out.println(uc2.getResponseMessage());
	// if (uc2.getResponseCode() == HttpURLConnection.HTTP_OK) {
	// BufferedReader in = new BufferedReader(new
	// InputStreamReader(uc2.getInputStream()));
	// StringBuffer response = new StringBuffer();
	//
	// String inputLine;
	//
	// while ((inputLine = in.readLine()) != null) {
	// response.append(inputLine);
	// }
	//
	// in.close();
	//
	// // System.out.println(response.toString());
	// String response1 = response.toString();
	//
	// JSONObject obj = new JSONObject(response1);
	// JSONArray resArr = obj.getJSONArray("response");
	// JSONObject resObj = resArr.getJSONObject(0);
	// expNotFilled = resObj.getInt("expNotCnt");
	// System.out.println(resObj);
	// } else {
	// System.out.println("Something went wrong!!!");
	// }
	//
	// /**************************
	// * Number of Candidate Expense Filled And Deviation Count
	// ********************/
	//
	// int totalExpFilled = 0, deviation = 0;
	//
	// URL stringurl3 = new URL(url3 + strRequest);
	// HttpURLConnection uc3 = (HttpURLConnection) stringurl3.openConnection();
	// System.out.println(uc3.getResponseMessage());
	// if (uc3.getResponseCode() == HttpURLConnection.HTTP_OK) {
	// BufferedReader in = new BufferedReader(new
	// InputStreamReader(uc3.getInputStream()));
	// StringBuffer response = new StringBuffer();
	//
	// String inputLine;
	//
	// while ((inputLine = in.readLine()) != null) {
	// response.append(inputLine);
	// }
	//
	// in.close();
	//
	// // System.out.println(response.toString());
	// String response1 = response.toString();
	//
	// JSONObject obj = new JSONObject(response1);
	// JSONArray resArr = obj.getJSONArray("response");
	// System.out.println(resArr.length());
	//
	// if (resArr.length() == 1) {
	//
	// } else {
	// for (int i = 0; i < resArr.length(); i++) {
	// JSONObject resObj = (JSONObject) resArr.get(i);
	// totalExpFilled = totalExpFilled + resObj.getInt("printCount");
	// deviation = deviation + resObj.getInt("daviCount");
	// }
	// }
	// } else {
	// System.out.println("Something went wrong!!!");
	// }
	//
	// System.out.println("Total Expense Count: " + totalExpFilled);
	// System.out.println("Total Deviation Count: " + deviation);
	//
	// String message = "Today's Count: \n" + " Total Candidate: " + totCand +
	// "\n Registered: " + registered
	// + "\n Not Registered: " + notRegistered + "\n Expense Not Filled: " +
	// expNotFilled
	// + "\n Total Expense Filled: " + totalExpFilled + "\n Deviation Count: " +
	// deviation;
	//
	// System.out.println(message);
	//
	// ArrayList<String> list = new ArrayList<String>();
	// list.add("8007990858");
	//
	// FcmModel fireBase = new FcmModel();
	// fireBase.setTitle("truevoter");
	// fireBase.setBody(message);
	// fireBase.setUserMobNoArr(list);
	//
	// send(fireBase);
	// }

}
