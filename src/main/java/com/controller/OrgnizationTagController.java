package com.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jboss.jandex.Main;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dao.BusinessDAO;
import com.dao.OrganizationTagDao;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.model.CreateOrganization;
import com.model.LeadSourceModel;
import com.model.OrganizationTagModel;
import com.security.EncryptDecryptStringWithAES;

@RestController

public class OrgnizationTagController {
	@Autowired
	OrganizationTagDao orgtagDao;
	@Autowired
	BusinessDAO businessDAO;
	ObjectMapper mapper = new ObjectMapper();
	String json = "";
	String json1 = "";

	EncryptDecryptStringWithAES encry = new EncryptDecryptStringWithAES();

	/**********************************
	 * Add and Update Organization Tags
	 * 
	 * @throws Exception
	 ********************************/
	@RequestMapping(value = "/addOrgTags", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public String addOrgTags(@RequestBody List<OrganizationTagModel> orgtagmodel) throws Exception {

		Map<String, Object> response = new HashMap<String, Object>();
		Map<String, Object> result = new HashMap<String, Object>();
		List<OrganizationTagModel> list = new ArrayList<OrganizationTagModel>();
		OrganizationTagModel orgdata = null;

		Map<String, Object> verification = businessDAO.downloadToken(orgtagmodel.get(0).getUid(),
				orgtagmodel.get(0).getDeviceid());
		int stat = (int) verification.get("status");
		if (stat == 106) {
			String token = (String) verification.get("token");
			String detoekn = encry.decrypt(orgtagmodel.get(0).getToken());

			if ((detoekn).equals(token) && token != null && !token.equals("")) {

				int status = 0;
				JSONObject jsonObject = null, jObj = null;
				JSONArray jArr = null;
				try {
					for (int j = 0; j < orgtagmodel.size(); j++) {
						OrganizationTagModel data = orgtagmodel.get(0);
						result = orgtagDao.addOrgTags(data);
						status = (int) result.get("o_status");
						if (status == 106) {
							json = mapper.writeValueAsString(result);
							jsonObject = new JSONObject(json);
							jArr = jsonObject.getJSONArray("#result-set-1");
							if (jArr.length() > 0) {
								for (int i = 0; i < jArr.length(); i++) {
									orgdata = new OrganizationTagModel();
									jObj = jArr.getJSONObject(i);
									orgdata.setTagid(jObj.getInt("tagid"));
									orgdata.setTagname(jObj.getString("tagname"));
									orgdata.setTagdescription(jObj.getString("tagdescription"));
									orgdata.setTagcolor(jObj.getString("tagcolor"));
									String uid = encry.encrypt(jObj.getString("uid"));
									orgdata.setUid(uid);
									String orguniqueid = encry.encrypt(jObj.getString("orguniqueid"));
									orgdata.setOrguniqueid(orguniqueid);
									orgdata.setStatus(jObj.getString("status"));
									orgdata.setDeviceid(jObj.getString("deviceid"));
									orgdata.setCreatedby(jObj.getString("createdby"));
									orgdata.setModifiedby(jObj.getString("modifiedby"));
									orgdata.setLocal_id(orgtagmodel.get(j).getLocal_id());
								}
								list.add(orgdata);
							}
						}
					}

					if (status == 106) {
						response.put("StatusCode", 200);
						response.put("response", "Data Added");
						response.put("response", list);
					} else if (status == 107) {
						response.put("StatusCode", 204);
						response.put("response", "Data Updated !!!");
						response.put("response", list);
					} else if (status == 404) {
						response.put("StatusCode", 404);
						response.put("response", "Server Error!!!");
					}

				} catch (Exception e) {
					// TODO: handle exception
				}
			}
		} else if (stat == 107) {
			response.put("StatusCode", 405);
			response.put("Message", "UnAuthorized user!!!");
		}

		json = mapper.writeValueAsString(response);
		return json;
	}

	/**********************************
	 * Download Organization Tags
	 * 
	 * @throws Exception
	 ********************************/
	@RequestMapping(value = "/downloadOrganizationTags", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public String downloadOrganizationTags(@RequestBody List<OrganizationTagModel> orgtagnew) throws Exception {
		Map<String, Object> response = new HashMap<String, Object>();
		Map<String, Object> result = new HashMap<String, Object>();
		List<OrganizationTagModel> list = new ArrayList<OrganizationTagModel>();
		String orgunique, orguniqueEnc;
		OrganizationTagModel data = orgtagnew.get(0);

		Map<String, Object> verification = businessDAO.downloadToken(data.getUid(), data.getDeviceid());
		int stat = (int) verification.get("status");
		if (stat == 106) {
			String token = (String) verification.get("token");
			String detoekn = encry.decrypt(orgtagnew.get(0).getToken());

			if ((detoekn).equals(token) && token != null && !token.equals("")) {
System.out.println("inside ......token");
				result = orgtagDao.downloadOrganizationTags(data);
				int status = (int) result.get("o_status");
				json = mapper.writeValueAsString(result);
				JSONObject jsonObj = new JSONObject(json);
				if (jsonObj.has("#result-set-1")) {
					JSONArray jsonArray = jsonObj.getJSONArray("#result-set-1");
					if (jsonArray.length() > 0) {
						for (int i = 0; i < jsonArray.length(); i++) {
							JSONObject MainObj = jsonArray.getJSONObject(i);
							OrganizationTagModel model = new OrganizationTagModel();
							model.setTagid(MainObj.getInt("tagid"));
							model.setTagname(MainObj.getString("tagname"));
							model.setTagdescription(MainObj.getString("tagdescription"));
							model.setTagcolor(MainObj.getString("tagcolor"));
							String uid = encry.encrypt(MainObj.getString("uid"));
							model.setUid(uid);

							orgunique = (MainObj.getString("orguniqueid"));
							orguniqueEnc = encry.encrypt(orgunique);

							model.setOrguniqueid(orguniqueEnc);
							model.setStatus(MainObj.getString("status"));
							model.setDeviceid(MainObj.getString("deviceid"));
							model.setCreatedby(MainObj.getString("createdby"));

							model.setModifiedby(MainObj.getString("modifiedby"));
							// model.setLocal_id(MainObj.getString("local_id"));
							list.add(model);
						}

						response.put("response", list);
						response.put("StatusCode", 200);
						response.put("Message", "Download Completed");
					} else {
						response.put("StatusCode", 204);
						response.put("response", list);
						response.put("Message", "No data found!!!");
					}
				} else {
					response.put("StatusCode", 204);
					response.put("Message", "No data found!!!");
				}
			}
		} else if (stat == 107) {
			response.put("StatusCode", 405);
			response.put("Message", "UnAuthorized user!!!");
		}
		json = mapper.writeValueAsString(response);
		return json;
	}

}
